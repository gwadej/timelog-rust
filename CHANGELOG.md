# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - Unreleased

### Changed

- Continue cleanup
- Documentation edits

## [1.0.0] - 2023-08-29

### Changed

- Updated dependencies
- Line order only determined by timestamps
- Another edit pass on documentation
- Update copyrights on documentation

### Removed

- Deprecated `pop` command
- Deprecated `drop` command
- Deprecated `lstk` command
- Deprecated `summary` command
- Deprecated `hours` command
- Deprecated `chart` command
- Deprecated `task` command

## [0.52.0] - unreleased

### Changed

- Refactoring and moving some error structs
- Overhaul use of Result
- Use operators instead of cmp directly.
- Cleanup work
- Add deny checks
- Use `pub use` to re-export types, rather than aliasing with `type`

## [0.51.1] - unreleased

### Changed

- Expanded clippy rules I'm following

## [0.51.0] - 2023-01-13

### Added

- Added the method `process_down_stack` to simplify processing the stack
  without manipulating the stack file directly.
- Added new `stack top` command to print just the top of stack.
- Added top-level `entry` command to replace `task`. The new name fits
  what the command is doing better.

### Changed

- Changed output of `stack ls` to include an index before each line
  - A stack of more than a handful was hard to read, which was necessary for keep/drop.
- The `task` top-level command is now deprecated in favor of `entry`.

## [0.50.3] - 2023-01-07

### Changed

- Fix documentation on rtimelog program for new commands
- Formatting changes for tests
- Optimize release build for size
- Improved comparison of Percent and TagPercent

## [0.50.2] - 2022-12-28

### Changed

- Modify `task rewrite` to not affect ignored or stop entries
- Modify `task was` to not affect ignored or stop entries
- Modify `task rewind` to not affect ignored or stop entries
- Modify `task now` to not affect ignored or stop entries

## [0.50.1] - Unreleased

### Changed

- Update Manual to add new commands and date range description
- Update Tutorial to add new commands
- Update Readme to point to the main branch on Gitlab.

## [0.50.0] - 2022-12-22

### Added

- New DateRange and RangeParser code to normalize date range work.
- Support new date range specifications with the new parsing code.
- Add new `task was` and `task rewind` commands.

### Changed

- Simplify arg processing for `stack drop` and `stack keep`.
- Some reorganization and better documentation of date module.
- Make `check` command aware of events.

## [0.49.2] - Unreleased

### Changed

- Update crate dependencies

## [0.49.1] - 2022-12-15

### Changed

- Modified formatting so long project names still have space before duration.
- Fixed day calculation when a task crosses midnight.

## [0.49.0] - 2022-12-15

### Added

- All reports except `chart` now supports a `-p` option that specifies a project to include.

### Changed

- Internal cleanup with pedantic clippy
- Replace PosIntArg with NonZeroU32
- Update to rust 1.65.0
- Drop structopt dependency for clap derive

### Removed

- The list of project entries after the date range on reports, use the new `-p` instead.

## [0.48.0] - 2022-09-17

### Changed

- Modify `report intervals` to display the interval since the last event
- Internal cleanup issues.

## [0.47.0] - 2022-09-06

### Added

- New parameter to `report events` giving a compact form of the report
- Added `report intervals` to show time between events
- `report events` now supports project filtering
- `report intervals` supports project filtering

## [0.46.4] - 2022-07-30

### Changed

- Event report failed on days with no time entries - fixed
- Factored the reporting code out of Day.

## [0.46.3] - 2022-07-10

### Changed

- Improve tutorial documentation
- Improve tutorial formatting
- Improve manual documentation
- Changed default browse entry from `chromium-browser` to `chromium` to match
  modern installation
- Correct handling of parameters overriding configuration file values.

## [0.46.2] - 2022-06-29

### Changed

- Fix `defcmd` to default to `curr`, as documented.
- Create `History.md` for the historical context portion of the `Manual.md`

### Removed

- Remove the redundant and unnecessary `stack pop` command.

## [0.46.1] - 2022-06-26

### Changed

- Update documentation in `Tutorial.md` and `Manual.md`
- Correct terminology in some of the help messages.

## [0.46.0] - 2022-06-25

### Changed

- Rewrite `Format.md` to cover the newest version of the file format.
- Changing all of the internal _event_ terminology to _entry_ for log entries.
- Change references to _event_ in a context with a duration to _task_.
- Change `pin` command to `event` for zero duration events.
- Change `report pins` to `report events`.

## [0.45.5] - Unreleased

### Changed

- Better handling of deprecated commands.

## [0.45.4] - Unreleased

### Changed

- Make EventKind public as well as renaming the internal representation.

## [0.45.3] - 2022-06-22

### Changed

- Improved accuracy in Manual.md and Tutorial.md documentation files.

## [0.45.1] - 2022-06-20

### Changed

- Updated Manual.md documentation to cover all new commands.

## [0.45.0] - 2022-06-19

### Added

- New `task discard` command to delete the most recent event from the logfile.
- New `task now` command to set the most recent event timestamp to now.
- New `task rewrite` command to change the task text for the most recent event.
- Add support for _ignored_ event lines.
- Add support for comments.
- Add support for _pin_ event lines.
- New `report pins` command to list the pins for a date range.

### Changed

- Replace reporting commands with a new set of sub commands
  - Replace `report` with `report detail`
  - Replace `summary` with `report summary`
  - Replace `hours` with `report hours`
  - Replace `chart` with `report chart`
- Updated Format.md to cover new event line types
  - ignored lines
  - comments

## [0.31.0] - Unreleased

### Changed

- Improve formatting of help messages.
- More modifications to make code more idiomatic

### Deprecated

- Deprecated old lstk, drop, and pop commands
   - They no longer work, they just get a message announcing the deprecation.

## [0.30.0] - 2022-04-03

### Added

- New `check` command to detect problems with the logfile.

## [0.25.3] - 2022-04-03

### Changed

- Update dependencies
- Fixed the day-crossing problem

## [0.25.2] - 2022-03-22

### Added

- More unit testing

## [0.25.1] - 2022-03-21

### Fixed

- Default command logic didn't work, corrected.
- Correct the end of year handling for archive.
- Fix chart if task has blank project

## [0.25.0] - 2022-03-19

### Added

- New stack commands: ls, clear, drop, keep, pop
- New swap command

### Deprecated

- Deprecated old lstk, drop, and pop commands
   - They still work, they just get a message announcing the deprecation.

## [0.21.0] - 2022-03-16

### Changed

- Changed alias expansion to support template parameters
- Cleaner, more idiomatic code

## [0.20.0] - 2022-03-15

### Added

- Released on gitlab.com
- Add License and contributing docs
- Mostly feature-complete with original Perl version
- New archive command
- New chart command
