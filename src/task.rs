//! Represention of a task event.
//!
//! # Examples
//!
//! ```rust
//! use std::time::Duration;
//! use timelog::{DateTime, TaskEvent};
//!
//! # fn main() {
//! let timestamp = DateTime::new((2022, 03, 14), (10, 0, 0)).expect("Invalid date or time");
//! let mut event = TaskEvent::new(
//!     timestamp,
//!     "project_1",
//!     Duration::from_secs(300)
//! );
//! println!("{}", event.project());
//! event.add_dur(Duration::from_secs(3000));
//! println!("{:?}", event.duration());
//! # }
//! ```
//!
//! # Description
//!
//! The [`TaskEvent`] type represents a single task event containing a
//! start time and duration.

use std::time::Duration;

#[doc(inline)]
use crate::date::DateTime;
#[doc(inline)]
use crate::entry::Entry;

/// Structure representing a single task event.
#[derive(Debug, Clone, Eq, Ord, PartialOrd, PartialEq)]
pub struct TaskEvent {
    /// Timestamp of the start of this event
    start: DateTime,
    /// Optional [`String`] representing the project
    proj:  Option<String>,
    /// Calculated total [`Duration`] of the event
    dur:   Duration
}

impl TaskEvent {
    /// Create a [`TaskEvent`] with the supplied parameters.
    pub fn new<'a, OS>(start: DateTime, proj: OS, dur: Duration) -> Self
    where
        OS: Into<Option<&'a str>>
    {
        Self { start, proj: proj.into().map(ToString::to_string), dur }
    }

    /// Create a [`TaskEvent`] from the supplied entry
    pub fn from_entry(entry: &Entry) -> Self {
        Self::new(entry.date_time(), entry.project(), Duration::default())
    }

    /// Return a tuple with two [`TaskEvent`]s, split at the supplied [`Duration`].
    /// The first starts at the same starting point as this [`TaskEvent`] and
    /// stopping at the supplied [`Duration`]. The second starts [`Duration`] after
    /// the start, containg the rest of the time.
    #[rustfmt::skip]
    pub fn split(&self, dur: Duration) -> Option<(Self, Self)> {
        Some((
            Self {
                start: self.start,
                proj:  self.proj(),
                dur
            },
            Self {
                start: (self.start + dur).ok()?,
                proj:  self.proj(),
                dur:   self.dur - dur
            }
        ))
    }

    /// Return the project as a String if one exists.
    pub fn proj(&self) -> Option<String> { self.proj.as_ref().cloned() }

    /// Return the project as a String defaulting to an empty string.
    pub fn project(&self) -> String { self.proj().unwrap_or_default() }

    /// Return the hour of the start time.
    pub fn hour(&self) -> usize { self.start.hour() as usize }

    /// Return the starting [`DateTime`].
    pub fn start(&self) -> &DateTime { &self.start }

    /// Extend the [`TaskEvent`]'s duration by the supplied amount.
    pub fn add_dur(&mut self, dur: Duration) { self.dur += dur; }

    /// Return the [`Duration`] of this [`TaskEvent`].
    pub fn duration(&self) -> Duration { self.dur }

    /// Return the number of seconds this [`TaskEvent`] has lasted.
    pub fn as_secs(&self) -> u64 { self.dur.as_secs() }

    /// Return the number of seconds after the hour of the starting time.
    pub fn second_offset(&self) -> u32 { self.start.second_offset() }
}

#[cfg(test)]
mod tests {
    use spectral::prelude::*;

    use super::*;
    use crate::DateTime;

    #[test]
    fn test_new_with_project() {
        let task = TaskEvent::new(
            DateTime::new((2022, 3, 20), (12, 34, 56)).expect("Failed to create DateTime"),
            "proja",
            Duration::from_secs(305)
        );
        assert_that!(task.proj()).is_some().contains(String::from("proja").as_str());
        assert_that!(task.project()).is_equal_to(String::from("proja"));
        let expect_start =
            DateTime::new((2022, 3, 20), (12, 34, 56)).expect("Failed to create DateTime");
        assert_that!(task.start()).is_equal_to(&expect_start);
        assert_that!(task.hour()).is_equal_to(12);
        assert_that!(task.duration()).is_equal_to(&Duration::from_secs(305));
    }

    #[test]
    fn test_new_without_project() {
        let task = TaskEvent::new(
            DateTime::new((2022, 3, 20), (13, 24, 56)).expect("Failed to create DateTime"),
            None,
            Duration::from_secs(255)
        );
        assert_that!(task.proj()).is_none();
        assert_that!(task.project()).is_equal_to(String::new());
        let expect_start =
            DateTime::new((2022, 3, 20), (13, 24, 56)).expect("Failed to create DateTime");
        assert_that!(task.start()).is_equal_to(&expect_start);
        assert_that!(task.hour()).is_equal_to(13);
        assert_that!(task.duration()).is_equal_to(&Duration::from_secs(255));
    }

    #[test]
    fn test_add_dur() {
        let mut task = TaskEvent::new(
            DateTime::new((2022, 3, 20), (12, 34, 56)).expect("Failed to create DateTime"),
            "proja",
            Duration::from_secs(305)
        );

        assert_that!(task.as_secs()).named("Initial value").is_equal_to(305);

        task.add_dur(Duration::from_secs(3600));
        assert_that!(task.as_secs()).named("After add_dur").is_equal_to(3905);
        assert_that!(task.duration()).is_equal_to(Duration::from_secs(3905));
    }

    #[test]
    fn test_second_offset() {
        let task = TaskEvent::new(
            DateTime::new((2022, 3, 20), (12, 34, 56)).expect("Failed to create DateTime"),
            "proja",
            Duration::from_secs(305)
        );

        assert_that!(task.second_offset()).is_equal_to(34 * 60 + 56);
    }

    #[test]
    fn test_split() {
        let task = TaskEvent::new(
            DateTime::new((2022, 3, 20), (12, 34, 56)).expect("Failed to create DateTime"),
            "proja",
            Duration::from_secs(600)
        );

        let pair = task.split(Duration::from_secs(250));
        assert_that!(pair).is_some();
        let (first, second) = pair.unwrap();

        assert_that!(first.start()).is_equal_to(
            &DateTime::new((2022, 3, 20), (12, 34, 56)).expect("Failed to create DateTime")
        );
        assert_that!(first.duration()).is_equal_to(Duration::from_secs(250));

        assert_that!(second.start()).is_equal_to(
            &DateTime::new((2022, 3, 20), (12, 39, 6)).expect("Failed to create DateTime")
        );
        assert_that!(second.duration()).is_equal_to(Duration::from_secs(350));
    }
}
