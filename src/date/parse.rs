//! Module implementing parsing of human-readable text into a [`DateRange`].
//!
//! # Examples
//!
//! ```rust
//! use timelog::date::{DateRange, RangeParser};
//! use timelog::Result;
//!
//! # fn main() -> Result<()> {
//! let parser = RangeParser::default();
//! let range = parser.parse_from_str("last week")?;
//! print!("Dates: {} up to {}", range.start(), range.end());
//! #   Ok(())
//! # }
//! ```
//!
//! # Description
//!
//! The [`RangeParser`] converts a description of relative or absolute dates into a
//! range of dates expressed as a half-open range including the start date, but excluding
//! the end date.
//!
//! # Date Range Descriptors
//!
//! A date range descriptor can be supplied to the parser either as a &str containing the
//! descriptor or as an iterator returning separate words of the descriptor.
//!
//! The parser can handle date range descriptors of the following forms:
//!
//! A single date in one of the following forms:
//!
//! * yyyy-mm-dd
//! * today
//! * yesterday
//! * a day of the week: sunday, monday, tuesday, wednesday, ... etc.
//!
//! All but the first of the above strings parse to dates relative to the base date of the
//! parser. "Today" is the current date. "Yesterday" is the day before the current date. The
//! weekdays are the last instance of that day before today.
//!
//! This will result in a range spanning one day.
//!
//! A pair of dates in the forms above.
//!
//! This pair results in a range that covers the two supplied dates. If the dates are in order,
//! the start is the first date and end is the day after the second date. If the dates are out of
//! order, the parse will return an empty range.
//!
//! A range description in one of the following forms:
//!
//! * a month name: january, february, march, ... etc.
//! * a short (3 char) month name: jan, feb, mar, ... etc.
//! * a relative timeframe: (this|last) (week|month|year)
//! * the string "ytd"
//!
//! If the parser is given a month name, the range will cover the whole month with that name
//! before today.
//!
//! If the parser is given "this" followed by "week", "month", or "year", the resultant range
//! covers:
//!
//! * week: from the last Sunday to Saturday of this week,
//! * month: from the first day of the current month to last day of the month,
//! * year: from the January 1 of the current year to the last day of the year.
//!
//! If the parser is given "last" followed by "week", "month", or "year", the resultant range
//! covers:
//!
//! * week: from the two Sundays ago to last Saturday,
//! * month: from the first of the month before this one to the last day of that month,
//! * year: from January 1 of the year before the current one to December 31 of the same year.
//!
//! If the parser receives the string "ytd", the range will be from January 1 of the current year
//! up to today.

use chrono::Weekday;

use crate::date::{self, Date, DateError, DateRange};

/// Struct implementing the parser to generate a [`Date`] from a date description.
pub struct DateParser {
    base: Date
}

impl Default for DateParser {
    /// Return a [`DateParser`], parsing relative to today.
    fn default() -> Self { Self { base: Date::today() } }
}

impl DateParser {
    // Return a [`RangeParser`] with the specified base date.
    //
    // Explicitly not public, since it's used for testing.
    fn new(base: Date) -> Self { Self { base } }

    /// Parse the supplied string to return a [`Date`] if successful.
    ///
    /// # Errors
    ///
    /// - Return [`DateError::InvalidDate`] if the specification for a single day is not valid.
    /// - Return [`DateError::InvalidDaySpec`] if overall range specification is not valid.
    pub fn parse(&self, datespec: &str) -> date::Result<Date> {
        match datespec {
            "today" => Ok(self.base),
            "yesterday" => Ok(self.base.pred()),
            "sunday" | "monday" | "tuesday" | "wednesday" | "thursday" | "friday" | "saturday" => {
                Ok(self.base.find_previous(weekday_from_str(datespec)?))
            }
            _ => Date::try_from(datespec)
        }
    }
}

/// Struct implementing the parser to generate a [`DateRange`] from a date range description.
pub struct RangeParser {
    base: Date
}

impl Default for RangeParser {
    /// Return a [`RangeParser`], parsing relative to today.
    fn default() -> Self { Self { base: Date::today() } }
}

// Convert weekday string into appropriate Weekday variant.
fn weekday_from_str(day: &str) -> date::Result<Weekday> {
    match day {
        "sunday" => Ok(Weekday::Sun),
        "monday" => Ok(Weekday::Mon),
        "tuesday" => Ok(Weekday::Tue),
        "wednesday" => Ok(Weekday::Wed),
        "thursday" => Ok(Weekday::Thu),
        "friday" => Ok(Weekday::Fri),
        "saturday" => Ok(Weekday::Sat),
        _ => Err(DateError::InvalidDaySpec(day.to_string()))
    }
}

fn month_from_name(name: &str) -> Option<u32> {
    let month = match name {
        "january" | "jan" => 1,
        "february" | "feb" => 2,
        "march" | "mar" => 3,
        "april" | "apr" => 4,
        "may" => 5,
        "june" | "jun" => 6,
        "july" | "jul" => 7,
        "august" | "aug" => 8,
        "september" | "sep" | "sept" => 9,
        "october" | "oct" => 10,
        "november" | "nov" => 11,
        "december" | "dec" => 12,
        _ => return None
    };
    Some(month)
}

impl RangeParser {
    // Return a [`RangeParser`] with the specified base date.
    #[cfg(test)]
    pub fn new(base: Date) -> Self { Self { base } }

    /// Parse the supplied string to return a [`DateRange`] if successful.
    ///
    /// # Errors
    ///
    /// - Return [`DateError::InvalidDate`] if the specification for a single day is not valid.
    /// - Return [`DateError::InvalidDaySpec`] if overall range specification is not valid.
    pub fn parse_from_str(&self, datespec: &str) -> date::Result<DateRange> {
        if datespec.is_empty() { return Ok(self.base.into()); }
        let mut iter = datespec.split_ascii_whitespace();
        self.parse(&mut iter).map(|(r, _)| r)
    }

    /// Parse the tokens from the supplied string iterator to return a tuple containing
    /// a [`DateRange`] and the last unused token if successful.
    ///
    /// # Errors
    ///
    /// - Return [`DateError::InvalidDate`] if the specification for a single day is not valid.
    /// - Return [`DateError::InvalidDaySpec`] if overall range specification is not valid.
    pub fn parse<'a, I>(&self, datespec: &mut I) -> date::Result<(DateRange, &'a str)>
    where
        I: Iterator<Item = &'a str>
    {
        let Some(token) = datespec.next() else {
            return Ok((self.base.into(), ""));
        };
        let ltoken = token.to_ascii_lowercase();
        // Parse month name
        if let Some(range) = self.month_range(ltoken.as_str()) {
            return Ok((range, ""));
        }

        let base = self.base;
        let range_opt = match ltoken.as_str() {
            "ytd" => {
                let start = base.year_start();
                DateRange::new_opt(start, base.succ())
            }
            "this" => {
                let Some(token) = datespec.next() else {
                    return Err(DateError::InvalidDaySpec(token.into()));
                };
                let ltoken = token.to_ascii_lowercase();
                match ltoken.as_str() {
                    "week" => DateRange::new_opt(base.week_start(), base.week_end().succ()),
                    "month" => DateRange::new_opt(base.month_start(), base.month_end().succ()),
                    "year" => DateRange::new_opt(base.year_start(), base.year_end().succ()),
                    _ => return Err(DateError::InvalidDaySpec(token.into()))
                }
            }
            "last" => {
                let Some(token) = datespec.next() else {
                    return Err(DateError::InvalidDate);
                };
                let ltoken = token.to_ascii_lowercase();
                match ltoken.as_str() {
                    "week" => {
                        let date = base.week_start();
                        DateRange::new_opt(date.pred().week_start(), date)
                    }
                    "month" => {
                        let date = base.month_start().pred().month_start();
                        DateRange::new_opt(date, date.month_end().succ())
                    }
                    "year" => {
                        let date = Date::new(base.year() - 1, 1, 1)?;
                        DateRange::new_opt(date, date.year_end().succ())
                    }
                    _ => return Err(DateError::InvalidDaySpec(token.into()))
                }
            }
            _ => None
        };

        if let Some(date_range) = range_opt {
            return Ok((date_range, ""));
        }

        // Parse one or two dates
        let dparser = DateParser::new(self.base);
        let Ok(start) = dparser.parse(&ltoken) else {
            return Ok((self.base.into(), token));
        };
        if let Some(token) = datespec.next() {
            let ltoken = token.to_ascii_lowercase();
            if let Ok(end) = dparser.parse(&ltoken) {
                let range =
                    DateRange::new_opt(start, end.succ()).ok_or(DateError::WrongDateOrder)?;
                return Ok((range, ""));
            }
            else {
                return Ok((start.into(), token));
            }
        }

        Ok((start.into(), ""))
    }

    // Utility method to parse a month name
    fn month_range(&self, token: &str) -> Option<DateRange> {
        let month = month_from_name(token)?;
        let this = self.base.month();
        let year = self.base.year();
        let year = if month < this { year } else { year - 1 };

        let start = Date::new(year, month, 1).ok()?;
        Some(DateRange { start, end: start.month_end().succ() })
    }
}

#[cfg(test)]
mod tests {
    use once_cell::sync::Lazy;
    use spectral::prelude::*;

    use super::*;
    use crate::date::{DateTime, Weekday};

    static BASE_DATE: Lazy<Date> = Lazy::new(
        || Date::new(2022, 11, 15).unwrap() // tuesday
    );

    // DateParser tests

    #[test]
    fn test_date_parse_today() {
        let p = DateParser::default();
        assert_that!(p.parse("today"))
            .is_ok()
            .is_equal_to(&Date::today());
    }

    #[test]
    fn test_date_parse_yesterday() {
        let p = DateParser::default();
        assert_that!(p.parse("yesterday"))
            .is_ok()
            .is_equal_to(&Date::today().pred());
    }

    #[test]
    fn test_date_parse_weekdays() {
        let max_dur = DateTime::days(7);
        #[rustfmt::skip]
        let days: [&str; 7] = [
            "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"
        ];
        let today = Date::today();
        let midnight = Date::today().day_end();
        let p = DateParser::default();
        days.iter().for_each(|&day| {
            let date = p.parse(day);
            assert_that!(date).named(day).is_ok().is_less_than(&today);

            let end_of_date = date.unwrap().day_end();
            assert_that!(midnight - end_of_date)
                .named(day)
                .is_ok()
                .is_less_than_or_equal_to(&max_dur);
        });
    }

    // RangeParser tests

    fn test_range_parser() -> RangeParser { RangeParser::new(*BASE_DATE) }

    #[test]
    fn test_parse_default() {
        let p = RangeParser::default();
        let expect: DateRange = DateRange::default();
        assert_that!(p.parse_from_str(""))
            .is_ok()
            .is_equal_to(&expect);
    }

    #[test]
    fn test_parse_new() {
        let p = test_range_parser();
        let expect: DateRange = (*BASE_DATE).into();
        assert_that!(p.parse_from_str(""))
            .is_ok()
            .is_equal_to(&expect);
    }

    // Single day parses

    #[test]
    fn test_parse_today() {
        let p = test_range_parser();
        let expect: DateRange = (*BASE_DATE).into();
        assert_that!(p.parse_from_str("today"))
            .is_ok()
            .is_equal_to(&expect);
    }

    #[test]
    fn test_parse_yesterday() {
        let p = test_range_parser();
        let expect: DateRange =
            Date::new(2022, 11, 14).expect("Hardcoded date must work").into();

        assert_that!(p.parse_from_str("yesterday"))
            .is_ok()
            .is_equal_to(&expect);
    }

    #[test]
    fn test_parse_date() {
        let p = RangeParser::default();
        let expect: DateRange =
            Date::new(2022, 10, 20).expect("Hardcoded date must work").into();

        assert_that!(p.parse_from_str("2022-10-20"))
            .is_ok()
            .is_equal_to(&expect);
    }

    #[test]
    fn test_parse_dayname() {
        let p = test_range_parser();
        assert_that!(p.parse_from_str("monday"))
            .is_ok()
            .is_equal_to(&BASE_DATE.find_previous(Weekday::Mon).into());
    }

    #[test]
    fn test_parse_later_dayname() {
        let p = test_range_parser();
        assert_that!(p.parse_from_str("wednesday"))
            .is_ok()
            .is_equal_to(&BASE_DATE.find_previous(Weekday::Wed).into());
    }

    // two date parses

    #[test]
    fn test_dates_both_dates() {
        let expected = DateRange::new(
            Date::new(2021, 12, 1).unwrap(),
            Date::new(2021, 12, 8).unwrap()
        );

        let p = test_range_parser();
        assert_that!(p.parse_from_str("2021-12-01 2021-12-07"))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_dates_both_dates_desc() {
        let expected = DateRange::new(
            Date::new(2022, 11, 13).unwrap(),
            BASE_DATE.succ()
        );

        let p = test_range_parser();
        assert_that!(p.parse_from_str("sunday today"))
            .is_ok()
            .is_equal_to(&expected);
    }

    // relative range parses

    #[test]
    fn test_month_name() {
        let tests = [
            ("january", Date::new(2022, 1, 1), Date::new(2022, 2, 1)),
            ("jan", Date::new(2022, 1, 1), Date::new(2022, 2, 1)),
            ("february", Date::new(2022, 2, 1), Date::new(2022, 3, 1)),
            ("feb", Date::new(2022, 2, 1), Date::new(2022, 3, 1)),
            ("march", Date::new(2022, 3, 1), Date::new(2022, 4, 1)),
            ("mar", Date::new(2022, 3, 1), Date::new(2022, 4, 1)),
            ("april", Date::new(2022, 4, 1), Date::new(2022, 5, 1)),
            ("apr", Date::new(2022, 4, 1), Date::new(2022, 5, 1)),
            ("may", Date::new(2022, 5, 1), Date::new(2022, 6, 1)),
            ("june", Date::new(2022, 6, 1), Date::new(2022, 7, 1)),
            ("jun", Date::new(2022, 6, 1), Date::new(2022, 7, 1)),
            ("july", Date::new(2022, 7, 1), Date::new(2022, 8, 1)),
            ("jul", Date::new(2022, 7, 1), Date::new(2022, 8, 1)),
            ("august", Date::new(2022, 8, 1), Date::new(2022, 9, 1)),
            ("aug", Date::new(2022, 8, 1), Date::new(2022, 9, 1)),
            ("september", Date::new(2022, 9, 1), Date::new(2022, 10, 1)),
            ("sep", Date::new(2022, 9, 1), Date::new(2022, 10, 1)),
            ("october", Date::new(2022, 10, 1), Date::new(2022, 11, 1)),
            ("oct", Date::new(2022, 10, 1), Date::new(2022, 11, 1)),
            ("november", Date::new(2021, 11, 1), Date::new(2021, 12, 1)),
            ("nov", Date::new(2021, 11, 1), Date::new(2021, 12, 1)),
            ("december", Date::new(2021, 12, 1), Date::new(2022, 1, 1)),
            ("dec", Date::new(2021, 12, 1), Date::new(2022, 1, 1))
        ];

        let p = test_range_parser();
        for (name, start_opt, end_opt) in tests.iter() {
            let start = start_opt.as_ref().unwrap();
            let end = end_opt.as_ref().unwrap();
            let expected = DateRange::new(*start, *end);
            assert_that!(p.parse_from_str(name))
                .named(&name)
                .is_ok()
                .is_equal_to(&expected);
        }
    }

    #[test]
    fn test_this_week() {
        let expected = DateRange::new(
            Date::new(2022, 11, 13).unwrap(),
            Date::new(2022, 11, 20).unwrap()
        );

        let p = test_range_parser();
        assert_that!(p.parse_from_str("this week"))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_this_month() {
        let expected = DateRange::new(
            Date::new(2022, 11, 1).unwrap(),
            Date::new(2022, 12, 1).unwrap()
        );

        let p = test_range_parser();
        assert_that!(p.parse_from_str("this month"))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_this_year() {
        let expected = DateRange::new(
            Date::new(2022, 1, 1).unwrap(),
            Date::new(2023, 1, 1).unwrap()
        );

        let p = test_range_parser();
        assert_that!(p.parse_from_str("this year"))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_ytd() {
        let expected = DateRange::new(Date::new(2022, 1, 1).unwrap(), BASE_DATE.succ());

        let p = test_range_parser();
        assert_that!(p.parse_from_str("ytd"))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_last_week() {
        let expected = DateRange::new(
            Date::new(2022, 11, 6).unwrap(),
            Date::new(2022, 11, 13).unwrap()
        );

        let p = test_range_parser();
        assert_that!(p.parse_from_str("last week"))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_last_month() {
        let expected = DateRange::new(
            Date::new(2022, 10, 1).unwrap(),
            Date::new(2022, 11, 1).unwrap()
        );

        let p = test_range_parser();
        assert_that!(p.parse_from_str("last month"))
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_last_year() {
        let expected = DateRange::new(
            Date::new(2021, 1, 1).unwrap(),
            Date::new(2022, 1, 1).unwrap()
        );

        let p = test_range_parser();
        assert_that!(p.parse_from_str("last year"))
            .is_ok()
            .is_equal_to(&expected);
    }
}
