use thiserror;

/// Errors related to dates and times.
#[derive(thiserror::Error, Debug, PartialEq, Eq)]
pub enum DateError {
    /// Invalid day specification
    #[error("'{0}' is not a valid day specification")]
    InvalidDaySpec(String),

    /// Invalid stamp format on an event line.
    #[error("Invalid stamp format.")]
    InvalidDate,

    /// Entry line is encountered that is before a previous entry line.
    #[error("Entries out of order.")]
    EntryOrder,

    /// Start date after end date.
    #[error("Start date after end date.")]
    WrongDateOrder
}
