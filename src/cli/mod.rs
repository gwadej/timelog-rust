//! Support for accessing the timelog logic from a tool
//!
//! # Examples
//!
//! ```rust,no_run
//! use clap::Parser;
//!
//! use timelog::Cli;
//!
//! # fn main() {
//! let cli = Cli::parse();
//! let _ = cli.run();
//! #  }
//! ```
//!
//! # Description
//!
//! The [`Cli`] struct handling the command line processing for the main program.
use std::iter::once;
use std::num::NonZeroU32;
use std::path::PathBuf;
use std::result;

use clap::{Args, Parser, Subcommand};
use once_cell::sync::Lazy;
use regex::Regex;

// Utility type for a vector of strings
#[derive(Args, Default)]
struct VecString {
    args: Vec<String>
}

// Utility type for an optional string
#[derive(Args, Default)]
struct OptString {
    arg: Option<String>
}

use crate::config::{Config, DEFAULT_CONF};
#[doc(inline)]
use crate::error::Error;
#[doc(inline)]
use crate::error::PathError;

pub mod args;
pub mod cmd;

pub use args::DateRangeArgs;
pub use args::FilterArgs;

/// Name of the program binary.
const BIN_NAME: &str = "rtimelog";

/// Regular expression for matching the alias template match
static SUBST_RE: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"\{\}").expect("Template pattern must be correct"));

// Enumeration for determining whether to expand aliases.
#[derive(Clone, Copy)]
enum ExpandAlias {
    Yes,
    No
}

// Help message explaining a task description
const TASK_DESC: &str = "The command takes a 'task description' consisting of an optional project \
                         formatted with a leading '+', an optional task name formatted with a \
                         leading '@', and potentially more text adding details to the task. If no \
                         task name starting with '@' is supplied, any extra text is treated as \
                         the task.";

// Help message explaining the format of an event description.
const DATE_DESC: &str = "The 'date range description' consists of a single date string or a pair \
                         of date strings of the form 'YYYY-MM-DD', or one of a set of relative \
                         date strings including: today, yesterday, sunday, monday, tuesday, \
                         wednesday, thursday, friday, or saturday. The first two are obvious. The \
                         others refer to the previous instance of that day. The range can also be \
                         described by a month name (like january), the string 'ytd', or a range \
                         specified by 'this' or 'last' followed by 'week', 'month', or 'year'.";

static FULL_DESC: Lazy<String> = Lazy::new(|| format!("{TASK_DESC}\n\n{DATE_DESC}"));

/// Specify the stack subcommands supported by the program.
#[derive(Subcommand)]
enum StackCommands {
    /// Clear all of the items from the stack.
    Clear,

    /// Drop items from stack.
    Drop {
        /// Number of items to drop from stack.
        ///
        /// If an positive integer is supplied, that number of items is dropped from the top of the
        /// stack. Otherwise, drop only the top item.
        #[arg(name = "num", default_value = "1")]
        num: NonZeroU32
    },

    /// Remove all except the top number of items from the stack.
    Keep {
        /// Number of items to keep on stack.
        ///
        /// If an positive integer is supplied, that number of items are kept on the top of stack,
        /// discarding the rest. Otherwise, drop all but the top 10 items
        #[arg(name = "num", default_value = "10")]
        num: NonZeroU32
    },

    /// Display items on the stack.
    Ls,

    /// Display just the top item on the stack.
    Top
}

/// Specify the task subcommands supported by the program.
#[derive(Subcommand)]
enum EntryCommands {
    /// Discard the most recent entry.
    Discard,

    /// Reset the time of the most recent entry to now.
    Now,

    /// Return to the previous task, setting the current entry as ignored.
    Ignore,

    /// Rewrite the most recent entry.
    #[command(after_help = TASK_DESC)]
    Rewrite {
        #[arg(name = "task_desc")]
        task: Vec<String>
    },

    /// Set the time on the most recent entry
    Was {
        /// The time to use for the last entry either as "hh:mm" or "hh:mm:ss".
        time: String
    },

    /// Shift the time on the most recent entry back the specified number of minutes.
    Rewind {
        /// Positive number of minutes
        minutes: NonZeroU32
    }
}

/// Specify the stack subcommands supported by the program.
#[derive(Subcommand)]
enum ReportCommands {
    /// Display a report for the specified days and projects.
    #[command(after_help = DATE_DESC)]
    Detail {
        /// Projects to report
        #[arg(name = "proj", short, long = "proj")]
        projs: Vec<String>,

        /// Date range specification
        #[arg(name = "date_range")]
        dates: Vec<String>
    },

    /// Display a summary of the appropriate days' projects.
    #[command(after_help = DATE_DESC)]
    Summary {
        /// Projects to report
        #[arg(name = "proj", short, long = "proj")]
        projs: Vec<String>,

        /// Date range specification
        #[arg(name = "date_range")]
        dates: Vec<String>
    },

    /// Display the hours worked for each of the appropriate days and projects.
    #[command(after_help = DATE_DESC)]
    Hours {
        /// Projects to report
        #[arg(name = "proj", short, long = "proj")]
        projs: Vec<String>,

        /// Date range specification
        #[arg(name = "date_range")]
        dates: Vec<String>
    },

    /// Display the zero duration events for each of the appropriate days and projects.
    #[command(after_help = DATE_DESC)]
    Events {
        /// Boolean option for a more compact format.
        #[arg(short)]
        compact: bool,

        /// Projects to report
        #[arg(name = "proj", short, long = "proj")]
        projs: Vec<String>,

        /// Date range specification
        #[arg(name = "date_range")]
        dates: Vec<String>
    },

    /// Display the intervals between zero duration events for each of the appropriate days and
    /// projects.
    #[command(after_help = DATE_DESC)]
    Intervals {
        /// Projects to report
        #[arg(name = "proj", short, long = "proj")]
        projs: Vec<String>,

        /// Date range specification
        #[arg(name = "date_range")]
        dates: Vec<String>
    },

    /// Display a chart of the hours worked for each of the appropriate days and projects.
    #[command(after_help = DATE_DESC)]
    Chart {
        /// Date range specification
        #[arg(name = "date_range")]
        dates: Vec<String>
    }
}

/// Specify the subcommands supported by the program.
#[derive(Subcommand)]
enum Subcommands {
    /// Create the timelog directory and configuration.
    Init {
        /// Directory for logging the task events and stack. Default to `~/timelog` if not
        /// supplied.
        #[arg(name = "dir")]
        dir: Option<String>
    },

    /// Start timing a new task.
    ///
    /// Stop timing the current task (if any) and start timing a new task.
    #[command(after_help = TASK_DESC)]
    Start {
        #[arg(name = "task_desc")]
        task: Vec<String>
    },

    /// Stop timing the current task.
    Stop,

    /// Save the current task and start timing a new task.
    ///
    /// This command works the same as the `start` command, except that the
    /// current task description is saved on the top of the stack. This makes
    /// resuming the previous task easier.
    #[command(after_help = TASK_DESC)]
    Push {
        #[arg(name = "task_desc")]
        task: Vec<String>
    },

    /// Stop last task and restart top task on stack.
    Resume,

    /// Save the current task and stop timing.
    Pause,

    /// Pop the top task description on the stack and push the current task.
    Swap,

    /// List entries for the specified day. Default to today.
    // #[command(arg(name = "date_desc"))]
    #[command(after_help = DATE_DESC)]
    Ls {
        #[arg(name = "date_desc")]
        date: Option<String>
    },

    /// Add a comment line.
    Comment(VecString),

    /// Add a zero duration event
    Event(VecString),

    /// List known projects.
    Lsproj,

    /// Open the timelog file in the current editor.
    Edit,

    /// Display the current task.
    Curr,

    /// Check the logfile for problems.
    Check,

    /// Archive the first year from the timelog file, as long as it isn't the current year.
    Archive,

    /// List the aliases from the config file.
    Aliases,

    /// Reporting commands
    #[command(subcommand)]
    Report(ReportCommands),

    /// Stack specific commands
    #[command(subcommand)]
    Stack(StackCommands),

    /// Entry specific commands
    #[command(subcommand)]
    Entry(EntryCommands),

    // `external_subcommand` tells clap to put
    // all the extra arguments into this Vec
    #[command(external_subcommand)]
    Other(Vec<String>)
}

/// Specify all of the command line parameters supported by the program.
#[derive(Parser)]
#[command(author, name = "rtimelog", version, about, long_about = None, after_help = FULL_DESC.as_str())]
pub struct Cli {
    /// Specify a directory for logging the task events and stack.
    #[arg(long, name = "dir")]
    dir: Option<PathBuf>,

    /// Specify the editor to use for modifying events.
    #[arg(long)]
    editor: Option<PathBuf>,

    /// Specify the path to the configuration file.
    #[arg(long, name = "filepath")]
    conf: Option<PathBuf>,

    /// Specify the command to execute the browser.
    #[arg(long)]
    browser: Option<String>,

    /// Sub-commands which determine what actions to take
    #[command(subcommand)]
    cmd: Option<Subcommands>
}

impl Cli {
    /// Execute the action specified on the command line.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FilenameMissing`] if no configuration file is known.
    /// - Return [`PathError::InvalidPath`] if the timelog directory is not a valid path.
    /// - Return [`PathError::FilenameMissing`] if no editor has been configured.
    /// - Return other errors specific to the commands.
    pub fn run(&self) -> crate::Result<()> {
        let config = self.config()?;
        match &self.cmd {
            Some(cmd) => cmd.run(&config, ExpandAlias::Yes),
            None => Subcommands::default_command(&config).run(&config, ExpandAlias::No)
        }
    }

    // Execute the action built from alias
    //
    // # Errors
    //
    // - Return [`PathError::FilenameMissing`] if no configuration file is known.
    // - Return [`PathError::InvalidPath`] if the timelog directory is not a valid path.
    // - Return [`PathError::FilenameMissing`] if no editor has been configured.
    // - Return other errors specific to the commands.
    fn run_alias(&self, config: &Config, expand: ExpandAlias) -> crate::Result<()> {
        match &self.cmd {
            Some(cmd) => cmd.run(config, expand),
            None => Subcommands::default_command(config).run(config, ExpandAlias::No)
        }
    }

    // Retrieve the configuration from the file.
    //
    // # Errors
    //
    // - Return [`PathError::FilenameMissing`] if no configuration file is known.
    // - Return [`PathError::InvalidPath`] if the timelog directory is not a valid path.
    // - Return [`PathError::FilenameMissing`] if no editor has been configured.
    fn config(&self) -> result::Result<Config, PathError> {
        let mut config = match &self.conf {
            Some(conf_file) => {
                Config::from_file(conf_file.to_str().ok_or(PathError::FilenameMissing)?)
            }
            None => Config::from_file(&DEFAULT_CONF)
        }
        .unwrap_or_default();

        if let Some(dir) = &self.dir {
            config.set_dir(
                dir.to_str()
                    .ok_or_else(|| PathError::InvalidPath(String::new(), String::new()))?
            );
        }
        if let Some(editor) = &self.editor {
            config.set_editor(editor.to_str().ok_or(PathError::FilenameMissing)?);
        }
        if let Some(browser) = &self.browser {
            config.set_browser(browser);
        }
        Ok(config)
    }
}

impl Subcommands {
    /// Execute the action associated with the current variant.
    ///
    /// # Errors
    ///
    /// - Return [`Error`]s for particular commands failing.
    /// - Return [`Error::InvalidCommand`] if the subcommand is not recognized and no alias
    /// matches.
    pub fn run(&self, config: &Config, expand: ExpandAlias) -> crate::Result<()> {
        use Subcommands as C;

        match &self {
            C::Init { dir } => Ok(cmd::initialize(config, dir)?),
            C::Start { task } => cmd::start_task(config, task),
            C::Stop => cmd::stop_task(config),
            C::Comment(VecString { args }) => cmd::add_comment(config, args),
            C::Event(VecString { args }) => cmd::add_event(config, args),
            C::Push { task } => cmd::push_task(config, task),
            C::Resume => cmd::resume_task(config),
            C::Pause => cmd::pause_task(config),
            C::Swap => cmd::swap_entry(config),
            C::Ls { date } => cmd::list_entries(config, date),
            C::Lsproj => cmd::list_projects(config),
            C::Edit => cmd::edit(config),
            C::Curr => cmd::current_task(config),
            C::Check => cmd::check_logfile(config),
            C::Archive => cmd::archive_year(config),
            C::Aliases => {
                cmd::list_aliases(config);
                Ok(())
            }
            C::Report(cmd) => cmd.run(config),
            C::Stack(cmd) => cmd.run(config),
            C::Entry(cmd) => cmd.run(config),
            C::Other(args) => match expand {
                ExpandAlias::Yes => Self::expand_alias(config, args),
                ExpandAlias::No => Err(Error::InvalidCommand(args[0].clone()))
            }
        }
    }

    fn default_command(config: &Config) -> Self {
        match config.defcmd() {
            "init" => Self::Init { dir: None },
            "start" => Self::Start { task: Vec::new() },
            "stop" => Self::Stop,
            "push" => Self::Push { task: Vec::new() },
            "resume" => Self::Resume,
            "pause" => Self::Pause,
            "swap" => Self::Swap,
            "ls" => Self::Ls { date: None },
            "lsproj" => Self::Lsproj,
            "edit" => Self::Edit,
            "curr" => Self::Curr,
            "archive" => Self::Archive,
            "aliases" => Self::Aliases,
            _ => Self::Curr
        }
    }

    // Replace an alias as the first argument with the definition of the alias.
    fn expand_alias(config: &Config, args: &[String]) -> crate::Result<()> {
        let alias = &args[0];
        let mut args_iter = args[1..].iter().map(String::as_str);

        let expand: Vec<String> = config
            .alias(alias)
            .ok_or_else(|| Error::InvalidCommand(alias.clone()))?
            .split(' ')
            .map(|w| {
                if SUBST_RE.is_match(w) {
                    args_iter.next().map_or_else(
                        || w.to_string(),
                        |val| SUBST_RE.replace(w, val).into_owned()
                    )
                }
                else {
                    w.to_string()
                }
            })
            .collect();

        let cmd = Cli::parse_from(
            once(BIN_NAME)
                .chain(expand.iter().map(String::as_str))
                .chain(args_iter)
        );
        cmd.run_alias(config, ExpandAlias::No)
    }
}

impl StackCommands {
    /// Execute the action associated with the current variant.
    ///
    /// # Errors
    ///
    /// - Return [`Error`]s for particular commands failing.
    /// - Return [`Error::InvalidCommand`] if the subcommand is not recognized and no alias
    /// matches.
    pub fn run(&self, config: &Config) -> crate::Result<()> {
        use StackCommands as SC;
        match &self {
            SC::Clear => cmd::stack_clear(config),
            SC::Drop { num } => cmd::stack_drop(config, *num),
            SC::Keep { num } => cmd::stack_keep(config, *num),
            SC::Ls => cmd::list_stack(config),
            SC::Top => cmd::stack_top(config)
        }
    }
}

impl EntryCommands {
    /// Execute the action associated with the current variant.
    ///
    /// # Errors
    ///
    /// - Return [`Error`]s for particular commands failing.
    /// - Return [`Error::InvalidCommand`] if the subcommand is not recognized and no alias
    /// matches.
    pub fn run(&self, config: &Config) -> crate::Result<()> {
        use EntryCommands as EC;
        match &self {
            EC::Discard => cmd::discard_last_entry(config),
            EC::Now => cmd::reset_last_entry(config),
            EC::Ignore => cmd::ignore_last_entry(config),
            EC::Rewrite { task } => cmd::rewrite_last_entry(config, task),
            EC::Was { time } => cmd::retime_last_entry(config, time),
            EC::Rewind { minutes } => cmd::rewind_last_entry(config, *minutes)
        }
    }
}

impl ReportCommands {
    /// Execute the action associated with the current variant.
    ///
    /// # Errors
    ///
    /// - Return [`Error`]s for particular commands failing.
    /// - Return [`Error::InvalidCommand`] if the subcommand is not recognized and no alias
    /// matches.
    pub fn run(&self, config: &Config) -> crate::Result<()> {
        use ReportCommands as RC;
        match &self {
            RC::Detail { projs, dates } => cmd::report_daily(config, dates, projs),
            RC::Summary { projs, dates } => cmd::report_summary(config, dates, projs),
            RC::Hours { projs, dates } => cmd::report_hours(config, dates, projs),
            RC::Events { compact, projs, dates } => {
                cmd::report_events(config, dates, projs, *compact)
            }
            RC::Intervals { projs, dates } => cmd::report_intervals(config, dates, projs),
            RC::Chart { dates } => cmd::chart_daily(config, dates)
        }
    }
}
