//! Implementations of the commands run by the CLI

use std::collections::HashSet;
use std::fs;
use std::io;
use std::iter::once;
use std::num::NonZeroU32;
use std::process::{Command, Stdio};
use std::result;

use xml::writer::{EmitterConfig, XmlEvent};

#[doc(inline)]
use crate::archive::Archiver;
use crate::buf_reader;
#[doc(inline)]
use crate::cli::args::DateRangeArgs;
use crate::cli::args::DayFilter;
#[doc(inline)]
use crate::cli::args::FilterArgs;
use crate::config::Config;
#[doc(inline)]
use crate::date::{Date, DateTime, Time};
use crate::day::format_dur;
#[doc(inline)]
use crate::day::Day;
use crate::emit_xml;
#[doc(inline)]
use crate::entry::{Entry, EntryKind, PROJECT_RE};
#[doc(inline)]
use crate::error::Error;
#[doc(inline)]
use crate::error::PathError;
#[doc(inline)]
use crate::logfile::Logfile;
#[doc(inline)]
use crate::stack::Stack;
#[doc(inline)]
use crate::task_line_iter::TaskLineIter;

/// The style definition for the chart HTML.
const STYLESHEET: &str = r"
    .day {
      display: grid;
      grid-template-columns: 25% 1fr;
      grid-template-rows: 4em auto;
      padding-left: 0.5em;
      padding-bottom: 2em;
    }
    .day:nth-child(even) { background-color: #eee; }
    .day + .day {
      border-top: 1px solid black;
    }
    .tasks {
      display: grid;
      grid-template-columns: auto auto auto;
      grid-template-rows: repeat(auto-file);
    }
    @media screen and (min-width:400px) {
      .day { grid-template-columns: auto auto; }
      .tasks { grid-template-columns: auto; }
    }
    @media screen and (min-width:1024px) {
      .day { grid-template-columns: 40% 1fr; }
      .tasks { grid-template-columns: auto auto; }
    }
    @media screen and (min-width:1250px) {
      .day { grid-template-columns: 30% 1fr; }
      .tasks { grid-template-columns: auto auto auto; }
    }
    @media screen and (min-width:1400px) {
      .day { grid-template-columns: 25% 1fr; }
      .tasks { grid-template-columns: auto auto auto auto; }
    }
    .day .project {
      grid-column: 1;
      grid-row: 1 / span 2;
    }
    .day .tasks {
      grid-column: 2;
      grid-row: 2;
    }
    .project h2 { margin-bottom: 2em; }
    .piechart {
      display: grid;
      grid-template-columns: auto 1fr;
      grid-template-rows: auto;
    }
    .piechart .pie { grid-column: 1; grid-row: 1; }
    .piechart table { grid-column: 2; grid-row: 1; }
    .hours { grid-column: 1 / span 2; grid-row: 2; }
    .hours h3 { margin-left: 5em; margin-bottom: 0.5ex; }
    table.legend { margin-left: 0.75em; margin-bottom: auto; }
    .legend span { margin-left: 0.25em; }
    .legend td {
        display: flex;
        align-items: center;
    }
";

// Utility function to return the current stack file.
//
// # Errors
//
// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
fn stack(config: &Config) -> crate::Result<Stack> { Ok(Stack::new(&config.stackfile())?) }

// Utility function to return the current log file.
//
// # Errors
//
// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
fn logfile(config: &Config) -> crate::Result<Logfile> { Ok(Logfile::new(&config.logfile())?) }

/// Initialize the timelog directory supplied and create a `.timelogrc` config file.
/// If no directory is supplied default to `~/timelog`
///
/// # Errors
///
/// - Return [`PathError::CantCreatePath`] if cannot create timelog directory
/// - Return [`PathError::FileAccess`] if we are unable to write the configuration.
/// - Return [`PathError::InvalidPath`] if the path is not valid
///
/// ## Panics
///
/// If the canonicalized path cannot be converted to a string.
pub fn initialize(config: &Config, dir: &Option<String>) -> result::Result<(), PathError> {
    let mut config = config.clone();
    let dir = dir.as_ref().map(|s| s.as_str()).unwrap_or(config.dir());

    fs::create_dir_all(dir)
        .map_err(|e| PathError::CantCreatePath(dir.to_string(), e.to_string()))?;

    let candir = fs::canonicalize(dir)
        .map_err(|e| PathError::InvalidPath(dir.to_string(), e.to_string()))?;
    // Convert type
    let candir = candir.to_str().ok_or_else(|| {
        PathError::InvalidPath(dir.to_string(), String::from("Directory name not valid"))
    })?;

    config.set_dir(candir);
    config.create()?;
    Ok(())
}

/// Start a task.
/// Add an entry to the logfile marked with the current date and time and the supplied task
/// description.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
/// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
/// - Return [`PathError::FileWrite`] if the function fails to append to the file.
pub fn start_task(config: &Config, args: &[String]) -> crate::Result<()> {
    let logfile = logfile(config)?;
    Ok(logfile.add_task(&args.join(" "))?)
}

/// Stop a task.
/// Add the 'stop' entry to the logfile marked with the current date and time.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file has no filename.
/// - Return [`PathError::InvalidPath`] if the path part of log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the log file cannot be opened or created.
/// - Return [`PathError::FileWrite`] if the function fails to append to the log file.
pub fn stop_task(config: &Config) -> crate::Result<()> {
    let logfile = logfile(config)?;
    Ok(logfile.add_task("stop")?)
}

/// Add a comment line to the logfile
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file has no filename.
/// - Return [`PathError::InvalidPath`] if the path part of log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the log file cannot be opened or created.
/// - Return [`PathError::FileWrite`] if the function fails to append to the log file.
pub fn add_comment(config: &Config, args: &[String]) -> crate::Result<()> {
    let logfile = logfile(config)?;
    Ok(logfile.add_comment(&args.join(" "))?)
}

/// Add a zero duration event to the logfile
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file has no filename.
/// - Return [`PathError::InvalidPath`] if the path part of log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the log file cannot be opened or created.
/// - Return [`PathError::FileWrite`] if the function fails to append to the log file.
pub fn add_event(config: &Config, args: &[String]) -> crate::Result<()> {
    let logfile = logfile(config)?;
    Ok(logfile.add_event(&args.join(" "))?)
}

/// Start a task and saving the current entry description to the stack.
///
/// Add the task entry description to the logfile marked with the current date and time.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file or stack file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log or stack file is not a valid
/// path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
/// - Return [`PathError::FileWrite`] if the function fails to append to the file.
pub fn push_task(config: &Config, args: &[String]) -> crate::Result<()> {
    let logfile = logfile(config)?;
    let entry = logfile.last_entry()?;
    if entry.entry_text().is_empty() {
        return Ok(());
    }

    let stack = stack(config)?;
    stack.push(entry.entry_text())?;

    logfile.add_task(&args.join(" ")).map_err(Into::into)
}

/// Resume the previous task entry by popping it off the stack and starting that task at the
/// current date and time.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file or stack file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log or stack file is not a valid
/// path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
/// - Return [`PathError::FileWrite`] if the function fails to append to the file.
pub fn resume_task(config: &Config) -> crate::Result<()> {
    let stack = stack(config)?;
    if let Some(task) = stack.pop() {
        logfile(config)?.add_task(&task)?;
    }
    Ok(())
}

/// Pause the current task by placing it on the stack and stopping timing.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file or stack file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log or stack file is not a valid
/// path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
/// - Return [`PathError::FileWrite`] if the function fails to append to the file.
pub fn pause_task(config: &Config) -> crate::Result<()> { push_task(config, &["stop".to_string()]) }

/// Discard the most recent entry in the logfile.
///
/// # Errors
///
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn discard_last_entry(config: &Config) -> crate::Result<()> {
    logfile(config)?.discard_line().map_err(Into::into)
}

/// Reset the datestamp on the most recent entry to now.
///
/// # Errors
///
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
#[rustfmt::skip]
pub fn reset_last_entry(config: &Config) -> crate::Result<()> {
    logfile(config)?.reset_last_entry()
}

/// Replace the task text on the most recent entry.
///
/// # Errors
///
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn rewrite_last_entry(config: &Config, args: &[String]) -> crate::Result<()> {
    logfile(config)?.rewrite_last_entry(&args.join(" "))
}

/// Replace the task time on the most recent entry.
///
/// # Errors
///
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn retime_last_entry(config: &Config, time_arg: &str) -> crate::Result<()> {
    let time = Time::parse_from_str(time_arg, "%H:%M")
        .or_else(|_| Time::parse_from_str(time_arg, "%H:%M:%S"))
        .map_err(|_| Error::InvalidWasArgument(time_arg.to_string()))?;
    logfile(config)?.retime_last_entry(time)
}

/// Shift the time back the number of minutes on the most recent entry.
///
/// # Errors
///
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn rewind_last_entry(config: &Config, minutes: NonZeroU32) -> crate::Result<()> {
    logfile(config)?.rewind_last_entry(minutes)
}

/// Mark the most recent entry as ignored
///
/// # Errors
///
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn ignore_last_entry(config: &Config) -> crate::Result<()> {
    logfile(config)?.ignore_last_entry()
}

/// List the entries for a particular date, or today if none is supplied.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn list_entries(config: &Config, date: &Option<String>) -> crate::Result<()> {
    use std::io::BufRead;

    let file = logfile(config)?.open()?;

    let start = Date::parse(date.as_ref().unwrap_or(&String::from("today")))?;
    let end = &start.succ().to_string();
    let start = start.to_string();

    for line in TaskLineIter::new(
        io::BufReader::new(file).lines().map_while(Result::ok),
        &start,
        end
    )? {
        println!("{line}");
    }
    Ok(())
}

/// List the projects in the logfile.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn list_projects(config: &Config) -> crate::Result<()> {
    use std::io::BufRead;

    let mut projects: HashSet<String> = HashSet::new();

    let file = logfile(config)?.open()?;
    io::BufReader::new(file)
        .lines()
        .map_while(Result::ok)
        .for_each(|ln| {
            if let Some(proj) = PROJECT_RE.captures(&ln) {
                // Since we've verified the captures, I expect it's safe to get the one.
                if let Some(p) = proj.get(1) {
                    projects.insert(p.as_str().to_string());
                }
            }
        });

    let mut names: Vec<String> = projects.iter().map(ToString::to_string).collect();
    names.as_mut_slice().sort();
    for p in &names {
        println!("{p}");
    }

    Ok(())
}

/// List the items on the stack, most recent first.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log or stack file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log or stack file is not a valid
/// path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn list_stack(config: &Config) -> crate::Result<()> {
    let stackfile = stack(config)?;
    if !stackfile.exists() { return Ok(()); }

    stackfile.process_down_stack(|i, ln| println!("{}) {ln}", i + 1))
}

/// Launch the configured editor to edit the logfile.
///
/// # Errors
///
/// - Return [`Error::EditorFailed`] if unable to run the editor.
pub fn edit(config: &Config) -> crate::Result<()> {
    Command::new(config.editor())
        .arg(config.logfile())
        .status()
        .map_err(|e| Error::EditorFailed(config.editor().to_string(), e.to_string()))?;

    Ok(())
}

// Utility function for creating a day and initializing it if a previous entry was still open.
fn start_day(start: &str, prev: &Option<Entry>) -> crate::Result<Day> {
    let mut day = Day::new(start)?;
    if let Some(p) = prev.as_ref() {
        day.start_day(p)?;
    }
    Ok(day)
}

// Generate a report from the entries in the logfile based on the supplied `args`.
//
// Uses the supplied filter object to apply appropriate filtering conditions that
// select the entries of interest from the logfile. These entries are collected into
// [`Day`]s which are then reported using the supplied function `f`.
//
// The purpose of this method is to handle all of the boring grunt-work of finding the
// entries in question and collecting them together to generate one or more daily reports.
//
// # Errors
//
// - Return [`PathError::FilenameMissing`] if the log file is missing.
// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
// - Return [`PathError::FileAccess`] if the file cannot be opened.
// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
// - Return [`Error::DateError`] if the start date is not before the end date
fn report<F>(config: &Config, filter: &dyn DayFilter, mut f: F) -> crate::Result<()>
where
    F: FnMut(&Day) -> crate::Result<()>
{
    let start = filter.start();

    let file = logfile(config)?.open()?;
    let mut prev: Option<Entry> = TaskLineIter::new(buf_reader(file), &start, &filter.end())?
        .last_line_before()
        .and_then(|l| Entry::from_line(&l).ok())
        .map(|e| e.to_day_end());

    let mut day = start_day(&start, &prev)?;

    let file = logfile(config)?.open()?;
    for line in TaskLineIter::new(buf_reader(file), &start, &filter.end())? {
        let entry = Entry::from_line(&line)?;
        let stamp = entry.stamp();

        if day.date_stamp() != stamp {
            // Deal with end of day
            if !day.is_complete() {
                if let Some(prev_entry) = prev {
                    let day_end = prev_entry.to_day_end();
                    day.add_entry(day_end.clone())?;
                    prev = Some(day_end);
                }
            }
            if let Some(filtered) = filter.filter_day(day) {
                f(&filtered)?;
            }

            day = start_day(&stamp, &prev)?;
        }
        day.add_entry(entry.clone())?;
        prev = Some(entry);
    }
    day.finish()?;

    if let Some(filtered) = filter.filter_day(day) {
        f(&filtered)?;
    }
    Ok(())
}

// Return a file object for the report file.
fn report_file(filename: &str) -> crate::Result<fs::File> {
    Ok(fs::OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open(filename)
        .map_err(|e| PathError::FileAccess(filename.to_string(), e.to_string()))?)
}

// Display the chart in the configured browser.
fn launch_chart(config: &Config, filename: &str) -> crate::Result<()> {
    Command::new(config.browser())
        .arg(filename)
        .stderr(Stdio::null()) // discard output, for odd chromium message
        .status()
        .map_err(|e| Error::EditorFailed(config.browser().to_string(), e.to_string()))?;
    Ok(())
}

/// Create a graphical chart for each supplied day.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
/// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
/// - Return [`Error::DateError`] if the start date is not before the end date
pub fn chart_daily(config: &Config, dates: &[String]) -> crate::Result<()> {
    let filename = config.reportfile();
    let mut file = report_file(&filename)?;
    let mut w = EmitterConfig::new()
        .perform_indent(true)
        .write_document_declaration(false)
        .create_writer(&mut file);
    emit_xml!(&mut w, html => {
        emit_xml!(w, head => {
            emit_xml!(w, title; "Daily Timelog Report")?;
            emit_xml!(w, style, type: "text/css"; STYLESHEET)
        })?;
        emit_xml!(w, body => {
            report(config, &DateRangeArgs::new(dates)?, |day|
                day.daily_chart().write(&mut w)
            )
        })
    })?;

    launch_chart(config, &filename)
}

/// Print the full daily report for each supplied day.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
/// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
/// - Return [`Error::DateError`] if the start date is not before the end date
pub fn report_daily(config: &Config, dates: &[String], projs: &[String]) -> crate::Result<()> {
    report(config, &FilterArgs::new(dates, projs)?, |day| {
        print!("{}", day.detail_report());
        Ok(())
    })
}

/// Print the summary report for each supplied day.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
/// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
/// - Return [`Error::DateError`] if the start date is not before the end date
pub fn report_summary(config: &Config, dates: &[String], projs: &[String]) -> crate::Result<()> {
    report(config, &FilterArgs::new(dates, projs)?, |day| {
        print!("{}", day.summary_report());
        Ok(())
    })
}

/// Print the hourly report for each supplied day.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
/// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
/// - Return [`Error::DateError`] if the start date is not before the end date
pub fn report_hours(config: &Config, dates: &[String], projs: &[String]) -> crate::Result<()> {
    report(config, &FilterArgs::new(dates, projs)?, |day| {
        print!("{}", day.hours_report());
        Ok(())
    })
}

/// Print a report of the zero duration events for each supplied day.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
/// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
/// - Return [`Error::DateError`] if the start date is not before the end date
pub fn report_events(
    config: &Config, dates: &[String], projs: &[String], compact: bool
) -> crate::Result<()> {
    report(config, &FilterArgs::new(dates, projs)?, |day| {
        print!("{}", day.event_report(compact));
        Ok(())
    })
}

/// Print a report of intervals between the zero duration events for each supplied day.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
/// - Return [`Error::BadProjectFilter`] if the supplied project Regexes are not valid
/// - Return [`Error::DateError`] if the start date is not before the end date
pub fn report_intervals(config: &Config, dates: &[String], projs: &[String]) -> crate::Result<()> {
    let mut events: Vec<Entry> = vec![];
    let filter = FilterArgs::new(dates, projs)?;
    report(config, &filter, |day| {
        events.append(&mut day.events().cloned().collect());
        Ok(())
    })?;
    if events.len() >= 2 {
        let now = Entry::new_marked("now", DateTime::now(), EntryKind::Event);
        for (first, second) in events.iter().zip(events[1..].iter().chain(once(&now))) {
            #[rustfmt::skip]
            println!("{} {} : {}",
                first.timestamp(),
                first.entry_text(),
                format_dur(&(second.date_time() - first.date_time())?)
            );
        }
    }
    Ok(())
}

/// Display the current task
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the log file is missing.
/// - Return [`PathError::InvalidPath`] if the path part of the log file is not a valid path.
/// - Return [`PathError::FileAccess`] if the file cannot be opened.
pub fn current_task(config: &Config) -> crate::Result<()> {
    let entry = logfile(config)?.last_entry()?;
    if entry.is_stop() {
        println!("Not in entry.");
    }
    else {
        println!("{}", &entry);
        let dur = (DateTime::now() - entry.date_time())?;
        println!("Duration: {}", format_dur(&dur));
    }

    Ok(())
}

/// Archive the first year from the logfile (if not the current year).
///
/// # Errors
///
/// - Return [`Error::PathError`] for any error accessing the log or archive files.
pub fn archive_year(config: &Config) -> crate::Result<()> {
    match Archiver::new(config).archive()? {
        None => println!("Nothing to archive"),
        Some(year) => println!("{year} archived")
    }
    Ok(())
}

/// List the aliases from the config file.
pub fn list_aliases(config: &Config) {
    let mut aliases: Vec<&str> = config.alias_names().map(String::as_str).collect();
    aliases.as_mut_slice().sort();
    let maxlen: usize = aliases.iter().map(|a| a.len()).max().unwrap_or_default();

    println!("Aliases:");
    for alias in aliases {
        if let Some(value) = config.alias(alias) {
            println!("  {1:0$} : {2}", &maxlen, &alias, value);
        }
    }
}

/// Check the logfile for any problems
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
/// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
pub fn check_logfile(config: &Config) -> crate::Result<()> {
    let problems = logfile(config)?.problems();

    if problems.is_empty() {
        println!("No problems found");
    }
    else {
        for p in problems {
            println!("{p}");
        }
    }

    Ok(())
}

/// Swap the current task with the top item on the stack.
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
/// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
pub fn swap_entry(config: &Config) -> crate::Result<()> {
    let stack = stack(config)?;
    let logfile = logfile(config)?;
    if let Some(curr_task) = logfile.last_line() {
        if let Some(task) = stack.pop() {
            logfile.add_task(&task)?;
        }
        let entry = Entry::from_line(&curr_task)?;
        if !entry.is_stop() {
            stack.push(entry.entry_text())?;
        }
    }
    Ok(())
}

/// Remove all but the top items on the stack
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
/// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
pub fn stack_keep(config: &Config, num: NonZeroU32) -> crate::Result<()> {
    let stack = stack(config)?;
    stack.keep(num.get())
}

/// Clear the stack
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
/// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
pub fn stack_clear(config: &Config) -> crate::Result<()> {
    let stack = stack(config)?;
    stack.clear().map_err(Into::into)
}

/// Clear the stack
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
/// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
pub fn stack_drop(config: &Config, num: NonZeroU32) -> crate::Result<()> {
    let stack = stack(config)?;
    stack.drop(num.get())
}

/// Display the top item on the stack
///
/// # Errors
///
/// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
/// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
pub fn stack_top(config: &Config) -> crate::Result<()> {
    let stack = stack(config)?;
    println!("{}", stack.top()?);
    Ok(())
}
