//! Configuration file definition
//!
//! # Examples
//!
//! ```rust
//! use timelog::{Config, Entry, Result};
//!
//! # fn main() -> Result<()> {
//! # let events: Vec<Entry> = vec![];
//! # let mut event_iter = events.into_iter();
//! let conf = Config::from_file("~/.timelogrc")?;
//!
//! println!("Dir: {}", conf.dir());
//! println!("Logfile: {}", conf.logfile());
//! println!("Editor: {}", conf.editor());
//! #   Ok(())
//! #  }
//! ```
//!
//! # Description
//!
//! The [`Config`] struct represents the configuration file information.

use std::collections::HashMap;
use std::env;
use std::fmt::Debug;
use std::fs;
use std::io::prelude::*;

use configparser::ini::Ini;
use once_cell::sync::Lazy;
use tilde_expand::tilde_expand;

#[doc(inline)]
use crate::error::PathError;

/// Default path to the timelog config file
pub static DEFAULT_CONF: Lazy<String> =
    Lazy::new(|| normalize_path("~/.timelogrc").expect("Default config file name must be valid"));
/// Default path to the timelog log directory
pub static DEFAULT_DIR: Lazy<String> =
    Lazy::new(|| normalize_path("~/timelog").expect("Default log directory must be valid"));
/// Default command to open the editor
pub static DEFAULT_EDITOR: Lazy<String> = Lazy::new(|| {
    env::var("VISUAL")
        .or_else(|_| env::var("EDITOR"))
        .unwrap_or_else(|_| String::from("vim"))
});

/// Default command to open the browser
#[cfg(target_os = "macos")]
pub const DEFAULT_BROWSER: &str = "open";

/// Default command to open the browser
#[cfg(not(target_os = "macos"))]
pub const DEFAULT_BROWSER: &str = "chromium";

/// Type specifying the configuration for the timelog program.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Config {
    /// The name of the configuration file
    configfile: String,
    /// The path of the directory that stores the log and stack files
    dir:        String,
    /// The path to the editor used with the `edit` command
    editor:     String,
    /// The path to the browser used with the `chart` command
    browser:    String,
    /// The default command if none is entered
    defcmd:     String,
    /// List of aliases to extend the commands
    aliases:    HashMap<String, String>
}

/// Utility function to handle tilde expansion of a path.
///
/// # Errors
///
/// - Return a [`PathError::InvalidPath`] if there are invalid characters in the path
pub fn normalize_path(filename: &str) -> Result<String, PathError> {
    // If this fails, the user directory must contain invalid characters.
    String::from_utf8(tilde_expand(filename.as_bytes()))
        .map_err(|e| PathError::InvalidPath(filename.to_string(), e.to_string()))
}

// Ensure the filename either exists or could be created in the supplied directory
//
// # Errors
//
// - Return [`PathError::FilenameMissing`] if the filename does not exist.
// - Return [`PathError::InvalidPath`] if the dirname portion of the file is not valid.
fn ensure_filename(file: &str) -> Result<String, PathError> {
    use std::path::PathBuf;

    if file.is_empty() {
        return Err(PathError::FilenameMissing);
    }
    let mut dir = PathBuf::from(normalize_path(file)?);
    let filename = dir
        .file_name()
        .ok_or(PathError::FilenameMissing)?
        .to_os_string();
    dir.pop();

    let mut candir = fs::canonicalize(dir)
        .map_err(|e| PathError::InvalidPath(file.to_string(), e.to_string()))?;
    candir.push(filename);
    Ok(candir.to_str().expect("The config filename must be a valid string").to_string())
}

impl Default for Config {
    /// Create a [`Config`] with all of the default parameters.
    ///
    /// ## Panics
    ///
    /// If any of the default valures are not legal in the current system, the code will
    /// panic. These is a programmer-specified defaults, and should never fail.
    fn default() -> Self {
        Self {
            configfile: normalize_path("~/.timelogrc").expect("Invalid config file"),
            dir:        normalize_path("~/timelog").expect("Invalid user dir"),
            editor:     "vim".into(),
            browser:    DEFAULT_BROWSER.into(),
            defcmd:     "curr".into(),
            aliases:    HashMap::new()
        }
    }
}

// Utility function for extracting conf data.
fn conf_get<'a>(base: &'a HashMap<String, Option<String>>, key: &'static str) -> Option<&'a str> {
    base.get(key).and_then(Option::as_deref)
}

impl Config {
    /// Create a new [`Config`] object with supplied parameters
    ///
    /// # Errors
    ///
    /// - Return [`PathError::InvalidConfigPath`] if the configuration filename is not valid.
    /// - Return [`PathError::InvalidTimelogPath`] if the timelog directory is not valid.
    pub fn new(
        config: &str, dir: Option<&str>, editor: Option<&str>, browser: Option<&str>,
        cmd: Option<&str>
    ) -> crate::Result<Self> {
        let dir = dir.unwrap_or("~/timelog");
        Ok(Self {
            configfile: normalize_path(config).map_err(|_| PathError::InvalidConfigPath)?,
            dir:        normalize_path(dir).map_err(|_| PathError::InvalidTimelogPath)?,
            editor:     editor.unwrap_or("vim").into(),
            browser:    browser.unwrap_or(DEFAULT_BROWSER).into(),
            defcmd:     cmd.unwrap_or("curr").into(),
            aliases:    HashMap::new()
        })
    }

    /// Create a new [`Config`] object from the supplied config file
    ///
    /// # Errors
    ///
    /// - Return [`PathError::InvalidConfigPath`] if the configuration filename is not valid.
    /// - Return [`PathError::InvalidTimelogPath`] if the timelog directory is not valid.
    /// - Return [`PathError::FileAccess`] if the config file is not accessible.
    pub fn from_file(filename: &str) -> crate::Result<Self> {
        let configfile = ensure_filename(filename)?;
        let mut parser = Ini::new();
        let config = parser
            .load(&configfile)
            .map_err(|e| PathError::FileAccess(configfile.clone(), e))?;

        let default = HashMap::new();
        let base = config.get("default").unwrap_or(&default);
        let mut conf = Config::new(
            &configfile,
            conf_get(base, "dir"),
            conf_get(base, "editor"),
            conf_get(base, "browser"),
            conf_get(base, "defcmd")
        )?;
        if let Some(aliases) = config.get("alias") {
            for (k, v) in aliases.iter() {
                if let Some(val) = v.as_ref() {
                    conf.set_alias(k, val);
                }
            }
        }

        Ok(conf)
    }

    /// Return a [`String`] containing the name of the config file.
    pub fn configfile(&self) -> &str { self.configfile.as_str() }

    /// Return the path of the directory that stores the log and stack files
    pub fn dir(&self) -> &str { self.dir.as_str() }

    /// Set the path of the directory that stores the log and stack files
    pub fn set_dir(&mut self, dir: &str) { self.dir = dir.to_string() }

    /// Return the path to the editor used with the `edit` command
    pub fn editor(&self) -> &str { self.editor.as_str() }

    /// Set the path to the editor used with the `edit` command
    pub fn set_editor(&mut self, editor: &str) { self.editor = editor.to_string() }

    /// The path to the browser used with the `chart` command
    pub fn browser(&self) -> &str { self.browser.as_str() }

    /// The path to the browser used with the `chart` command
    pub fn set_browser(&mut self, browser: &str) { self.browser = browser.to_string() }

    /// Return the default command if none is entered
    pub fn defcmd(&self) -> &str { self.defcmd.as_str() }

    /// The file containing the timelog entries
    pub fn logfile(&self) -> String { format!("{}/timelog.txt", self.dir) }

    /// The file that holds the stack
    pub fn stackfile(&self) -> String { format!("{}/stack.txt", self.dir) }

    /// The file containing the timelog entries
    pub fn reportfile(&self) -> String { format!("{}/report.html", self.dir) }

    /// Write the configuration to disk in the file specified by the [`Config`]
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if we are unable to write the configuration.
    pub fn create(&self) -> Result<(), PathError> {
        let configfile = self.configfile();
        let file = fs::OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(configfile)
            .map_err(|e| PathError::FileAccess(configfile.to_string(), e.to_string()))?;
        let mut stream = std::io::BufWriter::new(file);

        writeln!(&mut stream, "dir={}", self.dir())
            .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;
        writeln!(&mut stream, "editor={}", self.editor())
            .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;
        writeln!(&mut stream, "browser={}", self.browser())
            .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;
        writeln!(&mut stream, "defcmd={}", self.defcmd())
            .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;

        writeln!(&mut stream, "\n[alias]")
            .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;
        for (key, val) in &self.aliases {
            writeln!(stream, "    {key} = {val}")
                .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;
        }

        stream
            .flush()
            .map_err(|e| PathError::FileWrite(configfile.to_string(), e.to_string()))?;
        Ok(())
    }

    /// Return an iterator over the alias names
    pub fn alias_names(&self) -> impl Iterator<Item = &'_ String> { self.aliases.keys() }

    /// Retrieve the value associate with the named alias, if one exists.
    pub fn alias(&self, key: &str) -> Option<&str> { self.aliases.get(key).map(String::as_str) }

    /// Set the value of the named alias
    fn set_alias(&mut self, name: &str, val: &str) {
        self.aliases.insert(name.to_string(), val.to_string());
    }
}

#[cfg(test)]
mod tests {
    use std::fs::File;

    use spectral::prelude::*;
    use tempfile::TempDir;

    use super::*;

    #[test]
    fn test_default() {
        let config = Config::default();
        assert_that!(config.dir()).is_equal_to(&*normalize_path("~/timelog").unwrap());
        assert_that!(config.logfile())
            .is_equal_to(&normalize_path("~/timelog/timelog.txt").unwrap());
        assert_that!(config.stackfile())
            .is_equal_to(&normalize_path("~/timelog/stack.txt").unwrap());
        assert_that!(config.reportfile())
            .is_equal_to(&normalize_path("~/timelog/report.html").unwrap());
        assert_that!(config.editor()).is_equal_to("vim");
        assert_that!(config.browser()).is_equal_to(DEFAULT_BROWSER);
        assert_that!(config.defcmd()).is_equal_to("curr");
        assert_that!(config.alias_names().count()).is_equal_to(0);
    }

    #[test]
    fn test_new() {
        #[rustfmt::skip]
        let config = Config::new(
            "~/.timelogrc", Some("~/timelog"), Some("vim"), Some("chromium"), Some("curr")
        ).expect("Failed to create config");

        assert_that!(config.dir()).is_equal_to(&*normalize_path("~/timelog").unwrap());
        assert_that!(config.logfile())
            .is_equal_to(&normalize_path("~/timelog/timelog.txt").unwrap());
        assert_that!(config.stackfile())
            .is_equal_to(&normalize_path("~/timelog/stack.txt").unwrap());
        assert_that!(config.reportfile())
            .is_equal_to(&normalize_path("~/timelog/report.html").unwrap());
        assert_that!(config.editor()).is_equal_to("vim");
        assert_that!(config.browser()).is_equal_to("chromium");
        assert_that!(config.defcmd()).is_equal_to("curr");
        assert_that!(config.alias_names().count()).is_equal_to(0);
    }

    #[test]
    fn test_from_file_dir_only() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let path = tmpdir.path();
        let path_str = path.to_str().unwrap();

        let filename = format!("{path_str}/.timerc");
        let mut file = File::create(&filename).unwrap();
        let _ = file.write_all(format!("dir = {path_str}").as_bytes());

        let config = Config::from_file(&filename).expect("Failed to create config from file");
        let expect_log = normalize_path(format!("{path_str}/timelog.txt").as_str())
            .expect("Failed to create logfile name");
        let expect_stack = normalize_path(format!("{path_str}/stack.txt").as_str())
            .expect("Failed to create stackfile name");
        let expect_report = normalize_path(format!("{path_str}/report.html").as_str())
            .expect("Failed to create report file name");
        assert_that!(config.dir()).is_equal_to(path_str);
        assert_that!(config.logfile()).is_equal_to(&expect_log);
        assert_that!(config.stackfile()).is_equal_to(&expect_stack);
        assert_that!(config.reportfile()).is_equal_to(&expect_report);
        assert_that!(config.editor()).is_equal_to("vim");
        assert_that!(config.browser()).is_equal_to(DEFAULT_BROWSER);
        assert_that!(config.defcmd()).is_equal_to("curr");
        assert_that!(config.alias_names().count()).is_equal_to(0);
    }

    #[test]
    fn test_from_file_base() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let path = tmpdir.path();
        let path_str = path.to_str().unwrap();

        let filename = format!("{path_str}/.timerc");
        let mut file = File::create(&filename).unwrap();
        #[rustfmt::skip]
        let output = format!("dir={path_str}\neditor=nano\nbrowser=firefox\ndefcmd=stop");
        let _ = file.write_all(output.as_bytes());

        let config = Config::from_file(&filename).expect("Failed to create config");
        let expect_log = normalize_path(format!("{path_str}/timelog.txt").as_str())
            .expect("Failed to create logfile name");
        let expect_stack = normalize_path(format!("{path_str}/stack.txt").as_str())
            .expect("Failed to create stackfile name");
        let expect_report = normalize_path(format!("{path_str}/report.html").as_str())
            .expect("Failed to create report file name");
        assert_that!(config.dir()).is_equal_to(&*normalize_path(path_str).unwrap());
        assert_that!(config.logfile()).is_equal_to(&expect_log);
        assert_that!(config.stackfile()).is_equal_to(&expect_stack);
        assert_that!(config.reportfile()).is_equal_to(&expect_report);
        assert_that!(config.editor()).is_equal_to("nano");
        assert_that!(config.browser()).is_equal_to("firefox");
        assert_that!(config.defcmd()).is_equal_to("stop");
        assert_that!(config.alias_names().count()).is_equal_to(0);
    }

    #[test]
    fn test_from_file_aliases() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let path = tmpdir.path();
        let path_str = path.to_str().unwrap();

        let filename = format!("{path_str}/.timerc");
        let mut file = File::create(&filename).unwrap();
        let _ = file.write_all(b"[alias]\na=start +play @A\nb=start +work @B");

        let config = Config::from_file(&filename).expect("Failed to create config");
        let mut names: Vec<&String> = config.alias_names().collect();
        names.sort();
        assert_that!(names).is_equal_to(vec![&"a".to_string(), &"b".to_string()]);
        assert_that!(config.alias("a"))
            .is_some()
            .is_equal_to("start +play @A");
        assert_that!(config.alias("b"))
            .is_some()
            .is_equal_to("start +work @B");
    }
}
