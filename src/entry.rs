//! Module representing an entry in the timelog.
//!
//! # Examples
//!
//! ```rust
//! use timelog::entry::Entry;
//! use std::fs::File;
//! use std::io::{BufRead, BufReader};
//!
//! fn day_entrys(date: &str, file: &mut File) -> Vec<Entry> {
//!     let mut reader = BufReader::new(file);
//!     reader.lines()
//!           .filter_map(|line| Entry::from_line(&line.ok()?).ok())
//!           .filter(|ev| ev.stamp() == String::from(date))
//!           .collect::<Vec<Entry>>()
//! }
//! ```
//!
//! # Description
//!
//! Objects of this type represent the individual lines in the `timelog.txt` file.
//! Each [`Entry`] has a date and time stamp, an optional project, and a task.

use std::fmt::{self, Debug, Display};

use once_cell::sync::Lazy;
use regex::Regex;

const STOP_CMD: &str = "stop";

// These should not be able to fail, hardcoded input strings.
// Still using expect() in case the regex strings ever get changed.

/// Regular expression to match a time stamp
static TIMESTAMP_RE: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"\A(\d{4}[-/](?:0[1-9]|1[0-2])[-/](?:0[1-9]|[12][0-9]|3[01]) (?:[01][0-9]|2[0-3]):[0-5][0-9]:[0-6][0-9])").expect("Date time Regex failed.")
});
/// A somewhat lax regular expression to match an entry line.
static LAX_LINE_RE: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"\A(\d{4}[-/](?:0[1-9]|1[0-2])[-/](?:0[1-9]|[12][0-9]|3[01]) (?:[01][0-9]|2[0-3]):[0-5][0-9]:[0-6][0-9])(.)(.+)").expect("Entry line Regex failed.")
});
/// A regular expression matching the project part of an entry line.
pub static PROJECT_RE: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"\+(\S+)").expect("Entry project regex failed."));
/// A regular expression matching the task part of an entry line.
static TASKNAME_RE: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"@(\S+)").expect("Task name Regex failed."));
/// A regular expression matching a stop line
static STOP_LINE: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"\A(\d{4}[-/](?:0[1-9]|1[0-2])[-/](?:0[1-9]|[12][0-9]|3[01]) (?:[01][0-9]|2[0-3]):[0-5][0-9]:[0-6][0-9]) stop").expect("Stop line Regex failed")
});
/// Regular expression matching the year portion of an entry line.
pub static YEAR_RE: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"^(\d\d\d\d)").expect("Date regex failed"));
/// Regular expression extracting the marker from the line.
pub static MARKER_RE: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"^\d{4}-\d\d-\d\d \d\d:\d\d:\d\d(.)").expect("Marker regex failed"));

#[doc(inline)]
use crate::date::{Date, DateTime};

pub mod error;
pub mod kind;

/// Errors associated with the entry.
pub use error::EntryError;
/// The kind of entry
pub use kind::EntryKind;

/// Representation of an entry in the log
///
/// Objects of this type represent individual lines in the `timelog.txt` file.
/// Each [`Entry`] has a date and time stamp, an optional project, and a task.
#[derive(Debug, Clone, Eq, PartialEq)]
#[must_use]
pub struct Entry {
    /// Time that this entry began
    time:    DateTime,
    /// An optional project name
    project: Option<String>,
    /// The text of the entry from the entry line
    text:    String,
    /// The type of the entry
    kind:    EntryKind
}

/// # Line parsing tools
impl Entry {
    /// Parse the entry line text into the task name and detail parts if they exist.
    pub fn task_breakdown(entry_text: &str) -> (Option<String>, Option<String>) {
        if entry_text.is_empty() {
            return (None, None);
        }

        let task = PROJECT_RE.replace(entry_text, "").trim().to_string();
        if let Some(caps) = TASKNAME_RE.captures(&task) {
            if let Some(tname) = caps.get(1) {
                let detail = TASKNAME_RE.replace(&task, "").trim().to_string();
                let tname = tname.as_str().to_string();
                return (Some(tname), (!detail.is_empty()).then_some(detail));
            }
        }
        (None, (!task.is_empty()).then_some(task))
    }

    /// Return `true` if the supplied string looks like a stop line.
    pub fn is_stop_line(line: &str) -> bool { STOP_LINE.is_match(line) }

    /// Extract a date/time string from a task line
    pub fn datetime_from_line(line: &str) -> Option<&str> {
        if line.is_empty() || Self::is_comment_line(line) {
            return None;
        }

        if let Some(caps) = LAX_LINE_RE.captures(line) {
            return caps.get(1).map(|s| s.as_str());
        }
        None
    }

    /// Extract a date string from a task line
    pub fn date_from_line(line: &str) -> Option<&str> {
        Self::datetime_from_line(line).and_then(|s| s.split_whitespace().next())
    }

    /// Return the year for the supplied entry line, if any.
    pub fn extract_year(line: &str) -> Option<i32> {
        if Self::is_comment_line(line) {
            return None;
        }

        YEAR_RE
            .captures(line)
            .and_then(|cap| cap[0].parse::<i32>().ok())
    }

    /// Return `true` if the supplied line is a comment.
    pub fn is_comment_line(line: &str) -> bool { line.starts_with('#') }
}

/// # Constructors
impl Entry {
    /// Create a new [`Entry`] representing the supplied task at the supplied time.
    pub fn new(entry_text: &str, time: DateTime) -> Self {
        Self::new_marked(entry_text, time, EntryKind::Start)
    }

    /// Create a new [`Entry`] representing the supplied task at the supplied time and optional
    /// mark.
    pub fn new_marked(entry_text: &str, time: DateTime, kind: EntryKind) -> Self {
        let kind = if kind == EntryKind::Start && entry_text == STOP_CMD {
            EntryKind::Stop
        }
        else {
            kind
        };
        let oproject = PROJECT_RE.captures(entry_text)
            .and_then(|caps| caps.get(1).map(|m| String::from(m.as_str())));
        Self { time, project: oproject, text: entry_text.into(), kind }
    }

    /// Create a new [`Entry`] representing a stop entry for the supplied [`DateTime`]
    pub fn new_stop(time: DateTime) -> Self { Self::new_marked(STOP_CMD, time, EntryKind::Stop) }

    /// Create a new [`Entry`] representing the entry from the supplied line.
    ///
    /// This entry must be formatted as described in Format.md.
    ///
    /// # Errors
    ///
    /// Return an [`EntryError`] if the line is empty or formatted incorrectly.
    pub fn from_line(line: &str) -> Result<Self, EntryError> {
        if line.is_empty() {
            return Err(EntryError::BlankLine);
        }

        match LAX_LINE_RE.captures(line) {
            Some(caps) => {
                let Some(stamp) = caps.get(1) else { return Err(EntryError::InvalidTimeStamp); };
                let Ok(time) = DateTime::try_from(stamp.as_str()) else {
                    return Err(EntryError::InvalidTimeStamp);
                };
                let kind = EntryKind::try_new(caps.get(2).and_then(|m| m.as_str().chars().next()))?;
                Ok(Entry::new_marked(
                    caps.get(3).map_or("", |m| m.as_str()),
                    time,
                    kind
                ))
            }
            None => Err(if TIMESTAMP_RE.is_match(line) {
                EntryError::MissingTask
            }
            else {
                EntryError::InvalidTimeStamp
            })
        }
    }
}

/// # Accessors
impl Entry {
    /// Return the [`&str`] designated as the project, if any, from the [`Entry`].
    pub fn project(&self) -> Option<&str> { self.project.as_deref() }

    /// Return the [`&str`] containing all of the [`Entry`] except the time and date.
    pub fn entry_text(&self) -> &str { &self.text }

    /// Return the [`String`] containing all of the [`Entry`] except the time and date.
    pub fn task(&self) -> Option<String> { Self::task_breakdown(&self.text).0 }

    /// Return the [`String`] containing all of the [`Entry`] except the time and date.
    pub fn detail(&self) -> Option<String> { Self::task_breakdown(&self.text).1 }

    /// Return the [`String`] containing all of the [`Entry`] except the time and date.
    pub fn task_and_detail(&self) -> (Option<String>, Option<String>) {
        Self::task_breakdown(&self.text)
    }

    /// Return the time for the start of the [`Entry`] in epoch seconds.
    pub fn epoch(&self) -> i64 { self.time.timestamp() }

    /// Return the date for the start of the [`Entry`] as a [`Date`]
    pub fn date(&self) -> Date { self.time.date() }

    /// Return the time for the start of the [`Entry`] as a [`DateTime`]
    pub fn date_time(&self) -> DateTime { self.time }

    /// Return the time for the start of the [`Entry`] as a [`DateTime`]
    #[rustfmt::skip]
    pub fn timestamp(&self) -> String {
        format!("{} {:02}:{:02}", self.time.date(), self.time.hour(), self.time.minute())
    }

    /// Return the date stamp of the [`Entry`] in 'YYYY-MM-DD' format.
    pub fn stamp(&self) -> String { self.date().to_string() }

    /// Return `true` if this a start [`Entry`].
    pub fn is_start(&self) -> bool { self.kind == EntryKind::Start }

    /// Return `true` if this was a stop [`Entry`].
    pub fn is_stop(&self) -> bool { self.kind == EntryKind::Stop }

    /// Return `true` if this was an ignored [`Entry`].
    pub fn is_ignore(&self) -> bool { self.kind == EntryKind::Ignored }

    /// Return an ignored [`Entry`] converted from this one.
    pub fn ignore(&self) -> Self { Self { kind: EntryKind::Ignored, ..self.clone() } }

    /// Return `true` if this was a event [`Entry`].
    pub fn is_event(&self) -> bool { self.kind == EntryKind::Event }
}

/// # Mutators
impl Entry {
    /// Return a new copy of the current [`Entry`] with the date and time
    /// reset the the supplied value.
    pub fn change_date_time(&self, date_time: DateTime) -> Self {
        let mut entry = self.clone();
        entry.time = date_time;
        entry
    }

    /// Return a new copy of the current [`Entry`] with the date and time
    /// reset the the supplied value.
    pub fn change_text(&self, task: &str) -> Self {
        if self.is_stop() { return self.clone(); }
        Self::new_marked(task, self.time, self.kind)
    }

    /// Return a new [`Entry`] timestamped as the end of this date
    pub fn to_day_end(&self) -> Self { Self { time: self.date().day_end(), ..self.clone() } }
}

impl Display for Entry {
    /// Format the [`Entry`] formatted as described in Format.md.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mark = match self.kind {
            EntryKind::Ignored => '!',
            EntryKind::Event => '^',
            _ => ' '
        };
        write!(f, "{}{mark}{}", self.time, self.text)
    }
}

impl PartialOrd for Entry {
    /// This method returns an ordering between self and other values if one exists.
    #[rustfmt::skip]
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.time.cmp(&other.time)
            .then_with(|| self.text.cmp(&other.text)))
    }
}

#[cfg(test)]
mod tests {
    use spectral::prelude::*;

    use super::*;

    const CANONICAL_LINE: &str = "2013-06-05 10:00:02 +proj1 @do something";
    const IGNORED_LINE: &str = "2013-06-05 10:00:02!+proj1 @do something";
    const EVENT_LINE: &str = "2013-06-05 10:00:02^+proj1 @do something";
    const STOP_LINE: &str = "2013-06-05 10:00:02 stop";

    fn reference_time() -> i64 { DateTime::new((2013, 6, 5), (10, 0, 2)).unwrap().timestamp() }

    #[test]
    fn from_line_error_if_empty() {
        assert_that!(Entry::from_line("")).is_err_containing(EntryError::BlankLine);
    }

    #[test]
    #[rustfmt::skip]
    fn is_comment_on_comment() {
        assert_that!(Entry::is_comment_line("# Random comment")).is_true();
        assert_that!(Entry::is_comment_line("#2013-06-05 10:00:02 +test @Commented")).is_true();
    }

    #[test]
    #[rustfmt::skip]
    fn is_comment_on_empty() {
        assert_that!(Entry::is_comment_line("")).is_false();
    }

    #[test]
    #[rustfmt::skip]
    fn is_comment_on_entry() {
        assert_that!(Entry::is_comment_line("2013-06-05 10:00:02 +test @Commented")).is_false();
    }

    #[test]
    #[rustfmt::skip]
    fn test_datetime_from_empty_line() {
        assert_that!(Entry::datetime_from_line("")).is_none()
    }

    #[test]
    #[rustfmt::skip]
    fn test_datetime_from_commented_line() {
        assert_that!(Entry::datetime_from_line("# Random comment")).is_none();
        assert_that!(Entry::datetime_from_line("#2013-06-05 10:00:02 +test @Commented")).is_none()
    }

    #[test]
    #[rustfmt::skip]
    fn test_datetime_from_line() {
        assert_that!(Entry::datetime_from_line(CANONICAL_LINE)).contains("2013-06-05 10:00:02");
    }

    #[test]
    #[rustfmt::skip]
    fn test_datetime_from_ignored_line() {
        assert_that!(Entry::datetime_from_line(IGNORED_LINE)).contains("2013-06-05 10:00:02");
    }

    #[test]
    #[rustfmt::skip]
    fn test_date_from_empty_line() {
        assert_that!(Entry::date_from_line("")).is_none()
    }

    #[test]
    #[rustfmt::skip]
    fn test_date_from_commented_line() {
        assert_that!(Entry::date_from_line("# Random comment")).is_none();
        assert_that!(Entry::date_from_line("#2013-06-05 10:00:02 +test @Commented")).is_none()
    }

    #[test]
    fn test_date_from_line() {
        assert_that!(Entry::date_from_line(CANONICAL_LINE)).contains("2013-06-05");
    }

    #[test]
    fn test_date_from_ignored_line() {
        assert_that!(Entry::date_from_line(IGNORED_LINE)).contains("2013-06-05");
    }

    #[test]
    fn from_line_error_if_not_entry() {
        assert_that!(Entry::from_line("This is not an entry"))
            .is_err_containing(EntryError::InvalidTimeStamp);
    }

    #[test]
    fn from_line_canonical_entry() {
        let entry = Entry::from_line(CANONICAL_LINE).unwrap();
        assert_that!(&entry.stamp()).is_equal_to(&String::from("2013-06-05"));
        assert_that!(&entry.project()).contains_value("proj1");
        assert_that!(&entry.entry_text()).is_equal_to("+proj1 @do something");
        assert_that!(&entry.task()).contains_value(String::from("do"));
        assert_that!(&entry.detail()).contains_value(String::from("something"));
        assert_that!(&entry.task_and_detail())
            .is_equal_to((Some(String::from("do")), Some(String::from("something"))));
        assert_that!(&entry.epoch()).is_equal_to(&reference_time());
        assert_that!(&entry.to_string().as_str()).is_equal_to(&CANONICAL_LINE);
        assert_that!(&entry.is_stop()).is_false();
        assert_that!(&entry.is_ignore()).is_false();
        assert_that!(&entry.is_event()).is_false();
    }

    #[test]
    fn new_canonical_entry() {
        let canonical_time = DateTime::try_from("2013-06-05 10:00:02").unwrap();
        let entry = Entry::new("+proj1 @do something", canonical_time);
        assert_that!(&entry.stamp()).is_equal_to(String::from("2013-06-05"));
        assert_that!(&entry.project()).contains_value("proj1");
        assert_that!(&entry.entry_text()).is_equal_to("+proj1 @do something");
        assert_that!(&entry.task()).contains_value(String::from("do"));
        assert_that!(&entry.detail()).contains_value(String::from("something"));
        assert_that!(&entry.task_and_detail())
            .is_equal_to((Some(String::from("do")), Some(String::from("something"))));
        assert_that!(&entry.epoch()).is_equal_to(&reference_time());
        assert_that!(&entry.to_string().as_str()).is_equal_to(&CANONICAL_LINE);
        assert_that!(&entry.is_stop()).is_false();
        assert_that!(&entry.is_ignore()).is_false();
        assert_that!(&entry.is_event()).is_false();
    }

    #[test]
    fn from_line_no_task_entry() {
        const LINE: &str = "2013-06-05 10:00:02 +proj1 do something";
        let entry = Entry::from_line(LINE).unwrap();
        assert_that!(&entry.stamp()).is_equal_to(&String::from("2013-06-05"));
        assert_that!(&entry.project()).contains_value("proj1");
        assert_that!(&entry.entry_text()).is_equal_to("+proj1 do something");
        assert_that!(&entry.task()).is_none();
        assert_that!(&entry.detail()).contains_value(String::from("do something"));
        assert_that!(&entry.task_and_detail())
            .is_equal_to((None, Some(String::from("do something"))));
        assert_that!(&entry.epoch()).is_equal_to(&reference_time());
        assert_that!(&entry.to_string().as_str()).is_equal_to(&LINE);
        assert_that!(&entry.is_stop()).is_false();
        assert_that!(&entry.is_ignore()).is_false();
        assert_that!(&entry.is_event()).is_false();
    }

    #[test]
    fn from_line_no_detail_entry() {
        const LINE: &str = "2013-06-05 10:00:02 +proj1 @something";
        let entry = Entry::from_line(LINE).unwrap();
        assert_that!(&entry.stamp()).is_equal_to(&String::from("2013-06-05"));
        assert_that!(&entry.project()).contains_value("proj1");
        assert_that!(&entry.entry_text()).is_equal_to("+proj1 @something");
        assert_that!(&entry.task()).contains_value(String::from("something"));
        assert_that!(&entry.detail()).is_none();
        assert_that!(&entry.task_and_detail()).is_equal_to((Some(String::from("something")), None));
        assert_that!(&entry.epoch()).is_equal_to(&reference_time());
        assert_that!(&entry.to_string().as_str()).is_equal_to(&LINE);
        assert_that!(&entry.is_stop()).is_false();
        assert_that!(&entry.is_ignore()).is_false();
        assert_that!(&entry.is_event()).is_false();
    }

    #[test]
    fn from_line_no_entry_text() {
        const LINE: &str = "2013-06-05 10:00:02 +proj1";
        let entry = Entry::from_line(LINE).unwrap();
        assert_that!(&entry.stamp()).is_equal_to(&String::from("2013-06-05"));
        assert_that!(&entry.project()).contains_value("proj1");
        assert_that!(&entry.entry_text()).is_equal_to("+proj1");
        assert_that!(&entry.task()).is_none();
        assert_that!(&entry.detail()).is_none();
        assert_that!(&entry.task_and_detail()).is_equal_to((None, None));
        assert_that!(&entry.epoch()).is_equal_to(&reference_time());
        assert_that!(&entry.to_string().as_str()).is_equal_to(&LINE);
        assert_that!(&entry.is_stop()).is_false();
        assert_that!(&entry.is_ignore()).is_false();
        assert_that!(&entry.is_event()).is_false();
    }

    #[test]
    fn from_line_stop_entry() {
        let entry = Entry::from_line(STOP_LINE).unwrap();
        assert_that!(&entry.stamp()).is_equal_to(String::from("2013-06-05"));
        assert_that!(&entry.project()).is_none();
        assert_that!(&entry.entry_text()).is_equal_to("stop");
        assert_that!(&entry.task()).is_none();
        assert_that!(&entry.detail()).contains_value(String::from("stop"));
        assert_that!(&entry.task_and_detail()).is_equal_to((None, Some(String::from("stop"))));
        assert_that!(&entry.epoch()).is_equal_to(&reference_time());
        assert_that!(&entry.to_string().as_str()).is_equal_to(&STOP_LINE);
        assert_that!(&entry.is_stop()).is_true();
        assert_that!(&entry.is_ignore()).is_false();
        assert_that!(&entry.is_event()).is_false();
    }

    #[test]
    fn test_extract_year() {
        let line = "2018-11-20 12:34:43 +test @Event";
        assert_that!(Entry::extract_year(line)).contains(2018);
    }

    #[test]
    fn test_extract_year_fail() {
        let line = "xyzzy 2018-11-20 12:34:43 +test @Event";
        assert_that!(Entry::extract_year(line)).is_none();
    }

    #[test]
    fn new_stop_entry() {
        let canonical_time = DateTime::try_from("2013-06-05 10:00:02").unwrap();
        let entry = Entry::new("stop", canonical_time);
        assert_that!(&entry.stamp()).is_equal_to(String::from("2013-06-05"));
        assert_that!(&entry.project()).is_none();
        assert_that!(&entry.entry_text()).is_equal_to("stop");
        assert_that!(&entry.task()).is_none();
        assert_that!(&entry.detail()).contains_value(String::from("stop"));
        assert_that!(&entry.task_and_detail()).is_equal_to((None, Some(String::from("stop"))));
        assert_that!(&entry.epoch()).is_equal_to(&reference_time());
        assert_that!(&entry.to_string().as_str()).is_equal_to(&STOP_LINE);
        assert_that!(&entry.is_stop()).is_true();
        assert_that!(&entry.is_ignore()).is_false();
        assert_that!(&entry.is_event()).is_false();
    }

    #[test]
    fn from_line_ignored_entry() {
        let entry = Entry::from_line(IGNORED_LINE).unwrap();
        assert_that!(&entry.stamp()).is_equal_to(&String::from("2013-06-05"));
        assert_that!(&entry.project()).contains_value("proj1");
        assert_that!(&entry.entry_text()).is_equal_to("+proj1 @do something");
        assert_that!(&entry.task()).contains_value(String::from("do"));
        assert_that!(&entry.detail()).contains_value(String::from("something"));
        assert_that!(&entry.task_and_detail())
            .is_equal_to((Some(String::from("do")), Some(String::from("something"))));
        assert_that!(&entry.epoch()).is_equal_to(&reference_time());
        assert_that!(&entry.to_string().as_str()).is_equal_to(&IGNORED_LINE);
        assert_that!(&entry.is_stop()).is_false();
        assert_that!(&entry.is_ignore()).is_true();
        assert_that!(&entry.is_event()).is_false();
    }

    #[test]
    fn new_ignored_entry() {
        let canonical_time = DateTime::try_from("2013-06-05 10:00:02").unwrap();
        let entry = Entry::new_marked("+proj1 @do something", canonical_time, EntryKind::Ignored);
        assert_that!(&entry.stamp()).is_equal_to(String::from("2013-06-05"));
        assert_that!(&entry.project()).contains_value("proj1");
        assert_that!(&entry.entry_text()).is_equal_to("+proj1 @do something");
        assert_that!(&entry.task()).contains_value(String::from("do"));
        assert_that!(&entry.detail()).contains_value(String::from("something"));
        assert_that!(&entry.task_and_detail())
            .is_equal_to((Some(String::from("do")), Some(String::from("something"))));
        assert_that!(&entry.epoch()).is_equal_to(&reference_time());
        assert_that!(&entry.to_string().as_str()).is_equal_to(&IGNORED_LINE);
        assert_that!(&entry.is_stop()).is_false();
        assert_that!(&entry.is_ignore()).is_true();
        assert_that!(&entry.is_event()).is_false();
    }

    #[test]
    fn from_line_event_entry() {
        let entry = Entry::from_line(EVENT_LINE).unwrap();
        assert_that!(&entry.stamp()).is_equal_to(&String::from("2013-06-05"));
        assert_that!(&entry.project()).contains_value("proj1");
        assert_that!(&entry.entry_text()).is_equal_to("+proj1 @do something");
        assert_that!(&entry.task()).contains_value(String::from("do"));
        assert_that!(&entry.detail()).contains_value(String::from("something"));
        assert_that!(&entry.task_and_detail())
            .is_equal_to((Some(String::from("do")), Some(String::from("something"))));
        assert_that!(&entry.epoch()).is_equal_to(&reference_time());
        assert_that!(&entry.to_string().as_str()).is_equal_to(&EVENT_LINE);
        assert_that!(&entry.is_stop()).is_false();
        assert_that!(&entry.is_ignore()).is_false();
        assert_that!(&entry.is_event()).is_true();
    }

    #[test]
    fn new_event_entry() {
        let canonical_time = DateTime::try_from("2013-06-05 10:00:02").unwrap();
        let entry = Entry::new_marked("+proj1 @do something", canonical_time, EntryKind::Event);
        assert_that!(&entry.stamp()).is_equal_to(String::from("2013-06-05"));
        assert_that!(&entry.project()).contains_value("proj1");
        assert_that!(&entry.entry_text()).is_equal_to("+proj1 @do something");
        assert_that!(&entry.task()).contains_value(String::from("do"));
        assert_that!(&entry.detail()).contains_value(String::from("something"));
        assert_that!(&entry.task_and_detail())
            .is_equal_to((Some(String::from("do")), Some(String::from("something"))));
        assert_that!(&entry.epoch()).is_equal_to(&reference_time());
        assert_that!(&entry.to_string().as_str()).is_equal_to(&EVENT_LINE);
        assert_that!(&entry.is_stop()).is_false();
        assert_that!(&entry.is_ignore()).is_false();
        assert_that!(&entry.is_event()).is_true();
    }

    #[test]
    fn compare_entry() {
        const LINE1: &str = "2013-06-05 10:00:02 +proj1";
        const LINE2: &str = "2013-06-05 11:00:02 +proj1";
        let entry1 = Entry::from_line(LINE1).unwrap();
        let entry2 = Entry::from_line(LINE2).unwrap();
        assert_that!(entry2 > entry1).is_true();
        assert_that!(entry1 < entry2).is_true();
        assert_that!(entry1 == entry1).is_true();
    }

    #[test]
    fn test_change_date_time_start() {
        let entry = Entry::from_line("2022-12-27 10:00:00 +proj1").unwrap();
        let dt = DateTime::new((2022, 12, 27), (9, 50, 00)).unwrap();
        let new_entry = entry.change_date_time(dt);

        assert_that!(new_entry).is_not_equal_to(&entry);
        let dt = DateTime::new((2022, 12, 27), (9, 50, 00)).unwrap();
        assert_that!(new_entry.date_time()).is_equal_to(&dt);
        assert_that!(new_entry.entry_text()).is_equal_to(&entry.entry_text());
        assert_that!(new_entry.is_start()).is_true();
    }

    #[test]
    fn test_change_date_time_event() {
        let entry = Entry::from_line("2022-12-27 10:00:00^+event").unwrap();
        let dt = DateTime::new((2022, 12, 27), (9, 50, 00)).unwrap();
        let new_entry = entry.change_date_time(dt);

        assert_that!(new_entry).is_not_equal_to(&entry);
        let dt = DateTime::new((2022, 12, 27), (9, 50, 00)).unwrap();
        assert_that!(new_entry.date_time()).is_equal_to(&dt);
        assert_that!(new_entry.entry_text()).is_equal_to(&entry.entry_text());
        assert_that!(new_entry.is_event()).is_true();
    }

    #[test]
    fn test_change_date_time_ignored() {
        let entry = Entry::from_line("2022-12-27 10:00:00!+proj1").unwrap();
        let dt = DateTime::new((2022, 12, 27), (9, 50, 00)).unwrap();
        let new_entry = entry.change_date_time(dt);

        assert_that!(new_entry).is_not_equal_to(&entry);
        let dt = DateTime::new((2022, 12, 27), (9, 50, 00)).unwrap();
        assert_that!(new_entry.date_time()).is_equal_to(&dt);
        assert_that!(new_entry.entry_text()).is_equal_to(&entry.entry_text());
        assert_that!(new_entry.is_ignore()).is_true();
    }

    #[test]
    fn test_change_date_time_stop() {
        let entry = Entry::from_line("2022-12-27 10:00:00 stop").unwrap();
        let dt = DateTime::new((2022, 12, 27), (9, 50, 00)).unwrap();
        let new_entry = entry.change_date_time(dt);

        assert_that!(new_entry).is_not_equal_to(&entry);
        let dt = DateTime::new((2022, 12, 27), (9, 50, 00)).unwrap();
        assert_that!(new_entry.date_time()).is_equal_to(&dt);
        assert_that!(new_entry.entry_text()).is_equal_to(&entry.entry_text());
        assert_that!(new_entry.is_stop()).is_true();
    }

    #[test]
    fn test_change_text_start() {
        let entry = Entry::from_line("2022-12-27 10:00:00 +proj1").unwrap();
        let new_entry = entry.change_text("+proj2 @Changed");

        assert_that!(new_entry).is_not_equal_to(&entry);
        assert_that!(new_entry.date_time()).is_equal_to(&entry.date_time());
        assert_that!(new_entry.entry_text()).is_equal_to("+proj2 @Changed");
        assert_that!(new_entry.is_start()).is_true();
    }

    #[test]
    fn test_change_text_event() {
        let entry = Entry::from_line("2022-12-27 10:00:00^+event").unwrap();
        let new_entry = entry.change_text("+proj2 @Changed");

        assert_that!(new_entry).is_not_equal_to(&entry);
        assert_that!(new_entry.date_time()).is_equal_to(&entry.date_time());
        assert_that!(new_entry.entry_text()).is_equal_to("+proj2 @Changed");
        assert_that!(new_entry.is_event()).is_true();
    }

    #[test]
    fn test_change_text_ignored() {
        let entry = Entry::from_line("2022-12-27 10:00:00!+proj1").unwrap();
        let new_entry = entry.change_text("+proj2 @Changed");

        assert_that!(new_entry).is_not_equal_to(&entry);
        assert_that!(new_entry.date_time()).is_equal_to(&entry.date_time());
        assert_that!(new_entry.entry_text()).is_equal_to("+proj2 @Changed");
        assert_that!(new_entry.is_ignore()).is_true();
    }

    #[test]
    fn test_change_text_stop() {
        let entry = Entry::from_line("2022-12-27 10:00:00 stop").unwrap();
        let new_entry = entry.change_text("+proj2 @Changed");

        assert_that!(new_entry).is_equal_to(&entry);
        assert_that!(new_entry.date_time()).is_equal_to(&entry.date_time());
        assert_that!(new_entry.entry_text()).is_equal_to("stop");
        assert_that!(new_entry.is_stop()).is_true();
    }
}
