//! Representation of charts for timelog
//!
//! # Description
//!
//! Support for both pie charts (of projects and tasks in a project), and
//! a bar graph of tasks displayed hourly.

pub mod colors;
pub mod day;
pub mod histogram;
pub mod legend;
pub mod pie;
pub mod pie_data;
pub mod tag_percent;

/// The [`ColorIter`] type represents an ordered list of colors to use for
/// building a pie chart.
pub use colors::ColorIter;
/// The [`DayHours`] type holds project-based task events for building an
/// hourly bar graph of the projects worked.
pub use day::DayHours;
/// The [`BarGraph`] type holds configuration data needed to display an
/// hourly bar graph of the projects worked.
pub use histogram::BarGraph;
/// The [`Legend`] type formats the labels for a legend on the pie chart.
pub use legend::Legend;
/// The [`PieChart`] type holds the configuration data needed to display a
/// pie chart.
pub use pie::PieChart;
/// The [`PieData`] type holds the data for a pie chart.
pub use pie_data::PieData;
/// The [`Percent`] type represents a percentage greater than 0.0 up to 100.0
pub use tag_percent::Percent;
/// The [`TagPercent`] type holds a labeled percent value used in constructing
/// pie charts.
pub use tag_percent::TagPercent;
/// The [`Percentages`] type is a vector of [`TagPercent`]s.
pub type Percentages = Vec<TagPercent>;

pub(crate) mod utils {
    pub(crate) fn format_coord(coord: f32) -> String {
        let s = format!("{coord:.3}");
        s.trim_end_matches('0').trim_end_matches('.').to_string()
    }
}

#[cfg(test)]
mod tests {
    use spectral::prelude::*;

    use super::utils::*;

    #[test]
    fn test_format_coord() {
        assert_that!(format_coord(0.0).as_str()).is_equal_to("0");
        assert_that!(format_coord(1.2345678).as_str()).is_equal_to("1.235");
        assert_that!(format_coord(10.5).as_str()).is_equal_to("10.5");
        assert_that!(format_coord(10.25).as_str()).is_equal_to("10.25");
    }
}
