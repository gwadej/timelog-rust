//! Executable for logging time entries in a text-log-based format.
//!
//! ## Initializing `rtimelog`
//!
//! The `rtimelog` program uses a configuration file and a directory for runtime information.
//! You can initialize these with the `init` command.
//!
//! ```bash
//!   rtimelog init
//! ```
//!
//! ## Basic Usage
//!
//! The most basic usage of the tool involves two commands: `start` and `stop`
//!
//! ```bash
//!   rtimelog start +FizzBuzz @Code fix foobar bug
//! ```
//!
//! The above command stops timing on any current task and begins timing on a new one.
//! In this case, we are beginning work on fixing a bug. We've designated this as a
//! code task in the _FizzBuzz_ project.
//!
//! When we are finished working on a task, we can either start a new task or just stop
//! the present task.
//!
//! ```bash
//!   rtimelog stop
//! ```
//!
//! ## All Commands
//!
//! The application also supports the following commands:
//!
//! * aliases - List the aliases from the config file
//! * archive - Archive the first year from the timelog file, as long as it isn't the current year
//! * check - Check the logfile for problems
//! * comment - Add a comment line
//! * curr - Display the current task
//! * edit - Open the timelog file in the current editor
//! * event - Add a zero duration event
//! * help - Prints this message or the help of the given subcommand(s)
//! * init - Create the timelog directory and configuration
//! * ls - List entries for the specified day. Default to today
//! * lsproj - List known projects
//! * pause - Save the current task and stop timing
//! * push - Save the current task and start timing a new task
//! * report - Reporting commands
//!    * chart - Display a chart of the hours worked for each of the appropriate days and projects
//!    * detail - Display a report for the specified days and projects
//!    * events - Display the zero duration events for each of the appropriate days and projects
//!    * hours - Display the hours worked for each of the appropriate days and projects
//!    * intervals - Display the intervals between zero duration events for each of the appropriate
//!      days and projects
//!    * summary - Display a summary of the appropriate days' projects
//! * resume - Stop last task and restart top task on stack.
//! * stack - Stack specific commands
//!    * clear - Clear all of the items from the stack
//!    * drop - Drop items from stack
//!    * keep - Remove all except the top number of items from the stack
//!    * ls - Display items on the stack
//!    * top - Display just the top item on the stack
//! * start - Start timing a new task
//! * stop - Stop timing the current task
//! * swap - Pop the top task description on the stack and push the current task
//! * entry - Entry specific commands
//!    * discard - Discard the most recent entry
//!    * ignore - Return to the previous task, setting the current entry as ignored
//!    * now - Reset the time of the most recent entry to now
//!    * rewrite - Rewrite the most recent entry
//!    * was - Set the time on the most recent entry
//!    * rewind - Shift the time on the most recent entry back the specified number of minutes
//!
//! See the [`timelog`] library for all of the supporting code.

#![warn(clippy::cast_lossless)]
#![warn(clippy::return_self_not_must_use)]
#![warn(clippy::uninlined_format_args)]
#![warn(clippy::unwrap_used)]

extern crate clap;

use std::process;

use clap::Parser;

use timelog::Cli;

fn main() {
    let cli = Cli::parse();

    if let Err(e) = cli.run() {
        eprintln!("{e}");
        process::exit(1);
    }
}
