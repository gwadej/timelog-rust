//! Utilities for working with dates and times
//!
//! # Examples
//!
//! ```rust
//! use timelog::date::{Date, DateTime};
//! use timelog::Result;
//!
//! # fn main() -> Result<()> {
//! let mut day = Date::try_from("2021-07-02")?;
//! print!("Date: {}", day);
//!
//! let mut stamp = DateTime::try_from("2021-07-02 12:34:00")?;
//! print!("Stamp: {}", stamp);
//!
//! let mut today = Date::parse("today")?;
//! print!("Today: {}", today);
//! #   Ok(())
//! #  }
//! ```
//!
//! # Description
//!
//! The [`Date`] struct represents a date in the local time zone. The module also
//! provides tools for manipulating [`Date`]s.
//!
//! The [`DateRange`] struct represents a pair of dates in the local time zone.
//! The range represents a half-open range.
//!
//! The [`DateTime`] struct represents a date and time in the local time zone. The
//! module also provides tools for manipulating [`DateTime`]s.

use std::fmt::{self, Display};
use std::time::Duration;

use chrono::{Datelike, Local, NaiveDate, NaiveDateTime, NaiveTime, Timelike};

pub mod error;
pub mod parse;

/// Type shortcut for [`parse::DateParser`]
pub use parse::DateParser;
/// Type shortcut for [`parse::RangeParser`]
pub use parse::RangeParser;
/// Enum listing the days of the week. (From [`chrono::Weekday`])
pub type Weekday = chrono::Weekday;
/// Type for our internal time format.
pub type Time = chrono::NaiveTime;

/// Error type for errors in date functionality.
pub use error::DateError;
/// Result type for errors in date functionality.
pub type Result<T> = std::result::Result<T, DateError>;

/// Representation of a date in the local time zone.
#[derive(Debug, Clone, Copy, PartialOrd, Ord, PartialEq, Eq)]
pub struct Date(chrono::NaiveDate);

// Create Dates
impl Date {
    /// Create a [`Date`] out of a year, month, and day, returning a [`Result`].
    ///
    /// # Errors
    ///
    /// If one of the parameters is outside the legal range for a date, returns an
    /// [`DateError::InvalidDate`].
    pub fn new(year: i32, month: u32, day: u32) -> Result<Self> {
        Ok(Self(
            NaiveDate::from_ymd_opt(year, month, day).ok_or(DateError::InvalidDate)?
        ))
    }

    /// Create a [`Date`] for today.
    #[must_use]
    pub fn today() -> Self { Self(Local::now().date_naive()) }

    /// Create a [`Date`] object from the supplied date specification.
    ///
    /// Legal specifications include "today" and "yesterday", the days of the week "sunday"
    /// through "saturday", and a date in the form "YYYY-MM-DD".
    /// The days of the week result in the date representing the previous instance of that day
    /// (last Monday for "monday", etc.).
    ///
    /// # Errors
    ///
    /// Return [`DateError::InvalidDaySpec`] if the supplied spec is not valid.
    pub fn parse(datespec: &str) -> Result<Self> { DateParser::default().parse(datespec) }
}

// Accessors
impl Date {
    /// Return the year portion of the [`Date`]
    pub fn year(&self) -> i32 { self.0.year() }

    /// Return the month portion of the [`Date`]
    pub fn month(&self) -> u32 { self.0.month() }

    /// Return the day portion of the [`Date`]
    pub fn day(&self) -> u32 { self.0.day() }

    /// Return the day of the week enum.
    pub fn weekday(&self) -> Weekday { self.0.weekday() }

    /// Return the day of the week as a string.
    pub fn weekday_str(&self) -> &'static str {
        match self.0.weekday() {
            Weekday::Sun => "Sunday",
            Weekday::Mon => "Monday",
            Weekday::Tue => "Tuesday",
            Weekday::Wed => "Wednesday",
            Weekday::Thu => "Thursday",
            Weekday::Fri => "Friday",
            Weekday::Sat => "Saturday"
        }
    }
}

// Relative date methods
impl Date {
    /// Create a [`DateTime`] object for the first second of the current [`Date`]
    #[must_use]
    pub fn day_start(&self) -> DateTime {
        DateTime(self.0.and_hms_opt(0, 0, 0).expect("Midnight exists"))
    }

    /// Create a [`DateTime`] object for the last second of the current [`Date`]
    #[must_use]
    pub fn day_end(&self) -> DateTime {
        DateTime(self.0.and_hms_opt(23, 59, 59).expect("Midnight exists"))
    }

    // Find the last date before this one where the day of the week was
    // weekday.
    #[must_use]
    fn find_previous(&self, weekday: Weekday) -> Self {
        let mut day = self.0.pred_opt().expect("Not at beginning of time");
        while day.weekday() != weekday {
            day = day.pred_opt().expect("Not at beginning of time");
        }
        Self(day)
    }

    // Find the next date after this one where the day of the week was
    // weekday.
    #[must_use]
    fn find_next(&self, weekday: Weekday) -> Self {
        let mut day = self.0.succ_opt().expect("Not at end of time");
        while day.weekday() != weekday {
            day = day.succ_opt().expect("Not at end of time");
        }
        Self(day)
    }

    /// Create a [`Date`] object for the last day of the month.
    #[must_use]
    pub fn month_start(&self) -> Date { Date(self.0.with_day(1).expect("Reasonable date range")) }

    // Return true if the supplied year is a leap year
    fn is_leap_year(year: i32) -> bool {
        (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))
    }

    /// Create a [`Date`] object for the last day of the month.
    #[must_use]
    #[rustfmt::skip]
    pub fn month_end(&self) -> Date {
        let last_day = match self.0.month() {
            1 | 3 | 5 | 7 | 8 | 10 | 12 => 31,
            4 | 6 | 9 | 11 => 30,
            2 => if Self::is_leap_year(self.0.year()) { 29 } else { 28 },
            _ => unreachable!()
        };
        Date(self.0.with_day(last_day).expect("End of month should work"))
    }

    /// Create a [`Date`] object for the first day of the week.
    #[must_use]
    pub fn week_start(&self) -> Date {
        match self.0.weekday() {
            Weekday::Sun => *self,
            _ => self.find_previous(Weekday::Sun)
        }
    }

    /// Create a [`Date`] object for the last day of the week.
    #[must_use]
    pub fn week_end(&self) -> Date {
        match self.0.weekday() {
            Weekday::Sat => *self,
            _ => self.find_next(Weekday::Sat)
        }
    }

    /// Create a [`Date`] object for the beginning of the year containing the date.
    #[must_use]
    pub fn year_start(&self) -> Date {
        Self(self.0
            .with_month(1).expect("Within reasonable dates")
            .with_day(1).expect("Within reasonable dates"))
    }

    /// Create a [`Date`] object for the end of the year containing the date.
    #[must_use]
    pub fn year_end(&self) -> Date {
        Self(self.0
            .with_month(12).expect("Within reasonable dates")
            .with_day(31).expect("Within reasonable dates"))
    }

    /// Create a [`Date`] for the day after the current date.
    #[must_use]
    pub fn succ(&self) -> Date { Self(self.0.succ_opt().expect("Not at end of time")) }

    /// Create a [`Date`] for the day before the current date.
    #[must_use]
    pub fn pred(&self) -> Date { Self(self.0.pred_opt().expect("Not at beginnning of time")) }
}

impl Default for Date {
    /// The default [`Date`] is the current day.
    fn default() -> Date { Self::today() }
}

impl std::convert::TryFrom<&str> for Date {
    type Error = DateError;

    /// Create a [`Date`] out of a string, returning a [`Result`]
    ///
    /// # Errors
    ///
    /// If the date is not formatted as 'YYYY-MM-DD', returns an [`DateError::InvalidDate`].
    /// Also if the date string cannot be converted into a [`Date`], returns an
    /// [`DateError::InvalidDate`].
    #[rustfmt::skip]
    fn try_from(date: &str) -> Result<Self> {
        let Ok(parsed) = NaiveDate::parse_from_str(date, "%Y-%m-%d") else {
            return Err(DateError::InvalidDate);
        };
        Ok(Self(parsed))
    }
}

impl Display for Date {
    /// Format a [`Date`] object in "YYYY-MM-DD" format.
    #[rustfmt::skip]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}-{:02}-{:02}", self.0.year(), self.0.month(), self.0.day())
    }
}

impl From<Date> for String {
    /// Convert a [`Date`] into a [`String`]
    fn from(date: Date) -> Self { date.to_string() }
}

/// Representation of a half-open range of dates in the local time zone.
#[derive(Debug, PartialEq, Eq)]
pub struct DateRange {
    start: Date,
    end:   Date
}

impl DateRange {
    /// Create [`DateRange`] with the supplied start and end dates. If
    /// The end is <= the start, create an empty [`DateRange`]
    pub fn new(start: Date, end: Date) -> Self {
        Self::new_opt(start, end).unwrap_or(Self { start, end: start })
    }

    /// Create [`DateRange`] if the start date is less than the end date.
    pub fn new_opt(start: Date, end: Date) -> Option<Self> {
        (start < end).then_some(Self { start, end })
    }

    /// Create [`DateRange`] from an iterator returning parts of a date range descriptor.
    ///
    /// # Errors
    ///
    /// - Return [`DateError::InvalidDate`] if the specification for a single day is not valid.
    /// - Return [`DateError::InvalidDaySpec`] if overall range specification is not valid.
    pub fn parse<'a, I>(datespec: &mut I) -> Result<Self>
    where
        I: Iterator<Item = &'a str>
    {
        RangeParser::default().parse(datespec).map(|(dr, _)| dr)
    }
}

impl DateRange {
    /// Return a copy of the start date for the range.
    pub fn start(&self) -> Date { self.start }

    /// Return a copy of the end date for the range.
    pub fn end(&self) -> Date { self.end }

    /// Return true if the range is empty.
    pub fn is_empty(&self) -> bool { self.start >= self.end }
}

impl From<Date> for DateRange {
    /// Create [`DateRange`] covering the supplied date.
    fn from(date: Date) -> DateRange { Self { start: date, end: date.succ() } }
}

impl Default for DateRange {
    /// The default [`DateRange`] covers just today.
    fn default() -> Self {
        let today = Date::today();
        Self { start: today, end: today.succ() }
    }
}

/// Representation of a date and time in the local time zone.
#[derive(Debug, Clone, Copy, PartialOrd, Ord, PartialEq, Eq)]
pub struct DateTime(chrono::NaiveDateTime);

impl DateTime {
    /// Create a [`DateTime`] from two tuples, one representing the date (year, month, day) and
    /// the second representing the time (hour, minute, second).
    ///
    /// # Errors
    ///
    /// Return an [`DateError::InvalidDate`] if the values in the tuples are not appropriate for the
    /// data types.
    pub fn new(date: (i32, u32, u32), time: (u32, u32, u32)) -> Result<Self> {
        let Some(d) = NaiveDate::from_ymd_opt(date.0, date.1, date.2) else {
            return Err(DateError::InvalidDate);
        };
        let Some(t) = NaiveTime::from_hms_opt(time.0, time.1, time.2) else {
            return Err(DateError::InvalidDate);
        };
        Ok(Self(NaiveDateTime::new(d, t)))
    }

    /// Create a [`Date`] for right now.
    pub fn now() -> Self { Self(Local::now().naive_local()) }

    // Create a new [`DateTime`] from a [`Date`] and a [`NaiveTime`]
    pub(crate) fn new_from_date_time(date: Date, time: NaiveTime) -> Self {
        Self(NaiveDateTime::new(date.0, time))
    }
}

impl DateTime {
    /// Return the epoch time representing the current value of the [`DateTime`] object.
    pub fn timestamp(&self) -> i64 { self.0.timestamp() }

    /// Return a copy of just the [`Date`] portion of the [`DateTime`] object.
    pub fn date(&self) -> Date { Date(self.0.date()) }

    /// Return the hour of the day.
    pub fn hour(&self) -> u32 { self.0.hour() }

    /// Return the minute of the hour.
    pub fn minute(&self) -> u32 { self.0.minute() }

    /// Return the number of seconds after the hour.
    pub fn second_offset(&self) -> u32 { self.0.minute() * 60 + self.0.second() }

    /// Return the formatted time as a [`String`]
    pub fn hhmm(&self) -> String { format!("{:02}:{:02}", self.0.hour(), self.0.minute()) }
}

impl DateTime {
    /// Return a [`Duration`] lasting the supplied number of minutes.
    pub fn seconds(seconds: u64) -> Duration { Duration::from_secs(seconds) }

    /// Return a [`Duration`] lasting the supplied number of minutes.
    pub fn minutes(minutes: u64) -> Duration { Duration::from_secs(minutes * 60) }

    /// Return a [`Duration`] lasting the supplied number of hours.
    pub fn hours(hours: u64) -> Duration { Duration::from_secs(hours * 3600) }

    /// Return a [`Duration`] lasting the supplied number of days.
    pub fn days(days: u64) -> Duration { Duration::from_secs(days * 86400) }

    /// Return a [`Duration`] lasting the supplied number of weeks.
    pub fn weeks(weeks: u64) -> Duration { Self::days(weeks * 7) }
}

impl std::ops::Add<Duration> for DateTime {
    type Output = Result<DateTime>;

    /// Return a [`DateTime`] as a [`Result`] resulting from adding the `rhs` [`Duration`] to the
    /// object.
    ///
    /// # Errors
    ///
    /// Return an [`DateError::InvalidDate`] if adding the duration results in a bad date.
    fn add(self, rhs: Duration) -> Result<Self> {
        Ok(Self(
            self.0 + chrono::Duration::from_std(rhs).map_err(|_| DateError::InvalidDate)?
        ))
    }
}

impl std::ops::Sub<Self> for DateTime {
    type Output = Result<Duration>;

    /// Return the [`Duration`] as a [`Result`] resulting from subtracting the `rhs` from the
    /// object.
    ///
    /// # Errors
    ///
    /// Return an [`DateError::EntryOrder`] if the `rhs` is larger than the [`DateTime`].
    fn sub(self, rhs: Self) -> Result<Duration> {
        (self.0 - rhs.0).to_std().map_err(|_| DateError::EntryOrder)
    }
}

impl std::ops::Sub<Duration> for DateTime {
    type Output = Result<DateTime>;

    /// Return a [`DateTime`] as a [`Result`] resulting from subtracting the `rhs` [`Duration`]
    /// from the object.
    ///
    /// # Errors
    ///
    /// Return an [`DateError::InvalidDate`] if adding the duration results in a bad date.
    fn sub(self, rhs: Duration) -> Result<Self> {
        Ok(Self(
            self.0 - chrono::Duration::from_std(rhs).map_err(|_| DateError::InvalidDate)?
        ))
    }
}

impl std::convert::TryFrom<&str> for DateTime {
    type Error = DateError;

    /// Parse the [`DateTime`] out of a string, returning a [`Result`]
    ///
    /// # Errors
    ///
    /// If the datetime is not formatted as 'YYYY-MM-DD HH:MM:SS', returns an
    /// [`DateError::InvalidDate`]. Also if the datetime cannot be converted into a [`DateTime`],
    /// returns an [`DateError::InvalidDate`].
    #[rustfmt::skip]
    fn try_from(datetime: &str) -> Result<Self> {
        let Ok(parsed) = NaiveDateTime::parse_from_str(datetime, "%Y-%m-%d %H:%M:%S") else {
            return Err(DateError::InvalidDate);
        };
        Ok(Self(parsed))
    }
}

impl Display for DateTime {
    /// Format a [`DateTime`] object in "YYYY-MM-DD HH:MM:DD" format.
    #[rustfmt::skip]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}-{:02}-{:02} {:02}:{:02}:{:02}",
            self.0.year(), self.0.month(), self.0.day(),
            self.0.hour(), self.0.minute(), self.0.second())
    }
}

impl From<DateTime> for String {
    /// Convert a [`Date`] into a [`String`]
    fn from(datetime: DateTime) -> Self { datetime.to_string() }
}

#[cfg(test)]
mod tests {
    use spectral::prelude::*;

    use super::*;

    #[test]
    fn test_date_new() {
        let date = Date::new(2021, 11, 20);
        assert_that!(&date).is_ok();
        assert_that!(date.unwrap().to_string()).is_equal_to(&String::from("2021-11-20"));
    }

    #[test]
    fn test_date_new_bad_month() {
        assert_that!(Date::new(2021, 0, 20)).is_err_containing(&DateError::InvalidDate);
        assert_that!(Date::new(2021, 13, 20)).is_err_containing(&DateError::InvalidDate);
    }

    #[test]
    fn test_date_new_bad_day() {
        assert_that!(Date::new(2021, 11, 0)).is_err_containing(&DateError::InvalidDate);
        assert_that!(Date::new(2021, 11, 32)).is_err_containing(&DateError::InvalidDate);
    }

    #[test]
    fn test_date_day_end() {
        let date = Date::new(2021, 11, 20).unwrap();
        assert_that!(date.day_end())
            .is_equal_to(&DateTime::new((2021, 11, 20), (23, 59, 59)).unwrap());
    }

    #[test]
    fn test_date_day_start() {
        let date = Date::new(2021, 11, 20).unwrap();
        assert_that!(date.day_start())
            .is_equal_to(&DateTime::new((2021, 11, 20), (0, 0, 0)).unwrap());
    }

    #[test]
    fn test_month_start() {
        let date = Date::new(2022, 11, 20).unwrap();
        assert_that!(date.month_start()).is_equal_to(&Date::new(2022, 11, 1).unwrap());
    }

    #[test]
    fn test_month_end() {
        let tests = [
            ("jan", 1, 31),
            ("feb", 2, 28),
            ("mar", 3, 31),
            ("apr", 4, 30),
            ("may", 5, 31),
            ("jun", 6, 30),
            ("jul", 7, 31),
            ("aug", 8, 31),
            ("sep", 9, 30),
            ("oct", 10, 31),
            ("nov", 11, 30),
            ("dec", 12, 31)
        ];
        for (name, mon, day) in tests.iter() {
            let date = Date::new(2022, *mon, 20).unwrap();
            assert_that!(date.month_end())
                .named(name)
                .is_equal_to(&Date::new(2022, *mon, *day).unwrap());
        }
    }

    #[test]
    fn test_month_end_leap_year() {
        let date = Date::new(2020, 2, 20).unwrap();
        assert_that!(date.month_end()).is_equal_to(&Date::new(2020, 2, 29).unwrap());
    }

    #[test]
    fn test_date_week_start() {
        let date = Date::new(2022, 12, 20).unwrap();
        assert_that!(date.week_start()).is_equal_to(&Date::new(2022, 12, 18).unwrap());
    }

    #[test]
    fn test_date_week_start_no_change() {
        let date = Date::new(2022, 12, 18).unwrap();
        assert_that!(date.week_start()).is_equal_to(&Date::new(2022, 12, 18).unwrap());
    }

    #[test]
    fn test_date_week_end() {
        let date = Date::new(2022, 12, 15).unwrap();
        assert_that!(date.week_end()).is_equal_to(&Date::new(2022, 12, 17).unwrap());
    }

    #[test]
    fn test_date_week_end_no_change() {
        let date = Date::new(2022, 12, 17).unwrap();
        assert_that!(date.week_end()).is_equal_to(&Date::new(2022, 12, 17).unwrap());
    }

    #[test]
    fn test_date_year_start() {
        let date = Date::new(2022, 12, 20).unwrap();
        assert_that!(date.year_start()).is_equal_to(&Date::new(2022, 1, 1).unwrap());
    }

    #[test]
    fn test_date_year_end() {
        let date = Date::new(2022, 12, 20).unwrap();
        assert_that!(date.year_end()).is_equal_to(&Date::new(2022, 12, 31).unwrap());
    }

    #[test]
    fn test_date_succ() {
        let date = Date::new(2021, 11, 20).unwrap();
        assert_that!(date.succ()).is_equal_to(&Date::new(2021, 11, 21).unwrap());
    }

    #[test]
    fn test_date_pred() {
        let date = Date::new(2021, 11, 20).unwrap();
        assert_that!(date.pred()).is_equal_to(&Date::new(2021, 11, 19).unwrap());
    }

    #[test]
    fn test_date_try_from() {
        let date = Date::try_from("2021-11-20");
        assert_that!(&date).is_ok();
        assert_that!(date.unwrap()).is_equal_to(&Date::new(2021, 11, 20).unwrap());
    }

    #[test]
    fn test_date_try_from_bad() {
        let date = Date::try_from("fred");
        assert_that!(&date).is_err();
    }

    // DateRange

    #[test]
    fn test_date_range_default() {
        let range = DateRange::default();
        assert_that!(range.start()).is_equal_to(&Date::today());
        assert_that!(range.end()).is_equal_to(&Date::today().succ());
    }

    #[test]
    fn test_date_range_new_opt() {
        let range = DateRange::new_opt(Date::today(), Date::today().succ());
        assert_that!(range).is_some();
        let range = range.unwrap();
        assert_that!(range.start()).is_equal_to(&Date::today());
        assert_that!(range.end()).is_equal_to(&Date::today().succ());
    }

    #[test]
    fn test_date_range_new_opt_backwards() {
        let range = DateRange::new_opt(Date::today(), Date::today().pred());
        assert_that!(range).is_none();
    }

    #[test]
    fn test_date_range_new_opt_empty() {
        let range = DateRange::new_opt(Date::today(), Date::today());
        assert_that!(range).is_none();
    }

    #[test]
    fn test_date_range_new() {
        let range = DateRange::new(Date::today(), Date::today().succ());
        assert_that!(range.start()).is_equal_to(&Date::today());
        assert_that!(range.end()).is_equal_to(&Date::today().succ());
        assert_that!(range.is_empty()).is_equal_to(&false);
    }

    #[test]
    fn test_date_range_new_backwards() {
        let range = DateRange::new(Date::today(), Date::today().pred());
        assert_that!(range.start()).is_equal_to(&Date::today());
        assert_that!(range.end()).is_equal_to(&Date::today());
        assert_that!(range.is_empty()).is_equal_to(&true);
    }

    #[test]
    fn test_date_range_new_empty() {
        let range = DateRange::new(Date::today(), Date::today());
        assert_that!(range.start()).is_equal_to(&Date::today());
        assert_that!(range.end()).is_equal_to(&Date::today());
        assert_that!(range.is_empty()).is_equal_to(&true);
    }

    #[test]
    fn test_date_range_from_date() {
        let date = Date::new(2022, 12, 1).expect("Hard coded date won't fail.");
        let range: DateRange = date.into();
        let expect = DateRange::new(date, date.succ());

        assert_that!(range).is_equal_to(&expect);
    }

    // DateTime

    #[test]
    fn test_datetime_new() {
        let date = DateTime::new((2021, 11, 20), (11, 32, 18));
        assert_that!(date).is_ok();
        assert_that!(date.unwrap().to_string()).is_equal_to(&String::from("2021-11-20 11:32:18"));
    }

    #[test]
    fn test_datetime_new_bad_date() {
        let date = DateTime::new((2021, 13, 20), (11, 32, 18));
        assert_that!(date).is_err();
    }

    #[test]
    fn test_datetime_new_bad_time() {
        let date = DateTime::new((2021, 11, 20), (11, 82, 18));
        assert_that!(date).is_err();
    }

    #[test]
    fn test_datetime_try_from() {
        let date = DateTime::try_from("2021-11-20 11:32:18");
        assert_that!(&date).is_ok();
        assert_that!(date)
            .is_ok()
            .is_equal_to(&DateTime::new((2021, 11, 20), (11, 32, 18)).unwrap());
    }

    #[test]
    fn test_datetime_diff() {
        let date = DateTime::new((2021, 11, 20), (11, 32, 18)).unwrap();
        let old = DateTime::new((2021, 11, 18), (12, 00, 00)).unwrap();
        assert_that!(date - old)
            .is_ok()
            .is_equal_to(&Duration::from_secs(2 * 86400 - 28 * 60 + 18));
    }

    #[test]
    fn test_datetime_diff_bad() {
        let date = DateTime::new((2021, 11, 18), (12, 00, 00)).unwrap();
        let old = DateTime::new((2021, 11, 20), (11, 32, 18)).unwrap();
        assert_that!(date - old).is_err();
    }

    #[test]
    fn test_datetime_add_time() {
        let date = DateTime::new((2021, 11, 18), (12, 00, 00)).unwrap();
        let new = date + DateTime::minutes(10);
        assert_that!(new)
            .is_ok()
            .is_equal_to(&DateTime::new((2021, 11, 18), (12, 10, 00)).unwrap());
    }

    #[test]
    fn test_datetime_add_days() {
        let date = DateTime::new((2021, 11, 18), (12, 00, 00)).unwrap();
        let new = date + DateTime::days(3);
        assert_that!(new)
            .is_ok()
            .is_equal_to(&DateTime::new((2021, 11, 21), (12, 00, 00)).unwrap());
    }

    #[test]
    fn test_datetime_hhmm() {
        let date = DateTime::new((2021, 11, 18), (8, 5, 13)).unwrap();
        assert_that!(date.hhmm()).is_equal_to(&String::from("08:05"));
    }

    #[test]
    fn test_datetime_try_from_bad() {
        let date = DateTime::try_from("fred");
        assert_that!(&date).is_err();
    }
}
