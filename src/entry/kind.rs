//! Module representing the kind of an entry.
//!
//! # Examples
//!
//! ```rust
//! use timelog::entry::EntryKind;
//!
//! # fn main() {
//!     let ek = EntryKind::new(Some('!'));
//!     println!("Is ignored: {}", ek.is_ignored());
//!
//!     let ek = EntryKind::from_entry_line("2022-06-20 21:15:34^+happening Something");
//!     println!("Is event: {}", ek.is_event());
//! # }
//! ```
//!
//! # Description
//!
//! Objects of this type represent the kind of an [`Entry`].

#[cfg(doc)]
use crate::entry::Entry;
use crate::entry::{EntryError, MARKER_RE};

/// Enumeration of different kinds of task entries
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum EntryKind {
    /// An entry that starts a task.
    Start,
    /// Stop the most current task.
    Stop,
    /// An entry that is ignored.
    Ignored,
    /// An entry that marks a zero duration event.
    Event
}

impl From<EntryKind> for char {
    /// Return the matching mark character for the supplied kind.
    fn from(k: EntryKind) -> char {
        match k {
            EntryKind::Start | EntryKind::Stop => ' ',
            EntryKind::Ignored => '!',
            EntryKind::Event => '^'
        }
    }
}

impl EntryKind {
    /// Create a new [`EntryKind`] from an optional character, defaulting to [`EntryKind::Start`]
    /// if the character is not recognized.
    pub fn new<KC>(mark: KC) -> Self
    where
        KC: Into<Option<char>>
    {
        Self::try_new(mark).unwrap_or(Self::Start)
    }

    /// Create a new [`EntryKind`] from an optional character.
    ///
    /// # Errors
    ///
    /// Return [`EntryError::InvalidMarker`] if character not recognized.
    pub fn try_new<KC>(mark: KC) -> Result<Self, EntryError>
    where
        KC: Into<Option<char>>
    {
        match mark.into() {
            Some(' ') | None => Ok(Self::Start),
            Some('!') => Ok(Self::Ignored),
            Some('^') => Ok(Self::Event),
            Some(_) => Err(EntryError::InvalidMarker)
        }
    }

    /// Extract the appropriate [`EntryKind`] from supplied entry line.
    pub fn from_entry_line(line: &str) -> Self {
        let marker = MARKER_RE.captures(line)
            .map(|cap| cap.get(1).and_then(|m| m.as_str().chars().next()));
        Self::new(marker.flatten())
    }

    /// Is a start marker
    pub fn is_start(&self) -> bool { *self == Self::Start }

    /// Is an ignored entry marker
    pub fn is_ignored(&self) -> bool { *self == Self::Ignored }

    /// Is an event marker
    pub fn is_event(&self) -> bool { *self == Self::Event }

    /// Is a stop marker
    pub fn is_stop(&self) -> bool { *self == Self::Stop }
}

#[cfg(test)]
mod tests {
    use spectral::prelude::*;

    use super::*;

    #[test]
    #[rustfmt::skip]
    fn try_new_successes() {
        assert_that!(EntryKind::try_new(None)).is_ok().is_equal_to(EntryKind::Start);
        assert_that!(EntryKind::try_new(' ')).is_ok().is_equal_to(EntryKind::Start);
        assert_that!(EntryKind::try_new('!')).is_ok().is_equal_to(EntryKind::Ignored);
        assert_that!(EntryKind::try_new('^')).is_ok().is_equal_to(EntryKind::Event);
    }

    #[test]
    fn try_new_fails() {
        assert_that!(EntryKind::try_new('a')).is_err();
        assert_that!(EntryKind::try_new('8')).is_err();
        assert_that!(EntryKind::try_new('*')).is_err();
    }

    #[test]
    fn new_never_fails() {
        assert_that!(EntryKind::new(None)).is_equal_to(EntryKind::Start);
        assert_that!(EntryKind::new(' ')).is_equal_to(EntryKind::Start);
        assert_that!(EntryKind::new('!')).is_equal_to(EntryKind::Ignored);
        assert_that!(EntryKind::new('^')).is_equal_to(EntryKind::Event);
        assert_that!(EntryKind::new('a')).is_equal_to(EntryKind::Start);
        assert_that!(EntryKind::new('8')).is_equal_to(EntryKind::Start);
        assert_that!(EntryKind::new('*')).is_equal_to(EntryKind::Start);
    }

    #[test]
    fn char_from_kind() {
        assert_that!(char::from(EntryKind::Start)).is_equal_to(' ');
        assert_that!(char::from(EntryKind::Stop)).is_equal_to(' ');
        assert_that!(char::from(EntryKind::Ignored)).is_equal_to('!');
        assert_that!(char::from(EntryKind::Event)).is_equal_to('^');
    }

    #[test]
    fn from_entry_line_on_start() {
        #[rustfmt::skip]
        assert_that!(EntryKind::from_entry_line("2013-06-05 10:00:02 +test @Commented"))
            .is_equal_to(EntryKind::Start);
    }

    #[test]
    fn from_entry_line_on_ignored() {
        #[rustfmt::skip]
        assert_that!(EntryKind::from_entry_line("2013-06-05 10:00:02!+test @Ignored"))
            .is_equal_to(EntryKind::Ignored);
    }

    #[test]
    fn from_entry_line_on_pin() {
        #[rustfmt::skip]
        assert_that!(EntryKind::from_entry_line("2013-06-05 10:00:02^+test @Event"))
            .is_equal_to(EntryKind::Event);
    }

    #[test]
    fn get_line_entry_kind_on_comment() {
        #[rustfmt::skip]
        assert_that!(EntryKind::from_entry_line("#2013-06-05 10:00:02 +test @Commented"))
            .is_equal_to(EntryKind::Start);
    }
}
