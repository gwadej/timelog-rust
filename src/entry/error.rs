//! Module representing errors encountered parsing entry lines.

use thiserror;

/// Errors reported attempting to parse an entry line.
#[derive(Clone, Copy, Eq, thiserror::Error, Debug, PartialEq)]
pub enum EntryError {
    /// Entry line contains no text
    #[error("Line is empty")]
    BlankLine,

    /// Timestamp on entry line is unparseable
    #[error("Not a valid timestamp")]
    InvalidTimeStamp,

    /// Entry line is missing a task description
    #[error("No task description")]
    MissingTask,

    /// Entry line has unrecognized marker
    #[error("Unrecognized entry marker")]
    InvalidMarker
}
