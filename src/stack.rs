//! Interface to the stack file for the timelog application.
//!
//! # Examples
//!
//! ```rust
//! use timelog::stack::Stack;
//! # fn main() -> Result<(), timelog::Error> {
//! let stack = Stack::new("./stack.txt" )?;
//!
//! stack.push("+Project @Task More detail");
//! let task = stack.pop().expect("Can't pop task");
//! println!("{:?}", task);
//! stack.clear();
//! #   Ok(())
//! # }
//! ```

use std::fs::{self, File};
use std::io;
use std::io::prelude::*;
use std::path::Path;
use std::result;

#[doc(inline)]
use crate::error::{Error, PathError};
#[doc(inline)]
use crate::file;

/// Represent the stack file on disk.
#[derive(Debug)]
pub struct Stack(String);

impl Stack {
    /// Creates a [`Stack`] object wrapping the supplied file.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
    /// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
    /// - Return [`PathError::InvalidStackPath`] if stack path is invalid.
    pub fn new(file: &str) -> result::Result<Self, PathError> {
        file::canonical_filename(file, file::FileKind::StackFile).map(Self)
    }

    /// Open the stack file for reading, return a [`File`].
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if unable to open the file.
    pub fn open(&self) -> result::Result<File, PathError> {
        File::open(&self.0).map_err(|e| PathError::FileAccess(self.clone_file(), e.to_string()))
    }

    // Clone the filename
    fn clone_file(&self) -> String { self.0.clone() }

    /// Return `true` if the timelog file exists
    pub fn exists(&self) -> bool { Path::new(&self.0).exists() }

    /// Truncates the stack file, removing all items from the stack.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the stack file is not accessible.
    pub fn clear(&self) -> result::Result<(), PathError> {
        fs::remove_file(&self.0).map_err(|e| PathError::FileAccess(self.clone_file(), e.to_string()))
    }

    /// Adds a new event to the stack file.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the stack file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the stack file cannot be written.
    pub fn push(&self, task: &str) -> result::Result<(), PathError> {
        let file = file::append_open(&self.0)?;
        let mut stream = io::BufWriter::new(file);
        writeln!(&mut stream, "{task}")
            .map_err(|e| PathError::FileWrite(self.clone_file(), e.to_string()))?;
        stream
            .flush()
            .map_err(|e| PathError::FileWrite(self.clone_file(), e.to_string()))?;
        Ok(())
    }

    /// Remove the most recent task from the stack file and return the task string.
    pub fn pop(&self) -> Option<String> {
        let mut file = file::rw_open(&self.0).ok()?;
        file::pop_last_line(&mut file)
    }

    /// Remove one or more tasks from the stack file.
    ///
    /// If `arg` is 0, remove one item.
    /// If `arg` is a positive number, remove that many items from the stack.
    ///
    /// # Errors
    ///
    /// - Return [`Error::StackPop`] if attempts to pop more items than exist in the stack file.
    pub fn drop(&self, arg: u32) -> crate::Result<()> {
        (0..std::cmp::max(1, arg))
            .try_for_each(|_| self.pop().map(|_| ()))
            .ok_or(Error::StackPop)
    }

    /// Remove everything except the top `num` tasks from the stack.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the stack file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the stack file cannot be written.
    /// - Return [`PathError::RenameFailure`] if the stack file cannot be renamed.
    pub fn keep(&self, num: u32) -> crate::Result<()> {
        let file = self.open()?;
        let unum = num as usize;
        let len = io::BufReader::new(file).lines().count();

        if len > unum {
            let backfile = format!("{}-bak", self.0);
            let outfile = file::append_open(&backfile)?;
            let mut stream = io::BufWriter::new(outfile);
            let reader = io::BufReader::new(self.open()?);
            for line in reader.lines().skip(len - unum).flatten() {
                writeln!(&mut stream, "{line}")
                    .map_err(|e| PathError::FileWrite(self.clone_file(), e.to_string()))?;
            }
            stream
                .flush()
                .map_err(|e| PathError::FileWrite(self.clone_file(), e.to_string()))?;

            fs::rename(&backfile, self.clone_file())
                .map_err(|e| PathError::RenameFailure(self.clone_file(), e.to_string()))?;
        }
        Ok(())
    }

    /// Process the stack top-down, passing the index and lines to the supplied function.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the stack file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the stack file cannot be written.
    /// - Return [`PathError::RenameFailure`] if the stack file cannot be renamed.
    pub fn process_down_stack<F>(&self, mut func: F) -> crate::Result<()>
    where
        F: FnMut(usize, &str)
    {
        let file = self.open()?;
        let lines: Vec<String> = io::BufReader::new(file).lines().map_while(Result::ok).collect();
        for (i, ln) in lines.iter().rev().enumerate() {
            func(i, ln);
        }
        Ok(())
    }

    /// Format the stack as a [`String`].
    ///
    /// The stack will be formatted such that the most recent item is listed
    /// first.
    pub fn list(&self) -> String {
        let mut output = String::new();
        let Ok(_) = self.process_down_stack(|_, l| {
            output.push_str(l);
            output.push('\n');
        })
        else {
            return String::new();
        };

        output
    }

    /// Return the top of the stack as a [`String`].
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the stack file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the stack file cannot be written.
    /// - Return [`PathError::RenameFailure`] if the stack file cannot be renamed.
    pub fn top(&self) -> crate::Result<String> {
        let file = self.open()?;
        let reader = io::BufReader::new(file).lines().map_while(Result::ok);
        Ok(reader.last().unwrap_or_default())
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use spectral::prelude::*;
    use tempfile::TempDir;

    use super::*;

    #[test]
    fn test_new_missing_file() {
        assert_that!(Stack::new("")).is_err_containing(PathError::FilenameMissing);
    }

    #[test]
    fn test_new_bad_path() {
        let mut stackdir = TempDir::new()
            .expect("Cannot make tempdir")
            .path()
            .to_path_buf();
        stackdir.push("foo");
        stackdir.push("stack.txt");

        let file = stackdir.as_path().to_str().unwrap();
        assert_that!(Stack::new(file)).is_err_containing(PathError::InvalidPath(
            file.to_string(),
            "No such file or directory (os error 2)".to_string()
        ));
    }

    #[test]
    fn test_new() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        assert_that!(Stack::new(filename)).is_ok();
    }

    #[test]
    fn test_push() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(Path::new(filename).exists()).is_false();

        let task = "+house @todo change filters";
        assert_that!(stack.push(task)).is_ok();
        let path = Path::new(filename);
        assert_that!(path.is_file()).is_true();

        // test file length and handle newline lengths
        let filelen = path.metadata().expect("metadata fail").len() as usize;
        assert_that!(filelen).is_equal_to(task.len() + 1);

        assert_that!(stack.push(task)).is_ok();
        let filelen = path.metadata().expect("metadata fail").len() as usize;
        assert_that!(filelen).is_equal_to(2 * task.len() + 2);
    }

    #[test]
    fn test_pop() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.pop()).contains(String::from("+home @todo second"));
        assert_that!(stack.pop()).contains(String::from("+home @todo first"));

        let path = Path::new(filename);
        assert_that!(path.is_file()).is_true();
        // test file length and handle newline lengths
        let filelen = path.metadata().expect("metadata fail").len() as usize;
        assert_that!(filelen).is_equal_to(0);
    }

    #[test]
    fn test_clear() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.clear()).is_ok();
        assert_that!(Path::new(filename).exists()).is_false();
    }

    #[test]
    fn test_drop_0() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.drop(0)).is_ok();
        assert_that!(stack.pop()).contains(String::from("+home @todo first"));
    }

    #[test]
    fn test_drop_1() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.drop(1)).is_ok();
        assert_that!(stack.pop()).contains(String::from("+home @todo first"));
    }

    #[test]
    fn test_drop_2() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n+home @todo third\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.drop(2)).is_ok();
        assert_that!(stack.pop()).contains(String::from("+home @todo first"));
    }

    #[test]
    fn test_list() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n+home @todo third\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.list()).is_equal_to(String::from(
            "+home @todo third\n+home @todo second\n+home @todo first\n"
        ));
    }

    #[test]
    fn test_top() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        let mut file = File::create(filename).expect("Cannot create file");
        file.write_all(b"+home @todo first\n+home @todo second\n+home @todo third\n")
            .expect("Cannot fill file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.top()).contains(String::from("+home @todo third"));
    }

    #[test]
    fn test_top_empty() {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("stack.txt");

        let filename = path.to_str().unwrap();
        File::create(filename).expect("Cannot create file");

        let stack = Stack::new(filename).expect("Cannot create stack");
        assert_that!(stack.top()).contains(String::new());
    }
}
