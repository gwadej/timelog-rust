//! Interface to the timelog file for the timelog application.
//!
//! # Examples
//!
//! ```rust
//! use timelog::logfile::Logfile;
//! # fn main() -> Result<(), timelog::Error> {
//! let timelog = Logfile::new("./timelog.txt" )?;
//!
//! let task = timelog.last_line();
//! println!("{:?}", task);
//!
//! timelog.add_task("+Project @Task More detail");
//! #   std::fs::remove_file(timelog.clone_file()).expect("Oops");
//! #   Ok(())
//! # }
//! ```

use std::fmt::{self, Display};
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::num::NonZeroU32;
use std::path::Path;
use std::result;
use std::time::Duration;

use crate::buf_reader;
#[doc(inline)]
use crate::date::{DateTime, Time};
#[doc(inline)]
use crate::entry::{Entry, EntryError, EntryKind};
#[doc(inline)]
use crate::error::Error;
#[doc(inline)]
use crate::error::PathError;
#[doc(inline)]
use crate::file;

const TWELVE_HOURS: u64 = 12 * 3600;

/// Problems that can be found in a logfile
///
/// Problems contain the line number where the problem was discovered, except FileAccess.
#[derive(Debug, Eq, PartialEq)]
pub enum Problem {
    FileAccess,
    BlankLine(usize),
    InvalidTimeStamp(usize),
    MissingTask(usize),
    InvalidMarker(usize),
    EventsOrder(usize),
    EventLength(usize)
}

impl Display for Problem {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let (msg, lineno) = match self {
            Self::FileAccess => return write!(f, "Error: Unable to open file"),
            Self::BlankLine(n) => ("Error: Blank entry line", n),
            Self::InvalidTimeStamp(n) => ("Error: Time stamp is invalid or missing", n),
            Self::MissingTask(n) => ("Error: Task missing from entry line", n),
            Self::InvalidMarker(n) => ("Error: Unrecognized marker character", n),
            Self::EventsOrder(n) => ("Error: Entries out of order", n),
            Self::EventLength(n) => ("Warn: Very long interval, possibly missing stop", n)
        };
        write!(f, "Line {lineno}: {msg}")
    }
}

impl Problem {
    fn from_error(err: EntryError, lineno: usize) -> Self {
        match err {
            EntryError::BlankLine => Self::BlankLine(lineno),
            EntryError::InvalidTimeStamp => Self::InvalidTimeStamp(lineno),
            EntryError::MissingTask => Self::MissingTask(lineno),
            EntryError::InvalidMarker => Self::InvalidMarker(lineno)
        }
    }
}

/// A [`Logfile`] type that wraps the timelog log file.
#[derive(Debug)]
pub struct Logfile(String);

impl Logfile {
    /// Creates a [`Logfile`] object wrapping the supplied file.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FilenameMissing`] if the `file` has no filename.
    /// - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
    /// - Return [`PathError::InvalidTimelogPath`] if stack path is invalid.
    pub fn new(file: &str) -> result::Result<Self, PathError> {
        file::canonical_filename(file, file::FileKind::LogFile).map(Self)
    }

    /// Open the log file, return a [`File`].
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if unable to open the file.
    pub fn open(&self) -> result::Result<File, PathError> {
        File::open(&self.0).map_err(|e| PathError::FileAccess(self.0.clone(), e.to_string()))
    }

    /// Clone the filename
    pub fn clone_file(&self) -> String { self.0.clone() }

    /// Return `true` if the timelog file exists
    pub fn exists(&self) -> bool { Path::new(&self.0).exists() }

    /// Append the supplied line (including time stamp) to the timelog file
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    pub fn add_line(&self, entry: &str) -> result::Result<(), PathError> {
        let file = file::append_open(&self.0)?;
        let mut stream = io::BufWriter::new(file);
        writeln!(&mut stream, "{entry}")
            .map_err(|e| PathError::FileWrite(self.clone_file(), e.to_string()))?;
        stream
            .flush()
            .map_err(|e| PathError::FileWrite(self.clone_file(), e.to_string()))?;
        Ok(())
    }

    /// Append the supplied task to the timelog file
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    pub fn add_task(&self, task: &str) -> result::Result<(), PathError> {
        self.add_entry(&Entry::new(task, DateTime::now()))
    }

    /// Append the supplied [`Entry`] to the timelog file
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    pub fn add_entry(&self, entry: &Entry) -> result::Result<(), PathError> {
        let line = format!("{entry}");
        self.add_line(&line)
    }

    /// Add a comment line to the timelog file
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    pub fn add_comment(&self, comment: &str) -> result::Result<(), PathError> {
        let file = file::append_open(&self.0)?;
        let mut stream = io::BufWriter::new(file);
        writeln!(&mut stream, "# {comment}")
            .map_err(|e| PathError::FileWrite(self.clone_file(), e.to_string()))?;
        stream
            .flush()
            .map_err(|e| PathError::FileWrite(self.clone_file(), e.to_string()))?;
        Ok(())
    }

    /// Add a zero duration event to the timelog file
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    pub fn add_event(&self, line: &str) -> result::Result<(), PathError> {
        self.add_entry(&Entry::new_marked(line, DateTime::now(), EntryKind::Event))
    }

    /// Remove the most recent task from the logfile.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    pub fn discard_line(&self) -> result::Result<(), PathError> {
        let mut file = file::rw_open(&self.0)?;
        file::pop_last_line(&mut file);
        Ok(())
    }

    // Wrapper method that extracts the latest entry, calls a function on it
    // to produce a new entry and adds that one to replace the original.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    fn change_last_entry<F>(&self, func: F) -> result::Result<(), Error>
    where
        F: FnOnce(Entry) -> result::Result<Entry, Error>
    {
        let mut file = file::rw_open(&self.0)?;
        if let Some(line) = file::pop_last_line(&mut file) {
            let old_entry = Entry::from_line(&line)?;
            if old_entry.is_stop() { return Err(Error::InvalidStopEdit); }
            if old_entry.is_ignore() { return Err(Error::InvalidIgnoreEdit); }
            match func(old_entry) {
                Ok(entry) => self.add_entry(&entry)?,
                Err(e) => {
                    self.add_line(&line)?;
                    return Err(e);
                }
            }
        }
        Ok(())
    }

    /// Reset most recent entry to current date time.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    pub fn reset_last_entry(&self) -> result::Result<(), Error> {
        self.change_last_entry(|entry| Ok(entry.change_date_time(DateTime::now())))
    }

    /// Ignore the most recent entry to current date time.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    pub fn ignore_last_entry(&self) -> result::Result<(), Error> {
        self.change_last_entry(|entry| Ok(entry.ignore()))
    }

    /// Rewrite the text of the most recent entry.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    pub fn rewrite_last_entry(&self, task: &str) -> result::Result<(), Error> {
        self.change_last_entry(|entry| Ok(entry.change_text(task)))
    }

    /// Reset the time of the most recent entry.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    /// - Return [`Error::InvalidWasArgument`] if supplied time is not parsable.
    pub fn retime_last_entry(&self, time: Time) -> result::Result<(), Error> {
        self.change_last_entry(|entry| {
            let dt = DateTime::new_from_date_time(entry.date(), time);
            Ok(entry.change_date_time(dt))
        })
    }

    /// Shift the time of the most recent entry back the specified number of minutes.
    ///
    /// # Errors
    ///
    /// - Return [`PathError::FileAccess`] if the file cannot be opened or created.
    /// - Return [`PathError::FileWrite`] if the function fails to append to the file.
    /// - Return [`Error::InvalidWasArgument`] if supplied time is not parsable.
    pub fn rewind_last_entry(&self, minutes: NonZeroU32) -> result::Result<(), Error> {
        let dur = Duration::from_secs(u64::from(minutes.get()) * 60);
        self.change_last_entry(|entry| {
            let dt = (entry.date_time() - dur)?;
            Ok(entry.change_date_time(dt))
        })
    }

    /// Return the unfiltered last line of the timelog file or `None` if we can't.
    pub fn raw_last_line(&self) -> Option<String> {
        if self.exists() {
            let file = File::open(&self.0).ok()?;
            io::BufReader::new(file).lines().map_while(result::Result::ok).last()
        }
        else {
            None
        }
    }

    /// Return the last line of the timelog file or `None` if we can't.
    pub fn last_line(&self) -> Option<String> {
        if self.exists() {
            let file = File::open(&self.0).ok()?;
            io::BufReader::new(file)
                .lines()
                .map_while(Result::ok)
                .filter(|ln| !ln.starts_with('#') && EntryKind::from_entry_line(ln).is_start())
                .last()
        }
        else {
            None
        }
    }

    /// Return the last line of the timelog file as an [`Entry`].
    ///
    /// # Errors
    ///
    /// - Return [`Error::InvalidEntryLine`] if the line is not correctly formatted.
    pub fn last_entry(&self) -> result::Result<Entry, Error> {
        Entry::from_line(&self.last_line().unwrap_or_default()).map_err(Into::into)
    }

    /// Return a Vec of problems found with the file.
    ///
    /// If the Vec is empty, there are no problems.
    pub fn problems(&self) -> Vec<Problem> {
        if !self.exists() { return Vec::new(); }
        let Ok(file) = self.open() else {
            return vec![Problem::FileAccess];
        };

        let mut problems: Vec<Problem> = Vec::new();
        let mut iter = buf_reader(file);
        let Some(line) = iter.next() else { return problems; };
        let mut prev = match Entry::from_line(&line) {
            Ok(ev) => ev,
            Err(e) => {
                problems.push(Problem::from_error(e, 1));
                return problems;
            }
        };
        let twelve_hour_dur = Duration::from_secs(TWELVE_HOURS);
        for (line, lineno) in iter.zip(2..) {
            match Entry::from_line(&line) {
                Ok(ev) => {
                    if prev.date_time() > ev.date_time() {
                        problems.push(Problem::EventsOrder(lineno));
                    }
                    else if !prev.is_stop() && !prev.is_event() {
                        let diff = (ev.date_time() - prev.date_time()).unwrap_or_default();
                        if diff > twelve_hour_dur {
                            problems.push(Problem::EventLength(lineno));
                        }
                    }
                    prev = ev;
                }
                Err(e) => problems.push(Problem::from_error(e, lineno))
            }
        }

        problems
    }
}

#[cfg(test)]
mod tests {
    use std::fs::{canonicalize, OpenOptions};

    use regex::Regex;
    use spectral::prelude::*;
    use tempfile::TempDir;

    use super::*;
    use crate::date::Date;
    use crate::DateTime;

    fn make_timelog(lines: &Vec<String>) -> (TempDir, String) {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("timelog.txt");
        let filename = path.to_str().unwrap();
        let file = OpenOptions::new()
            .create(true)
            .append(true)
            .open(filename)
            .unwrap();
        let mut stream = io::BufWriter::new(file);
        lines
            .iter()
            .for_each(|line| writeln!(&mut stream, "{line}").unwrap());
        stream.flush().unwrap();
        (tmpdir, filename.to_string())
    }

    fn touch_timelog() -> (TempDir, String) { make_timelog(&vec![String::new()]) }

    // new

    #[test]
    fn test_new() {
        let logfile = Logfile::new("./foo.txt").unwrap();
        let expected = canonicalize(".")
            .map(|mut pb| {
                pb.push("foo.txt");
                pb.to_str().unwrap().to_string()
            })
            .unwrap_or("".to_string());
        assert_that!(logfile.clone_file()).is_equal_to(&expected);
    }

    #[test]
    fn test_new_empty_name() {
        assert_that!(Logfile::new(""))
            .is_err()
            .is_equal_to(&PathError::FilenameMissing);
    }

    #[test]
    fn test_new_bad_path() {
        assert_that!(Logfile::new("./xyzzy/foo.txt")).is_err_containing(PathError::InvalidPath(
            "./xyzzy/foo.txt".to_string(),
            "No such file or directory (os error 2)".to_string()
        ));
    }

    // exists

    #[test]
    fn test_exists_false() {
        let logfile = Logfile::new("./foo.txt").unwrap();
        assert_that!(logfile.exists()).is_false();
    }

    #[test]
    fn test_exists_true() {
        let (_tmpdir, filename) = touch_timelog();
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.exists()).is_true();
    }

    // add

    #[test]
    fn test_add_line() {
        let (_tmpdir, filename) = touch_timelog();
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.add_line("2021-11-18 18:00:00 +project @task")).is_ok();
        assert_that!(logfile.last_line())
            .contains_value(&String::from("2021-11-18 18:00:00 +project @task"));
    }

    #[test]
    fn test_add_entry() {
        let (_tmpdir, filename) = touch_timelog();
        let logfile = Logfile::new(&filename).unwrap();
        let entry = Entry::new(
            "+project @task",
            DateTime::try_from("2021-11-18 18:00:00").expect("Bad date")
        );
        assert_that!(logfile.add_entry(&entry)).is_ok();
        assert_that!(logfile.last_line())
            .contains_value(&String::from("2021-11-18 18:00:00 +project @task"));
    }

    #[test]
    fn test_add_comment() {
        let (_tmpdir, filename) = touch_timelog();
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.add_comment("This is a test")).is_ok();
        assert_that!(logfile.raw_last_line()).contains_value(&String::from("# This is a test"));
    }

    #[test]
    fn test_add_event() {
        let (_tmpdir, filename) = touch_timelog();
        let logfile = Logfile::new(&filename).unwrap();
        #[rustfmt::skip]
        let expect = Regex::new(
            r"\A\d{4}-\d\d-\d\d \d\d:\d\d:\d\d\^something happened"
        ).expect("Regex failed");
        assert_that!(logfile.add_event("something happened")).is_ok();
        let last_line = logfile.raw_last_line();
        assert_that!(last_line).is_some();
        assert_that!(last_line.unwrap()).matches(|val| expect.is_match(&val));
    }

    // discard_line

    #[test]
    fn test_discard_line() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.discard_line()).is_ok();
        assert_that!(logfile.last_line()).contains_value("2021-11-18 17:04:02 +bar".to_string());
    }

    // change entry

    #[test]
    fn test_reset_last_entry() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.reset_last_entry()).is_ok();
        let entry = logfile.last_entry().unwrap();
        assert_that!(entry.entry_text()).is_equal_to("+baz");
        assert_that!(entry.date()).is_equal_to(Date::today());
    }

    // TODO: Figure out why test fails, when parallel tests don't
    // #[test]
    // fn test_ignore_last_entry() {
    //     let (_tmpdir, filename) = make_timelog(&vec![
    //         "2021-11-18 17:01:01 +foo".to_string(),
    //         "2021-11-18 17:04:02 +bar".to_string(),
    //         "2021-11-18 17:08:04 +baz".to_string(),
    //     ]);
    //     let expect = Entry::from_line("2021-11-18 17:08:04 +baz").unwrap().ignore();
    //     let logfile = Logfile::new(&filename).unwrap();
    //     assert_that!(logfile.ignore_last_entry()).is_ok();
    //     assert_that!(logfile.last_entry()).is_ok().is_equal_to(&expect);
    // }

    #[test]
    fn test_rewrite_last_entry() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
        ]);
        let expect = Entry::from_line("2021-11-18 17:08:04 +foobar @Frond").unwrap();
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.rewrite_last_entry("+foobar @Frond")).is_ok();
        assert_that!(logfile.last_entry()).is_ok().is_equal_to(&expect);
    }

    #[test]
    fn test_retime_last_entry() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
        ]);
        let expect = Entry::from_line("2021-11-18 16:01:00 +baz").unwrap();
        let logfile = Logfile::new(&filename).unwrap();
        let time = Time::from_hms_opt(16, 1, 0).expect("");
        assert_that!(logfile.retime_last_entry(time)).is_ok();
        assert_that!(logfile.last_entry()).is_ok().is_equal_to(&expect);
    }

    #[test]
    fn test_rewind_last_entry() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
        ]);
        let expect = Entry::from_line("2021-11-18 16:57:04 +baz").unwrap();
        let logfile = Logfile::new(&filename).unwrap();
        let minutes = NonZeroU32::new(11).expect("Hard-coded legal NonZero value");
        assert_that!(logfile.rewind_last_entry(minutes)).is_ok();
        assert_that!(logfile.last_entry()).is_ok().is_equal_to(&expect);
    }

    // last_line

    #[test]
    fn test_last_line_missing() {
        let (_tmpdir, filename) = touch_timelog();
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.last_line()).contains_value(String::new());
    }

    #[test]
    fn test_last_line_empty() {
        let (_tmpdir, filename) = make_timelog(&vec![]);
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.last_line()).is_none();
    }

    #[test]
    fn test_last_line_lines() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.last_line()).contains_value(&"2021-11-18 17:08:04 +baz".to_string());
    }

    #[test]
    fn test_last_entry() {
        let (_tmpdir, filename) = make_timelog(&vec![]);
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.last_entry())
            .is_err()
            .is_equal_to(&EntryError::BlankLine.into());
    }

    #[test]
    fn test_last_entry_lines() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        let expected = Entry::from_line("2021-11-18 17:08:04 +baz").unwrap();
        assert_that!(logfile.last_entry())
            .is_ok()
            .is_equal_to(&expected);
    }

    #[test]
    fn test_problems_all_good() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
            "2021-11-18 17:08:04 stop".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.problems()).is_empty();
    }

    #[test]
    fn test_problems_all_good_with_comments() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "# Start of file".to_string(),
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "# Middle of file".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
            "2021-11-18 17:08:04 stop".to_string(),
            "# End of file".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        assert_that!(logfile.problems()).is_empty();
    }

    #[test]
    fn test_problems_blank_line() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        let problems = logfile.problems();
        assert_that!(problems).has_length(1);
        assert_that!(problems.iter()).contains(Problem::BlankLine(2));
    }

    #[test]
    fn test_problems_bad_date() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-1-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
            "2021-11-18 17:08:04 stop".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        let problems = logfile.problems();
        assert_that!(problems).has_length(1);
        assert_that!(problems.iter()).contains(Problem::InvalidTimeStamp(1));
    }

    #[test]
    fn test_problems_bad_time() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 7:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
            "2021-11-18 17:08:04 stop".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        let problems = logfile.problems();
        assert_that!(problems).has_length(1);
        assert_that!(problems.iter()).contains(Problem::InvalidTimeStamp(1));
    }

    #[test]
    fn test_problems_missing_timestamp() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "+foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
            "2021-11-18 17:08:04 stop".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        let problems = logfile.problems();
        assert_that!(problems).has_length(1);
        assert_that!(problems.iter()).contains(Problem::InvalidTimeStamp(1));
    }

    #[test]
    fn test_problems_no_task() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 ".to_string(),
            "2021-11-18 17:08:04 stop".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        let problems = logfile.problems();
        assert_that!(problems).has_length(1);
        assert_that!(problems.iter()).contains(Problem::MissingTask(3));
    }

    #[test]
    fn test_problems_unknown_marker() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02*+bar".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
            "2021-11-18 17:08:04 stop".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        let problems = logfile.problems();
        assert_that!(problems).has_length(1);
        assert_that!(problems.iter()).contains(Problem::InvalidMarker(2));
    }

    #[test]
    fn test_problems_entry_unordered() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:08:04 +baz".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-18 17:08:04 stop".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        let problems = logfile.problems();
        assert_that!(problems).has_length(1);
        assert_that!(problems.iter()).contains(Problem::EventsOrder(3));
    }

    #[test]
    fn test_problems_open_ended() {
        let (_tmpdir, filename) = make_timelog(&vec![
            "2021-11-18 17:01:01 +foo".to_string(),
            "2021-11-18 17:04:02 +bar".to_string(),
            "2021-11-19 17:08:04 +baz".to_string(),
            "2021-11-19 17:08:04 stop".to_string(),
        ]);
        let logfile = Logfile::new(&filename).unwrap();
        let problems = logfile.problems();
        assert_that!(problems).has_length(1);
        assert_that!(problems.iter()).contains(Problem::EventLength(3));
    }
}
