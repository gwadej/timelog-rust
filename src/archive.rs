//! Handle the archiving of old records from the timelog.

use std::fmt::{self, Display};
use std::fs::{self, File};
use std::io::prelude::*;
use std::io::{BufRead, BufReader, BufWriter};
use std::path::Path;

use crate::config::Config;
#[doc(inline)]
use crate::date::Date;
#[doc(inline)]
use crate::date::DateTime;
#[doc(inline)]
use crate::entry::{Entry, EntryError};
#[doc(inline)]
use crate::error::PathError;
#[doc(inline)]
use crate::logfile::Logfile;

#[derive(Debug, Default)]
struct EntryLine {
    comments: Vec<String>,
    line:     Option<String>
}

// Representation of an entry line with optional leading comment lines.
impl EntryLine {
    // Add an optional text line to the [`EntryLine`] object.
    //
    // If the supplied optional line is None, do nothing and return false.
    // If the supplied line starts with the comment character, store as leading
    // comment and return false.
    // If the supplied line is not a comment, add as the entry line and return true.
    fn add_line<OS>(&mut self, oline: OS) -> bool
    where
        OS: Into<Option<String>>
    {
        if let Some(line) = oline.into() {
            if Entry::is_comment_line(&line) {
                self.comments.push(line);
            }
            else {
                self.line = Some(line);
                return true;
            }
        }
        false
    }

    // Extract the year from the contained [`EntryLine`] returning an optional year number.
    fn extract_year(&self) -> Option<i32> {
        self.line.as_ref().and_then(|ln| Entry::extract_year(ln))
    }

    // Return true if there is an entry line and it is a stop.
    fn is_stop_line(&self) -> bool { self.line.as_ref().map_or(false, |l| Entry::is_stop_line(l)) }

    // Convert the entry line to an entry if it parses and exists.
    //
    // Return None if no Entry line exists.
    // Return Some(Err(EntryError)) if the Entry conversion fails.
    fn to_entry(&self) -> Option<Result<Entry, EntryError>> {
        self.line.as_ref().map(|l| Entry::from_line(l))
    }

    // Convert the [`EntryLine`] to an optional EntryLine.
    //
    // If there are no comments and no entry line, return None.
    // Otherwise, return itself as a Some().
    fn make_option(self) -> Option<Self> {
        (!self.comments.is_empty() || self.line.is_some()).then_some(self)
    }
}

impl Display for EntryLine {
    // Format the [`EntryLine`] for display.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut iter = self.comments.iter().chain(self.line.iter());
        if let Some(line) = iter.next() {
            write!(f, "{line}")?;
            for line in iter {
                write!(f, "\n{line}")?;
            }
        }
        Ok(())
    }
}

// Iterator that returns [`EntryLine`]s.
struct EntryLineIter<'a> {
    lines: std::io::Lines<BufReader<&'a File>>
}

impl<'a> EntryLineIter<'a> {
    // Create an [`EntryLineIter`] from a [`File`] reference.
    #[rustfmt::skip]
    pub fn new(file: &'a File) -> Self {
        Self { lines: BufReader::new(file).lines() }
    }
}

impl<'a> Iterator for EntryLineIter<'a> {
    type Item = EntryLine;

    // Return next [`EntryLine`] if it exists.
    fn next(&mut self) -> Option<Self::Item> {
        let mut eline = EntryLine::default();
        for line in self.lines.by_ref() {
            if eline.add_line(line.ok()) {
                return eline.make_option();
            }
        }
        eline.make_option()
    }
}

/// Configuration for archiving previous year information from the timelog.txt file.
pub(crate) struct Archiver<'a> {
    // Reference to the rtimelog configuration.
    config:    &'a Config,
    // The current year
    curr_year: i32,
    // The name for the new file created to store the previous year.
    new_file:  String,
    // The name for the backup file.
    back_file: String
}

impl<'a> Archiver<'a> {
    /// Create a new Archiver object.
    pub(crate) fn new(config: &'a Config) -> Self {
        let logfile = config.logfile();
        Self {
            config,
            curr_year: Date::today().year(),
            new_file: format!("{logfile}.new"),
            back_file: format!("{logfile}.bak")
        }
    }

    // Return the [`Logfile`] object representing the file on disk
    //
    // # Errors
    //
    // - Return [`PathError::FilenameMissing`] if the `file` has no filename.
    // - Return [`PathError::InvalidPath`] if the path part of `file` is not a valid path.
    fn logfile(&self) -> crate::Result<Logfile> {
        Logfile::new(&self.config.logfile()).map_err(Into::into)
    }

    // Return an appropriate path/filename for the supplied year.
    fn archive_filepath(&self, year: i32) -> String {
        format!("{}/timelog-{year}.txt", self.config.dir())
    }

    // Return a [`BufWriter`] wrapping the file for writing to the supplied filename.
    //
    // # Errors
    //
    // - Return [`PathError::FileAccess`] if unable to open the file.
    fn archive_writer(filename: &str) -> crate::Result<BufWriter<File>> {
        let file = fs::OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(filename)
            .map_err(|e| PathError::FileAccess(filename.to_string(), e.to_string()))?;
        Ok(BufWriter::new(file))
    }

    /// Archive the first (non-current) year from the logfile.
    ///
    /// - Return Ok(Some(year)) if `year` was archived.
    /// - Return Ok(None) if no previous year to archive.
    ///
    /// # Errors
    ///
    /// - Return [`Error::PathError`] for any error accessing the log or archive files.
    pub(crate) fn archive(&self) -> crate::Result<Option<i32>> {
        let file = self.logfile()?.open()?;
        let mut elines = EntryLineIter::new(&file);
        let Some(first) = elines.next() else { return Ok(None); };

        let arc_year = first.extract_year().ok_or(EntryError::InvalidTimeStamp)?;
        if arc_year >= self.curr_year { return Ok(None); }

        let logfile = self.config.logfile();
        let archive_filename = self.archive_filepath(arc_year);
        if Path::new(&archive_filename).exists() {
            return Err(PathError::AlreadyExists(archive_filename).into());
        }
        let mut arc_stream = Self::archive_writer(&archive_filename)?;
        let mut new_stream = Self::archive_writer(&self.new_file)?;

        writeln!(&mut arc_stream, "{first}")
            .map_err(|e| PathError::FileWrite(archive_filename.clone(), e.to_string()))?;

        let mut prev = Some(first);
        let mut save = false;
        for line in elines {
            save = line.extract_year().map_or(save, |y| y == arc_year);

            let mut stream = if save {
                &mut arc_stream
            }
            else if let Some(pline) = prev {
                if !pline.is_stop_line() {
                    if let Some(ev) = pline.to_entry() {
                        let entry = ev?;
                        // finish archive file
                        writeln!(&mut arc_stream, "{}", Self::entry_end_year(&entry)).map_err(
                            |e| PathError::FileWrite(archive_filename.clone(), e.to_string())
                        )?;
                        // start new file
                        writeln!(&mut new_stream, "{}", Self::entry_next_year(&entry)).map_err(
                            |e| PathError::FileWrite(archive_filename.clone(), e.to_string())
                        )?;
                    }
                }
                prev = None;
                &mut new_stream
            }
            else {
                &mut new_stream
            };

            writeln!(&mut stream, "{line}")
                .map_err(|e| PathError::FileWrite(archive_filename.clone(), e.to_string()))?;
            if save {
                prev = Some(line);
            }
        }

        Self::flush(&archive_filename, &mut arc_stream)?;
        Self::flush(&self.new_file, &mut new_stream)?;
        Self::rename(&logfile, &self.back_file)?;
        Self::rename(&self.new_file, &logfile)?;

        Ok(Some(arc_year))
    }

    /// Clone supplied [`Entry`] moved to the beginning of the next year
    #[rustfmt::skip]
    fn entry_end_year(entry: &Entry) -> Entry {
        Entry::new_stop(
            DateTime::new((entry.date().year() + 1, 1, 1), (0, 0, 0)).expect("Not at end of time")
        )
    }

    /// Clone supplied [`Entry`] moved to the beginning of the next year
    fn entry_next_year(entry: &Entry) -> Entry {
        Entry::new(
            entry.entry_text(),
            DateTime::new((entry.date().year() + 1, 1, 1), (0, 0, 0)).expect("Not at end of time")
        )
    }

    // Utility method for flushing a writer and properly reporting any error.
    fn flush(filename: &str, stream: &mut BufWriter<File>) -> crate::Result<()> {
        stream
            .flush()
            .map_err(|e| PathError::FileWrite(filename.to_string(), e.to_string()).into())
    }

    // Utility method for renaming a file and properly reporting any error.
    fn rename(old: &str, new: &str) -> crate::Result<()> {
        fs::rename(old, new)
            .map_err(|e| PathError::RenameFailure(old.to_string(), e.to_string()).into())
    }
}

#[cfg(test)]
mod tests {
    use std::iter::once;

    use spectral::prelude::*;
    use tempfile::TempDir;

    use super::*;
    use crate::Date;

    fn make_timelog(lines: &Vec<String>) -> (TempDir, String) {
        let tmpdir = TempDir::new().expect("Cannot make tempfile");
        let mut path = tmpdir.path().to_path_buf();
        path.push("timelog.txt");
        let filename = path.to_str().unwrap();
        let file = fs::OpenOptions::new()
            .create(true)
            .append(true)
            .open(filename)
            .unwrap();
        let mut stream = BufWriter::new(file);
        lines
            .iter()
            .for_each(|line| writeln!(&mut stream, "{}", line).unwrap());
        stream.flush().unwrap();
        (tmpdir, filename.to_string())
    }

    #[test]
    fn test_new() {
        let config = Config::default();
        let arch = Archiver::new(&config);
        let logfile = config.logfile();
        assert_that!(arch.config).is_equal_to(&config);
        assert_that!(arch.curr_year).is_equal_to(&(Date::today().year()));
        assert_that!(arch.new_file).is_equal_to(&format!("{}.new", logfile));
        assert_that!(arch.back_file).is_equal_to(&format!("{}.bak", logfile));
    }

    #[test]
    fn test_archive_filepath() {
        let config = Config::default();
        let arch = Archiver::new(&config);
        let expect = format!("{}/timelog-{}.txt", config.dir(), 2011);
        assert_that!(arch.archive_filepath(2011)).is_equal_to(&expect);
    }

    #[test]
    fn test_archive_this_year() {
        let curr_year = Date::today().year();
        let (tmpdir, _filename) = make_timelog(&vec![
            format!("{curr_year}-02-10 09:01:00 +foo"),
            format!("{curr_year}-02-10 09:10:00 stop"),
            format!("{curr_year}-02-15 09:01:00 +foo"),
            format!("{curr_year}-02-15 09:01:00 stop"),
        ]);
        #[rustfmt::skip]
        let config = Config::new(
            ".timelog",
            Some(tmpdir.path().to_str().expect("tempdir failed to return string")),
            Some("vim"),
            None,
            None
        ).expect("Legal config");
        let arch = Archiver::new(&config);
        assert_that!(arch.archive()).is_ok().is_none();
    }

    #[test]
    fn test_archive_this_year_with_comments() {
        let curr_year = Date::today().year();
        let (tmpdir, _filename) = make_timelog(&vec![
            String::from("# Initial comment"),
            format!("{curr_year}-02-10 09:01:00 +foo"),
            format!("{curr_year}-02-10 09:10:00 stop"),
            String::from("# Middle comment"),
            format!("{curr_year}-02-15 09:01:00 +foo"),
            format!("{curr_year}-02-15 09:01:00 stop"),
            String::from("# Trailing comment"),
        ]);
        #[rustfmt::skip]
        let config = Config::new(
            ".timelog",
            Some(tmpdir.path().to_str().expect("tempdir failed to return string")),
            Some("vim"),
            None,
            None
        ).expect("Legal config");
        let arch = Archiver::new(&config);
        assert_that!(arch.archive()).is_ok().is_none();
    }

    #[test]
    fn test_archive_prev_year() {
        let prev_year = Date::today().year() - 1;
        let (tmpdir, filename) = make_timelog(&vec![
            format!("{prev_year}-02-10 09:01:00 +foo"),
            format!("{prev_year}-02-10 09:10:00 stop"),
            format!("{prev_year}-02-15 09:01:00 +foo"),
            format!("{prev_year}-02-15 09:01:00 stop"),
        ]);
        let expected = std::fs::read_to_string(&filename).expect("Could not read archive");
        #[rustfmt::skip]
        let config = Config::new(
            ".timelog",
            Some(tmpdir.path().to_str().expect("tempdir failed to return string")),
            Some("vim"),
            None,
            None
        ).expect("Legal config");
        let arch = Archiver::new(&config);
        assert_that!(arch.archive()).is_ok().contains(prev_year);

        let archive_file = arch.archive_filepath(prev_year);
        let actual = std::fs::read_to_string(&archive_file).expect("Could not read archive");
        assert_that!(actual).is_equal_to(&expected);

        let metadata = std::fs::metadata(&filename).expect("metadata failed");
        assert_that!(metadata.is_file()).is_true();
        assert_that!(metadata.len()).is_equal_to(0u64);
    }

    #[test]
    fn test_archive_split_years() {
        let curr_year = Date::today().year();
        let prev_year = curr_year - 1;
        let prev_year_lines = vec![
            format!("{prev_year}-12-10 19:01:00 +foo"),
            format!("{prev_year}-12-10 19:10:00 stop"),
            format!("{prev_year}-12-15 19:01:00 +foo"),
            format!("{prev_year}-12-15 19:01:00 stop"),
        ];
        let curr_year_lines = vec![
            format!("{curr_year}-02-10 09:01:00 +foo"),
            format!("{curr_year}-02-10 09:10:00 stop"),
            format!("{curr_year}-02-15 09:01:00 +foo"),
            format!("{curr_year}-02-15 09:01:00 stop"),
        ];
        let mut expected = curr_year_lines.join("\n");
        expected.push_str("\n");
        let lines: Vec<String> = prev_year_lines
            .iter()
            .chain(curr_year_lines.iter())
            .cloned()
            .collect();
        let (tmpdir, filename) = make_timelog(&lines);
        #[rustfmt::skip]
        let config = Config::new(
            ".timelog",
            Some(tmpdir.path().to_str().expect("tempdir failed to return string")),
            Some("vim"),
            None,
            None
        ).expect("Legal config");
        let arch = Archiver::new(&config);
        assert_that!(arch.archive()).is_ok().contains(prev_year);

        let actual = std::fs::read_to_string(&filename).expect("Could not read archive");
        assert_that!(actual).is_equal_to(&expected);

        let mut expected = prev_year_lines.join("\n");
        expected.push_str("\n");
        let archive_file = arch.archive_filepath(prev_year);
        let actual = std::fs::read_to_string(&archive_file).expect("Could not read archive");
        assert_that!(actual).is_equal_to(&expected);
    }

    #[test]
    fn test_archive_split_years_with_comments() {
        let curr_year = Date::today().year();
        let prev_year = curr_year - 1;
        let prev_year_lines = vec![
            String::from("# Initial commment"),
            format!("{prev_year}-12-10 19:01:00 +foo"),
            format!("{prev_year}-12-10 19:10:00 stop"),
            format!("{prev_year}-12-15 19:01:00 +foo"),
            format!("{prev_year}-12-15 19:01:00 stop"),
        ];
        let curr_year_lines = vec![
            String::from("# Breaking commment"),
            format!("{curr_year}-02-10 09:01:00 +foo"),
            format!("{curr_year}-02-10 09:10:00 stop"),
            format!("{curr_year}-02-15 09:01:00 +foo"),
            format!("{curr_year}-02-15 09:01:00 stop"),
            String::from("# Trailing commment"),
        ];
        let mut expected = curr_year_lines.join("\n");
        expected.push_str("\n");
        let lines: Vec<String> = prev_year_lines
            .iter()
            .chain(curr_year_lines.iter())
            .cloned()
            .collect();
        let (tmpdir, filename) = make_timelog(&lines);
        #[rustfmt::skip]
        let config = Config::new(
            ".timelog",
            Some(tmpdir.path().to_str().expect("tempdir failed to return string")),
            Some("vim"),
            None,
            None
        ).expect("Legal config");
        let arch = Archiver::new(&config);
        assert_that!(arch.archive()).is_ok().contains(prev_year);

        let actual = std::fs::read_to_string(&filename).expect("Could not read archive");
        assert_that!(actual).is_equal_to(&expected);

        let mut expected = prev_year_lines.join("\n");
        expected.push_str("\n");
        let archive_file = arch.archive_filepath(prev_year);
        let actual = std::fs::read_to_string(&archive_file).expect("Could not read archive");
        assert_that!(actual).is_equal_to(&expected);
    }

    #[test]
    fn test_archive_split_years_task_crosses() {
        let curr_year = Date::today().year();
        let prev_year = curr_year - 1;
        let prev_year_lines = vec![
            format!("{prev_year}-12-10 19:01:00 +foo"),
            format!("{prev_year}-12-10 19:10:00 stop"),
            format!("{prev_year}-12-15 19:01:00 +foo"),
            format!("{prev_year}-12-15 19:01:00 stop"),
            format!("{prev_year}-12-31 23:01:00 +bar"),
        ];
        let curr_year_lines = vec![
            format!("{curr_year}-01-01 00:10:00 stop"),
            format!("{curr_year}-02-10 09:01:00 +foo"),
            format!("{curr_year}-02-10 09:10:00 stop"),
            format!("{curr_year}-02-15 09:01:00 +foo"),
            format!("{curr_year}-02-15 09:01:00 stop"),
        ];
        let mut expected = once(&format!("{}-01-01 00:00:00 +bar", curr_year))
            .chain(curr_year_lines.iter())
            .cloned()
            .collect::<Vec<String>>()
            .join("\n");
        expected.push_str("\n");
        let lines: Vec<String> = prev_year_lines
            .iter()
            .chain(curr_year_lines.iter())
            .cloned()
            .collect();
        let (tmpdir, filename) = make_timelog(&lines);
        #[rustfmt::skip]
        let config = Config::new(
            ".timelog",
            Some(tmpdir.path().to_str().expect("tempdir failed to return string")),
            Some("vim"),
            None,
            None
        ).expect("Legal config");
        let arch = Archiver::new(&config);
        assert_that!(arch.archive()).is_ok().contains(prev_year);

        let actual = std::fs::read_to_string(&filename).expect("Could not read archive");
        assert_that!(actual).is_equal_to(&expected);

        let mut expected = prev_year_lines
            .iter()
            .chain(once(&format!("{curr_year}-01-01 00:00:00 stop")))
            .cloned()
            .collect::<Vec<String>>()
            .join("\n");
        expected.push_str("\n");
        let archive_file = arch.archive_filepath(prev_year);
        let actual = std::fs::read_to_string(&archive_file).expect("Could not read archive");
        assert_that!(actual).is_equal_to(&expected);
    }

    #[test]
    fn test_archive_split_years_task_crosses_with_comments() {
        let curr_year = Date::today().year();
        let prev_year = curr_year - 1;
        let prev_year_lines = vec![
            String::from("# Initial comment"),
            format!("{prev_year}-12-10 19:01:00 +foo"),
            format!("{prev_year}-12-10 19:10:00 stop"),
            format!("{prev_year}-12-15 19:01:00 +foo"),
            format!("{prev_year}-12-15 19:01:00 stop"),
            format!("{prev_year}-12-31 23:01:00 +bar"),
        ];
        let curr_year_lines = vec![
            String::from("# Split comment"),
            format!("{curr_year}-01-01 00:10:00 stop"),
            format!("{curr_year}-02-10 09:01:00 +foo"),
            format!("{curr_year}-02-10 09:10:00 stop"),
            format!("{curr_year}-02-15 09:01:00 +foo"),
            format!("{curr_year}-02-15 09:01:00 stop"),
            String::from("# Trailing comment"),
        ];
        let mut expected = once(&format!("{}-01-01 00:00:00 +bar", curr_year))
            .chain(curr_year_lines.iter())
            .cloned()
            .collect::<Vec<String>>()
            .join("\n");
        expected.push_str("\n");
        let lines: Vec<String> = prev_year_lines
            .iter()
            .chain(curr_year_lines.iter())
            .cloned()
            .collect();
        let (tmpdir, filename) = make_timelog(&lines);
        #[rustfmt::skip]
        let config = Config::new(
            ".timelog",
            Some(tmpdir.path().to_str().expect("tempdir failed to return string")),
            Some("vim"),
            None,
            None
        ).expect("Legal config");
        let arch = Archiver::new(&config);
        assert_that!(arch.archive()).is_ok().contains(prev_year);

        let actual = std::fs::read_to_string(&filename).expect("Could not read archive");
        assert_that!(actual).is_equal_to(&expected);

        let mut expected = prev_year_lines
            .iter()
            .chain(once(&format!("{curr_year}-01-01 00:00:00 stop")))
            .cloned()
            .collect::<Vec<String>>()
            .join("\n");
        expected.push_str("\n");
        let archive_file = arch.archive_filepath(prev_year);
        let actual = std::fs::read_to_string(&archive_file).expect("Could not read archive");
        assert_that!(actual).is_equal_to(&expected);
    }
}
