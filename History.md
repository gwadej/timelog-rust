# History of timelog/rtimelog

## INTRODUCTION

In a job a long time ago, I found that I would regularly reach the end of a
long, tiring day and have no idea what I had done. Just as regularly, I found
that my most important projects were not progressing very rapidly. Many people
find themselves in the same position. I decided to do something about it, I
found an application on the Palm that tracked Clients, Projects, and Tasks (as
well as mileage and expenses) called TEAK.

I started tracking everything I did at work. After a few days, I had identified
many of the interruptions that prevented me from making progress. I had also
noted that some of my busiest, yet least productive days were almost all
interruptions. I used this information to improve my work and to keep people
updated with why my various projects were not progressing as expected. When I
needed to make faster progress, I had the data to help my manager redirect the
worst of the interruptions.

### The Need for Change

This was wonderful. Unfortunately, the Palm platform did not survive in the
market. I continued to keep my Palm functional partially to use TEAK. But, I
eventually had to admit that I could not rely on my Palm to continue. By this
time I had an Android smart phone and I figured it would be pretty easy to find
something to replace TEAK. No such luck.

I had been using Gina Trapani's Todo.txt program for a while at this point.
It's main feature is the simplicity of its file format:

- One entry per line
- pure text
- minimal formatting

I decided that this would be a better basis for a new time logging program than
some binary format. So I wrote a quickie Perl script to allow me to start and
stop task timers and to generate simple reports.

As I used the program, I found other commands and reports that could be useful.
In the end, it was obvious that this tool needed to be cleaned up a re-written.
The Perl module version was the result of that rewrite. I used that version of
the program for nearly a decade.

While learning Rust, I decided to make a project out of porting of the Perl
program to Rust. Mostly it was an excuse to move beyond the toy exercises that
dominate the beginning of learning any new language. Partly though, it was an
attempt to explore the features of Rust in a closer to _real world_ application
without the risk of depending on my new understanding for something in production.

The current version of the Rust project has diverged from the Perl version, and
implements [version 2][format] of the time log format.

[format]: Format.md

## AUTHOR

G. Wade Johnson  `<rust@gwadej.org>`

## LICENCE AND COPYRIGHT

Copyright (c) 2021-2023, G. Wade Johnson `<rust@gwadej.org>`. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself. See [perlartistic](https://opensource.org/licenses/Artistic-2.0).
