# Tutorial for `rtimelog`

## VERSION

This tutorial applies to `rtimelog` version 1.0.

## Getting Started

The `rtimelog` program requires a directory where it stores its log files. This
directory may be specified in the `.timelogrc` configuration file in your
home directory. If that file does not exist, the program defaults to the
`timelog` directory in your home directory.

You can use the `init` command to set up the configuration file and directory
for you. If the default configuration is satisfactory, just run the following
command:

```
  $ rtimelog init
```

If you would like to change the directory where the logs are stored, supply
that directory path as an argument to the `init` command.

```
  $ rtimelog init ~/my_timelog
```

## Example Usage:

In this example, we will see most of the `rtimelog` commands you would use in a
normal day. The `start` and `stop` commands are the minimum that you would
need, but we will go a bit further.

To begin tracking a task, you use the `start` command and supply text to
describe the task you are beginning. The `rtimelog` program assumes that you
are only working a single thing at a time. I originally tried to support the
ability to track more than one task at a time and found that I never actually
used it.

Let's walk through an example. Assume that it's just after lunch and you want
to take care of your email before your team meeting at 1pm. At 10 minutes to 1,
you start a task named _Email_ in the _Admin_ project.

```
  $ rtimelog start +Admin @Email
```

The first group of non-whitespace characters beginning with '`+`' is considered
the *project*. If there is a string of non-whitespace characters beginning with
'`@`', it is treated as the *task name*. Any other text is explanatory details
for the task.

At 1pm, the conference call with your team begins. You start that task. Since
we are going to be talking about the `Foo` project, note that in the task
description.

```
  $ rtimelog start +Foo @ConfCall Team Meeting
```

When the meeting is over, you need to change tasks. During the meeting, you
agreed to do some general work on the manual for the project's software. Enter
the task to begin working on that.

```
  $ rtimelog start +Foo @Docs working on manual
```

We are in the same project, so `rtimelog` will be able to report the cumulative
total of the last two tasks. About 20 minutes after you start on the manual,
Sue comes by to ask about the project you worked on last week. Since this
is will be a temporary interruption, we use the `push` command. It saves the
current task, so that we can restore it when the interruption is over.

```
  $ rtimelog push +Bar @Help Sue questions
```

When all of Sue's questions are answered, you go back to the manual work using
the **resume** command.

```
  $ rtimelog resume
```

The `resume` command restores the task that was current when the last `push`
or `pause` command was executed. This way you don't need to type in the whole
command (or up-arrow through your command history), for short interruptions.

A while later, you've finished with the manual for today and want to finish up
the work on the database module for the _Foo_ project.

```
  $ rtimelog start +Foo @Code database module
```

You get some good uninterrupted time on this and finish up the module in time
to stop at 5.

```
  $ rtimelog stop
```

As you can see, this requires a bit more to your daily work flow. The goal of
the tool is to provide some benefits to make this extra work worthwhile. That
benefit is the ability to see and understand where you spend time during the
day through reports of your work.

Later in the tutorial we will cover a few features that can reduce the cost of
using the tool.

## Events

In addition to timing tasks, `rtimelog` can be used to track events that don't
have a useful duration. These _zero duration events_ are added to the timelog
with the `event` command. The command tasks a description of the event. For example,

```
   $ rtimelog event +dog @Walk Evening
```

The event description follows the same form as a task. You can think of the
_project_ in this case as being more of a group of events.

## Correcting Entries

There are many reasons why you might not enter an entry exactly correctly:

* you typo the details on a task entry
* you realize that a quick interruption is taking real time
* you begin a task entry for a meeting and find it's not starting yet
* you start working on a new task and then realize that you forgot to enter it

Obviously, you can use the `edit` command to make any changes you need to the
logfile. But, that seems overkill for a simple adjustment. The `rtimelog` program
supports a few more targeted commands for manipulating the last task.

### What if you start a task and decide you won't do it?

Maybe you start a task for a meeting and it gets cancelled. You can use

```
   $ rtimelog entry discard
```

to throw that entry away. If you want to keep a record of the meeting start
but not count it in your time reports, you can use the following instead.

```
   $ rtimelog entry ignore
```

### You accidentally start the task too soon.

I sometimes start the task entry and realize that the activity has
not started yet. (Maybe starting a couple of minutes before the meeting
actually starts.)

```
   $ rtimelog entry now
```

This resets the entry to right now.

### You realize that you are working on an activity and did not add an entry.

This happens more than I would like to admit. So, I immediately add a new entry.
Then I will want to adjust the time. If I only should have started a few minutes
ago, the `rewind` command allows me to move back a number of minutes.

```
   $ rtimelog entry rewind 5
```

On the other hand, if I know the exact time I should have started the task, I would
use the `was` command instead.

```
   $ rtimelog entry was 9:00
```

## Reports

The most detailed report you might do is to list all of the entries for a given
day. If you executed the following command the day after the example above, you
would get a list of the entries for that day.

```
  $ rtimelog ls yesterday
```

The output of this command (assuming only the commands from the example) would
look like this:

    2013-07-01 12:50:02 +Admin @Email
    2013-07-01 13:00:21 +Foo @ConfCall Team Meeting
    2013-07-01 14:02:43 +Foo @Docs working on manual
    2013-07-01 14:22:03 +Bar @Help Sue questions
    2013-07-01 14:27:06 +Foo @Docs working on manual
    2013-07-01 15:10:20 +Foo @Code database module
    2013-07-01 17:00:24 stop

This is the most detailed report, but it isn't really useful for much. Usually,
you want some form of summarized information. The other reporting commands take
care of that.

### Detail Report

The report I use the most looks like the following:

```
  $ rtimelog report detail yesterday
```

This report shows the data for the given date grouped by project. Although the
actual times are missing and the data is re-ordered, you can get a really good
sense of where your time went with this report.

    2013-07-01  4:10
      Admin         0:10
          Email                0:10
      Bar           0:05
          Help                 0:05 (Sue questions)
      Foo           3:55
          Code                 1:50 (database module)
          Docs                 1:03 (working on manual)
          ConfCall             1:02 (Team Meeting)

From this report, we can see that the part of the day we tracked was 4 hours
and 10 minutes. Almost 4 hours of that was spent on the `Foo` project, on three
separate tasks:

* Code work on the database module
* Documentation on the manual
* Team conference call

We also spent some incidental time on email and answering questions.

Where this report really shines are those days that seems to be one interrupt
after another. That was really the origin of the program for me. I found that
there were days when I left work tired, but didn't feel like I had
*accomplished* anything. Looking back at this report showed the day taken up by
dozens of interruptions.

### Summary Report

Sometimes, the normal report is still too much data. Let's say I only care
about the aggregate amount of time spent on each project. I would then use the
following command:

```
  $ rtimelog report summary yesterday
```

This report has no task information, it just reports the time on each project
for the day.

    2013-07-01  4:10
      Admin         0:10
      Bar           0:05
      Foo           3:55

If you only work on one project at a time, this report will not be very useful.
However, I have found this report handy for as few as two projects. For
example, handing off one project to another team, while beginning a new
project. During the beginning of the hand-off, there are usually a number of
meetings and interruptions helping the other team take over. It's not always
easy to realize how much time is spent on this kind of activity.

### Hours Report

Finally, you may only want the amount of time you spent for the whole day. That
report is generated by the following command:

```
  $ rtimelog report hours yesterday
```

This just tells how long you were working. Sometimes, it is all you need.

    2013-07-01  4:10

### Chart

The `report chart` command gives a graphical representation of the time spent during
the day as an HTML page. The data is displayed as a pie chart breaking down the
projects worked during the day. For each project, there is a smaller pie chart
for the tasks applied to that project.

Finally, there is a hourly bar chart showing the time in each hour spent on the
various projects. The [chart report](tutorial/report.html) for the example
shows the output of this command.

I created this report specifically as an exercise after seeing some of the
output from other time tracking programs. Although I use it relatively rarely,
someone who is more visually-oriented might find this version more useful.

### Event Reports

If you are using the zero duration events feature of `rtimelog`, you might want
to generate reports for those.

The `report events` command lists your events and the time that they were
added grouped by date.

```
    $ rtimelog report events -p dog yesterday today
```

If you used events to track letting the dog out, the command could give a report of

    2022-12-15
      07:50  +dog Backyard
      14:22  +dog Backyard
      18:33  +dog Walk
    2022-12-16
      08:10  +dog Backyard
      14:32  +dog Backyard
      18:27  +dog Walk

The `report intervals` command lists the interval between events. So if you track
replacing a battery in an outdoor camera, knowing the time since the last change
would be useful.

```
   $ rtimelog report intervals -p camera october today
```

The above command might report

   2022-10-07 19:20 +camera Battery : 29d 12:03
   2022-11-06 07:23 +camera Battery : 26d 11:03
   2022-12-02 18:27 +camera Battery : 20d 23:51

This shows that we'll want to check the battery about a week from now.

## Stack For Temporary Task Shifts

As in the example, you may find yourself needing to temporarily pause your
current task to work on another, with the intent to come right back. The `push`
command saves the current task to the stack, and changes to a new task.

```
  $ rtimelog push +projA @Meeting
```

When you are ready to come back to the previous task, you use the `resume` command.

```
  $ rtimelog resume
```

To see the task descriptions on the stack, use the `stack ls`.

```
  $ rtimelog stack ls
```

If you use the stack a great deal, you may find that it begins to grow out of
hand. You'll probably decide that some of the items on the stack are no longer
useful and can be discarded. The following commands support manipulation of the
stack.

If you want to discard task descriptions from the top of the stack, use the `stack drop`
command.

```
  $ rtimelog stack drop
```

To discard everything on the stack, use `stack clear`.

```
  $ rtimelog stack clear
```

If the most recent task descriptions might still be useful, but the older items
could be removed, you can use the `stack keep`.

```
  $ rtimelog stack keep
```

## Reducing Typing with Aliases

As you use the `rtimelog` program more, typing long strings for individual
entries becomes annoying. If you are working on the same projects or tasks on a
regular basis, you might find typing the detail annoying even if you want it
for reporting.

The `rtimelog` program supports defining aliases in your `.timelogrc` file.
These aliases can be either full events or partial events. The aliases are
defined in a **[alias]** section in your `.timelogrc` file. They take the
following form:

   [alias]
      email = start +Misc @Email
      proja = start +ProjectA
      int   = push +Misc @Questions

When `rtimelog` encounters an alias at the beginning of its command line, it
replaces the alias with the string after the equals. This allows you to define
full events or partial events easily. To use an alias, use the alias as the
command when calling `rtimelog`:

```
  $ rtimelog email
```

This gets converted to

```
  $ rtimelog start +Misc @Email
```

The _proja_ alias above is a good example of a partial event. You would use
the alias like:

```
  $ rtimelog proja @ConfCall Client design team
```

which would be converted to

```
  $ rtimelog start +ProjectA @ConfCall Client design team
```

Over time, I often find myself adding new aliases for events relating to new
projects and deleting aliases that I no longer use. After a while, I noticed
that I tended to make clusters of aliases for particular projects. All of the
extra alias maintenance seemed ridiculous. What I really needed was a way to
make aliases that were kind of templates, rather than just replacement strings.

If the expansion string for an alias contains the string `{}`, it is handled
a bit differently. For example, if we assume the following aliases:

    email = start +Misc @Email
    code  = start +client:{} @Code
    doc   = start +client:{} @Doc
    test  = start +client:{} @Test

`rtimelog` replaces the `{}` with the argument that comes after the alias. So
using the *code* alias as follows

```
  $ rtimelog code Bob New UI
```

is converted to

```
  $ rtimelog start +client:Bob @Code New UI
```

This expansion continues until all `{}` strings are replaced. If we try to
replace a `{}` and there is no corresponding argument, the `{}` will be left in
place. This has dramatically improved the entry detail I use without requiring
nearly as much maintenance of my aliases.

## AUTHOR

G. Wade Johnson  `<rust@gwadej.org>`

## LICENSE AND COPYRIGHT

Copyright (c) 2021-2023, G. Wade Johnson `<rust@gwadej.org>`. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself. See [perlartistic](https://opensource.org/licenses/Artistic-2.0).
