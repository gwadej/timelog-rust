# Overview of `rtimelog`

## VERSION

This manual applies to `rtimelog` version 1.0.

## INTRODUCTION

For reasons explained in the [History][history]  document, I found the need years
ago to build a simple tool for tracking my time. The format of the timelog file
and some of the functionality was inspired by Gina Trapani's Todo.txt program.
It's main feature is the simplicity of its file format:

- One entry per line
- pure text
- minimal formatting

For details of the file format, see the [Format][format] document.

## The `rtimelog` Program

The `rtimelog` program manipulates your time log and generates reports. The program
is executed with a command and optional arguments.

## Supported Commands

The `rtimelog` program supports a number of commands that manipulate or report
on the time logged by the program.

### Starting Tasks

The commands you will use the most involve adding events to the time log. The
string `{task description}` is a text string that may have a _+project_ and/or
a _@taskname_. The first string of non-whitespace characters beginning with a '`+`'
is treated as the _project_ for this event. The project is only relevant when
using the reporting commands. Likewise, the first string of non-whitespace
characters beginning with a '`@`' is treated as a task name.

If a task has no name, the non-project part of the entry is treated as the
task name. Otherwise, the non-project, non-taskname part of the event is treated
as detail information for the event.

- `start` {task description}

    Stop the current task and start timing a new task. Since `rtimelog` only
    tracks one task at a time, the current task is always stopped when starting
    a new task.

- `stop`

    Stop timing the current task.

- `push` {task description}

    Save the current task description on stack and start timing new task. The
    current task description is saved in the stack file in the timelog directory.
    You can manipulate the saved task description with **resume**, or one of the
    **stack** commands.

- `resume`

    Stop the current task and restart top task from the stack. This also removes
    the task description from the stack.

- `pause`

    Save the current task description on the stack and stop that task from timing.
    It's the equivalent of pushing the current task, while stopping timing at the
    same time.

- `swap`

    Pop the top task description off the stack, resuming that task. Save the
    current task description on the stack. This is useful for time-slicing or
    toggling between a couple of activities.

### Adding Non-task Entries

Over time, I found a need for a few non-task related entries that would be useful
to track along with the tasks. These commands allow adding those entries.

- `comment` {comment text}

   Add a comment line to the logfile.

- `event` {event text}

   Add a zero duration event to the logfile. Denotes an event that has occurred
   and you would like to track, but which is not the beginning of a task.

### Entry Manipulation Commands

The `entry` command has subcommands to perform different actions on the most recent
task entry in the logfile. In the past, I often found the need to execute the `edit`
command to make small alterations to the most recent entry. These subcommands cover many
of those small edits. The supported subcommands are:

- `entry discard`

   Discard the most recent entry from the logfile. This is useful if you started
   a new task without meaning to.

- `entry ignore`

   Mark the most recent entry in the logfile as ignored. This is similar to discarding
   the new entry, but leaves an indication that the task was nearly changed. The ignored
   entry does not contribute to reports in any way.

   A good use for the ignore command is to show that you were interrupted from your current
   task, but spent no time away. For example, a meeting was supposed to start at 1pm. You
   mark the time, just as you get a message that the meeting was cancelled. You haven't really
   spent any time on the meeting, but it might be useful to track the interruption.

I found that most of the time I used the `edit` command was to adjust the time of the most
recent entry. Quite often I would start typing the command a bit before starting a meeting
or something and then miss pressing enter. Other times I would start to answer a question
for someone and then realize that this was going to be a long exercise, not a quick question.
These commands allow for quickly adjusting the time.

- `entry now`

   Reset the time of the most recent entry to now. This can be useful if you
   started a new task a little too soon.

- `entry rewrite` {task description}

   Replace the task description on the most recent entry.

- `entry was` {hh:mm|hh:mm:ss}

   Reset the time on the most recent entry

- `entry rewind` {minutes}

   Shift the time on the most recent entry back the specified number of minutes.

### Stack Manipulation

In a given day, I find that much time is spent with meetings or quick questions that interrupt the
main flow of my work. I will often need to move back to a previous task after the interruption. This
lead to the addition of stack manipulation commands. These are purely for the purpose of reducing
typing in certain circumstances and are not actually required for normal use of the program.

The `stack` command has subcommands to perform different actions on the stack
file. Those subcommands are:

- `stack ls`

    Display items on the stack. The top task description on the stack is the one that is
    resumed by the **resume** command.

- `stack top`

   Display just the top item on the stack.

- `stack clear`

    Clear the stack file.

- `stack drop` \[{n}\]

    Drop one or more task descriptions from top of entry stack. If no arguments are
    supplied, only the top item is dropped. If the argument is a number, that number of
    task descriptions is dropped from the stack.

- `stack keep` \[{n}\]

    Remove all task descriptions from the stack except the top `{n}` items. If no
    arguments are supplied, keep the top 10 items from the stack. If the supplied
    argument is a positive integer, all stack items except the `n` topmost are
    discarded.

### Informational Commands

There are a number of commands that display some information from the timelog file.

- `ls` \[date\]

    List entries for the specified day. See the [Reporting] section that follows for
    information about date formats. Default to *today*.

- `lsproj`

    List known projects, one per line.

- `curr`

    Display the start date and time and task description of the current task,
    if any.

### Reporting

The reporting commands generate a report of information from the timelog file.
Many of these commands accept a _date range descriptor_.
A date range descriptor can take one of the following forms:

#### Single Date

A single date in one of the following forms:

* explicit date of the form yyyy-mm-dd
* the string _today_
* the string _yesterday_
* a day of the week: _sunday_, _monday_, _tuesday_, _wednesday_, ... etc.

The values of _today_ and _yesterday_ describe the appropriate expected day.
Each of the weekday values references the last instance of that day before today.

This will result in a range spanning one day.

#### Date Pair

A pair of dates in the forms above.

This pair results in a range that covers the two supplied dates. If the dates are in order,
the start is the first date and end is the second date. If the dates are out of order, the
range is not valid.

#### Range Description

A range description in one of the following forms:

* a month name: _january_, _february_, _march_, ... etc.
* a short (3 char) month name: _jan_, _feb_, _mar_, ... etc.
* a relative time frame: _(this|last)_ _(week|month|year)_
* the string _ytd_

If the range description is a month name, the range will cover the whole month with that name
before today.

If the range description is _this_ followed by _week_, _month_, or _year_, the resultant range
covers:

* week: from the last Sunday to Saturday of this week,
* month: from the first day of the current month to last day of the month,
* year: from the January 1 of the current year to the last day of the year.

If the range description is _last_ followed by _week_, _month_, or _year_, the resultant range
covers:

* week: from the two Sundays ago to last Saturday,
* month: from the first of the month before this one to the last day of that month,
* year: from January 1 of the year before the current one to December 31 of the same year.

If the range description is the string _ytd_, the range will be from January 1 of the current year
up to today.

Any time you supply a pair of dates, the second must be later in time than the
first.

#### Individual Reports

The report commands (except for `chart`) also support a repeatable option of `-p`
which specifies a project to include in the report. The argument supplied with
the `-p` option can either be the project name or part of a project name.

The reporting commands are:

- `report hours` \[-p proj\] \[date \[end date\]\]

    Display the hours worked for each of the appropriate days. The default date
    range is *today*.

- `report detail` \[-p proj\] \[date \[end date\]\]

    Display a report for the specified days.

    This report is grouped by day, with each project grouped under the day.
    Each event for a project is displayed under the project. Accumulated times are
    displayed for each project and day. The default date range is *today*.

- `report summary` \[-p proj\] \[date \[end date\]\]

    Display a summary of the appropriate days' projects.

    For each day, the list of projects active during that day is shown along with
    the aggregate time spent on that project. The default date range is *today*.

- `report chart` \[date \[end date\]\]

    Create a graphical dashboard containing pie charts showing the proportion
    of projects during the day, and tasks for each project, as well as bar graph
    showing how time during the day is distributed into projects. The default date
    range is *today*.

- `report events` \[-p proj\] \[date \[end date\]\]

    Display a list of the zero duration events from the supplied date range,
    along with the time of the event. The default date range is *today*.

- `report intervals` \[-p proj\] \[date \[end date\]\]

    Display a list of the zero duration events from the supplied date range,
    along with the time between the events. The default date range is *today*.

### Other Commands

The remaining commands do not necessarily fit into one of the other categories.

- `init` \[dir\]

    Prepare the system for use with `rtimelog`. First, it creates the directory
    where rtimelog stores its information, if that directory does not already exist.
    Then, the command creates and initializes the `.timelogrc` configuration file,
    in the user's home directory, with the information needed to run.

    If a directory is not supplied, use the default location of `timelog` in
    the user's home directory.

- `edit`

    Open the timelog file in the current editor. The editor can be specified in the
    configuration file. If no editor is specified in the configuration, the program
    uses the value of the `VISUAL` environment variable. If `VISUAL` has no value,
    then the value of `EDITOR` is used instead. If neither environment variable has
    a value, then the program defaults to `vim`

- `archive`

    Move all entries from the earliest year in the logfile, into a new file in the
    timelog directory named `timelog-{year}.txt`. No entries are moved from the
    current year.

- `check`

    Walk the logfile and report any problems found with the file.
    
- `help` \[command\]

    Display help about commands. The _help_ command gives more detailed help
    about the command.

    With no arguments, you get a list of the commands, each with its associated
    help text. The commands are each listed as a usage line with some
    explanatory text.

    Any other argument is looked up in the command list.

- `aliases`

    List all of the aliases and their expansions from the config file.

## Configuration

The `rtimelog` program uses the file `~/.timelogrc` if it exists.

The configuration file is expected to contain data in two major parts:

### General Configuration

The first section defined general configuration information in a key=value
format. The recognized keys are:

- editor

    The editor to use when opening the timelog file with the `edit` command.
    If not specified, it will use the value of either the `VISUAL` or `EDITOR`
    environment variables. If none are found, it will default to `vim`.

- browser

    The browser command to use when displaying the report generated by the
    `chart` command. If not specified, it will use a hard-coded value of
    `chromium`, except on Mac OS, where it will use `open`.

- dir

    The directory in which to find the timelog data files. Defaults to the
    `timelog` directory in the user's home directory.

- defcmd

    The default command to be used if none is supplied to timelog. By default,
    this is the '**curr**' command.

### Command Aliases

The configuration file may also contain an '`[alias]`' section that defines
command aliases. Each alias is defined as a `shortname=expansion string`.

For example, if you regularly need to make entries for reading email and
triaging bug reports you might want the following in your configuration.

    [alias]
      email = start +Misc @Email
      triage = start +BugTracker @Triage

If the expansion contains the string `{}`, it will be replaced with the first
argument after the alias name from the command line. This will continue until
all `{}` strings are replaced. If we try to replace a `{}` and there is no
corresponding argument, the `{}` will be left in place.

[history]: History.md
[format]: Format.md

## AUTHOR

G. Wade Johnson  `<rust@gwadej.org>`

## LICENCE AND COPYRIGHT

Copyright (c) 2021-2023, G. Wade Johnson `<rust@gwadej.org>`. All rights reserved.

This module is free software; you can redistribute it and/or modify it under the
same terms as Perl itself. See [perlartistic](https://opensource.org/licenses/Artistic-2.0).
