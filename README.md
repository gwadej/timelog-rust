# TimeLog

Version of the rtimelog program written in Rust.

## Purpose

The rtimelog program provides a low-impact, simple tool for tracking a person's
time. It tracks individual changes in task _events_ in a simple text file.

Rtimelog is a command line tool that supports adding and ending events. It also
supports different reports on the amount of time spend on various tasks.

Full documentation of the tool is found in [Manual][manual] and [Tutorial][tutorial].

The file [Format][format] provides information about how events are stored in the timelog
file. This would be useful if you want to modify the file or generate your own
reports from the file.

The original version of the format is described in [Format, version 1][format1].
That version is the one supported by the Perl version of the application.

## Install

Use `cargo` to install the `rtimelog` executable.

```bash
cargo install rtimelog
```

## Badges

![I'm working on it](https://img.shields.io/badge/maintenance-actively--developed-brightgreen.svg "maintenace badge")

[manual]: http://gwadej.org/timelog/manual/
[tutorial]: http://gwadej.org/timelog/tutorial/
[format]: http://gwadej.org/timelog/manual/appendix/format.html
[format1]: https://gitlab.com/gwadej/timelog-rust/-/blob/main/Format-v1.md
